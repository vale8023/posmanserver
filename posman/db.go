package posman

import (
	"github.com/astaxie/beego/orm"
	"strconv"
	"errors"

	"freshlife.io/PosmanServer/hh"

)


var ServerID =hh.NowTime()


//通过商品ID和店铺id 当前商品的价格
func getStockProductPrice(shopid int,product_id int) (float64,error){
	db := orm.NewOrm()
	var maps []orm.Params
	num, err := db.Raw("SELECT * FROM `gr_pos_shop_stock` WHERE `shop_id` = ? AND `product_id` = ? ", shopid,product_id).Values(&maps)
	if err == nil && num > 0 {
		stock_single_price ,_:= strconv.ParseFloat(maps[0]["stock_single_price"].(string),64)
		return stock_single_price,nil

	}else{

		return 0,errors.New("查询失败")
	}
}

//获取商品的名称
func getProductName(product_id int) (string,error){
	db := orm.NewOrm()
	var maps []orm.Params
	num, err := db.Raw("SELECT name FROM `gr_pos_product` WHERE `id` = ? ",product_id).Values(&maps)
	if err == nil && num > 0 {

		return maps[0]["name"].(string),nil

	}else{
		return "",errors.New("查询失败")
	}
}




//收银机客户端软件登录
func ClientLogin(client_id string,shop_id int,client_version string){


	db := orm.NewOrm()

	db.Raw("DELETE FROM `gr_pos_client_online` WHERE `client_id` = ?", client_id).Exec()
	db.Raw("INSERT INTO `gr_pos_client_online` (`server_id`,`client_id`,`client_version`, `shop_id`, `is_online`, `login_time`) VALUES (?,?,?,?,1,unix_timestamp());", ServerID,client_id,client_version,shop_id).Exec()
}

//收银机客户端软件断开
func ClientLogout(client_id string){

	db := orm.NewOrm()
	db.Raw("DELETE FROM `gr_pos_client_online` WHERE `client_id` = ?", client_id).Exec()

}

//收银机客户端软件全部断开
func ClientALLLogout(){
	db := orm.NewOrm()
	db.Raw("DELETE FROM `gr_pos_client_online`").Exec()
}