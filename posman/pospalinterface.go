package posman

import (
	"net/http"
	"strings"
	"github.com/astaxie/beego"
	"freshlife.io/PosmanServer/hh"
	"github.com/bitly/go-simplejson"
	"time"
	"github.com/astaxie/beego/orm"
)

type PospalController struct {
	BaseController
}



func (c *PospalController) GetPosPalCardInfo(event hh.Event) map[string]interface{} {

	memberNO, checkerr1 := event.Content.Data.Get("cardno").String()

	c.Data = make(map[string]interface{})
	if checkerr1 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}
	client := &http.Client{}
	bodystr := "{\"appId\":\"FD17BEBFFD325719C225A1383C64794E\",\"customerNum\":\"" + memberNO + "\"}"
	reqest, _ := http.NewRequest("POST", "https://area12-win.pospal.cn:443/pospal-api2/openapi/v1/customerOpenApi/queryByNumber", strings.NewReader(bodystr))

	reqest.Header.Set("User-Agent", "openApi")
	reqest.Header.Set("Content-Type", "application/json; charset=utf-8")
	reqest.Header.Set("accept-encoding", "gzip,deflate")
	reqest.Header.Set("time-stamp", hh.NowTime())
	reqest.Header.Set("data-signature", strings.ToUpper(md5str("624917582949429807"+bodystr)))

	response, _ := client.Do(reqest)
	defer response.Body.Close()

	//body, _ := ioutil.ReadAll(response.Body)json类型不适用

	body, err := simplejson.NewFromReader(response.Body)
	//err := json.NewDecoder(response.Body).Decode(&body)// https://stackoverflow.com/questions/27595480/does-golang-request-parseform-work-with-application-json-content-type

	if err != nil {
		beego.Error("获取银豹会员信息，请求失败")
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "获取银豹会员信息，请求失败。"
		return c.Data
	}

	result, _ := body.Get("status").String()

	if result == "success" {
		c.Data["status"] = "success"
		c.Data["num"] = 1
		c.Data["message"] = "数据查询成功"
		c.Data["data"] = body.Get("data")
		beego.Info("获取银豹会员信息成功")
	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "数据查询失败"
		beego.Error("获取银豹会员信息失败")
	}

	return c.Data
}

func (c *PospalController) PospalSwitchtoXSH(event hh.Event) map[string]interface{} {

	customerUid, checkerr1 := event.Content.Data.Get("customerUid").String()
	balanceIncrement, checkerr2 := event.Content.Data.Get("balanceIncrement").String()
	shop_id , checkerr3:= event.Content.Data.Get("client_shop_id").Int()
	card_num , checkerr4:= event.Content.Data.Get("card_num").String()
	card_hard_num , checkerr5:= event.Content.Data.Get("card_hard_num").String()
	card_type , checkerr6:= event.Content.Data.Get("card_type").Int()
	card_password , checkerr7:= event.Content.Data.Get("card_password").String()
	card_name , checkerr8:= event.Content.Data.Get("card_name").String()
	card_phone , checkerr9:= event.Content.Data.Get("card_phone").String()
	card_address , checkerr10:= event.Content.Data.Get("card_address").String()
	card_create_by , checkerr11:= event.Content.Data.Get("card_create_by").Int()
	card_remark , checkerr12:= event.Content.Data.Get("card_remark").String()
	pospal_num , checkerr13:= event.Content.Data.Get("pospal_num").String()
	member_money,checkerr14 :=event.Content.Data.Get("member_money").Float64()

	o := orm.NewOrm()

	var maps0 []orm.Params
	num0, err0 := o.Raw("SELECT * FROM `gr_pos_member_card` WHERE card_hard_num=?",card_hard_num).Values(&maps0)
	if err0 != nil{

		c.Data["status"]="fail"
		c.Data["message"]="会员卡查询错误"
		return  c.Data
	}
	if num0 > 0 {
		c.Data["status"]="fail"
		c.Data["message"]="会员卡已经存在，无法继续添加"
		return  c.Data
	}

	//SQL准备
	o.Begin()
	//0.插入转换记录表准备
	db_member_card_insert, _ := o.Raw("INSERT INTO `gr_pos_member_card` ( `card_num`, `card_hard_num`, `type`, `password`, " +
		"`level`, `name`, `phone`, `address`, `amount`, `open_card_shop_id`, `create_by`, `remark`, `status`, `createAt`, `updateAt`, `delflag`)" +
		" VALUES ( ?, ?, ?, ?, 0 , ? , ? , ? , ?, ? , ? , ? , 1, unix_timestamp() ,unix_timestamp(), '0')").Prepare()
	defer db_member_card_insert.Close()

	db_member_card_detail_insert ,_ := o.Raw("INSERT INTO `gr_pos_member_pospal` " +
		"( `card_num`, `card_hard_num`, `pospal_card_num`, `amount`, `shop_id`, `create_by`, `remark`, `status`, `createAt`, `updateAt`, `delflag`)" +
		" VALUES ( ?, ?, ?, ?, ?, ?, ?, 1,  unix_timestamp(), NULL, '0');").Prepare()
	defer db_member_card_detail_insert.Close()

	if len(card_password) > 0{
		card_password = md5str(card_password)
	}

	_,err1 := db_member_card_insert.Exec(card_num,card_hard_num,card_type,card_password,card_name,card_phone,card_address,member_money,shop_id,card_create_by,card_remark)

	_,err2 := db_member_card_detail_insert.Exec(card_num,card_hard_num,pospal_num,member_money,shop_id,card_create_by,card_remark)

	if err1 != nil || err2 != nil{
		o.Rollback()
		c.Data["status"]="fail"
		c.Data["code"]=-5001
		c.Data["message"]="新增会员卡失败。"
		return c.Data
	}

	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM `gr_pos_member_card` WHERE card_hard_num=?",card_hard_num).Values(&maps)
	if err != nil{
		o.Rollback()
		c.Data["status"]="fail"
		c.Data["message"]="会员卡查询错误"
		return  c.Data
	}

	o.Commit()

	c.Data = make(map[string]interface{})
	if checkerr1 !=nil || checkerr2 != nil || checkerr3 != nil || checkerr4 != nil || checkerr5 != nil || checkerr6 != nil || checkerr7 != nil || checkerr8 != nil || checkerr9 != nil || checkerr10 != nil || checkerr11 != nil || checkerr12 != nil || checkerr13 != nil || checkerr14 != nil{
		c.Data["status"]="fail"
		c.Data["code"]=-9999
		c.Data["message"]="传入的参数有误。"
		return c.Data
	}
	bodystr := "{\"appId\":\"FD17BEBFFD325719C225A1383C64794E\",\"customerUid\":\"" + customerUid + "\",\"balanceIncrement\":"+balanceIncrement+",\"pointIncrement\":0,\"dataChangeTime\":\""+time.Now().String()[0:19]+"\"}"

	client := &http.Client{}
	reqest, err1 := http.NewRequest("POST", "https://area12-win.pospal.cn:443/pospal-api2/openapi/v1/customerOpenApi/updateBalancePointByIncrement", strings.NewReader(bodystr))
	if err1 !=nil{
		
	}
	reqest.Header.Set("User-Agent", "openApi")
	reqest.Header.Set("Content-Type", "application/json; charset=utf-8")
	reqest.Header.Set("accept-encoding", "gzip,deflate")
	reqest.Header.Set("time-stamp", hh.NowTime())
	reqest.Header.Set("data-signature", strings.ToUpper(md5str("624917582949429807"+bodystr)))

	response, _ := client.Do(reqest)

	defer response.Body.Close()

	body, err := simplejson.NewFromReader(response.Body)

	if err != nil {
		beego.Error("获取银豹会员信息，请求失败")
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "会员卡添加成功！获取银豹会员信息请求失败。"
		return c.Data
	}

	result, _ := body.Get("status").String()

	if result == "success" {
		c.Data["status"] = "success"
		c.Data["num"] = num
		c.Data["message"] = "成功"
		c.Data["data"] = maps
		beego.Info("会员卡转入成功")
	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "失败"
		beego.Error("会员卡添加成功！修改银豹会员信息失败")
	}

	return c.Data
}