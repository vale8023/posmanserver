package posman

import (
	"github.com/astaxie/beego/orm"
	"freshlife.io/PosmanServer/hh"

	"github.com/astaxie/beego"

	"strconv"
)

type UserController struct {
	BaseController
}

//code 1000

//收银员登录
func (c *UserController) ClientLogin(event hh.Event) map[string]interface{} {
	c.Prepare()
	client_id, checkerr1 := event.Content.Data.Get("client_id").String()
	shop_id, checkerr2 := event.Content.Data.Get("shop_id").Int()
	client_version, checkerr1 := event.Content.Data.Get("client_version").String()
	ClientLogin(client_id, shop_id, client_version)
	if checkerr1 != nil || checkerr2 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}
	return c.Data
}

//收银员登录
func (c *UserController) CashierLogin(event hh.Event) map[string]interface{} {

	c.Prepare()

	name, checkerr1 := event.Content.Data.Get("username").String()
	pwd, checkerr2 := event.Content.Data.Get("userpwd").String()
	shop_id, checkerr3 := event.Content.Data.Get("shop_id").Int()

	if checkerr1 != nil || checkerr2 != nil || checkerr3 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	var ucauthkey = beego.AppConfig.String("UC_AUTH_KEY")
	var passwordmd5 = think_ucenter_md5(pwd, ucauthkey);

	//连接数据库准备查询
	o := orm.NewOrm()
	o.Begin()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw("SELECT a.*,b.nickname,b.status member_status"+
		" FROM gr_ucenter_member a,gr_member b "+
		" WHERE a.username = ? and a.password = ? and a.id = b.uid", name, passwordmd5).Values(&maps)
	if err != nil || num < 1 {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["message"] = "登录失败,用户表查询错误"
		return c.Data
	}

	if maps[0]["status"].(string) != "1" || maps[0]["member_status"].(string) != "1" {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["message"] = "该用户已被禁用"
		return c.Data
	}

	uid, err := strconv.ParseInt(maps[0]["id"].(string), 10, 0)

	var maps2 []orm.Params
	num2, err2 := o.Raw("SELECT a.* FROM gr_pos_change_shift_record a  WHERE a.shop_id = ? and a.is_end = 0", shop_id).Values(&maps2)
	if err2 != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["message"] = "登录失败,上下班表操作错误"
		return c.Data
	}

	if num2 > 0 {
		//已经登录过之前断开过
		start_time, _ := strconv.ParseInt(maps2[0]["start_time"].(string), 10, 0)
		maps[0]["work_start_time"] = start_time

	} else {
		//这个店铺又是一个新的班次开始了

		res, err3 := o.Raw("INSERT INTO `gr_pos_change_shift_record` "+
			"(`shop_id`, `start_time`, `end_time`, `is_end`, `create_by`, `status`, `createAt`, `updateAt`, `delflag`) "+
			"VALUES (?, "+ hh.NowTime()+ ", 0,0, ?, '1', "+ hh.NowTime()+ ", "+ hh.NowTime()+ ", '0')", shop_id, uid).Exec()

		if err3 != nil {
			o.Rollback()
			c.Data["status"] = "fail"
			c.Data["message"] = "登录失败,上下班表操作错误"
			return c.Data
		}

		lastInsertId, _ := res.LastInsertId()

		var maps4 []orm.Params
		_, err4 := o.Raw("SELECT a.* FROM gr_pos_change_shift_record a  WHERE id = ? and a.is_end = 0", lastInsertId).Values(&maps4)
		if err4 != nil {
			o.Rollback()
			c.Data["status"] = "fail"
			c.Data["message"] = "登录失败,上下班表操作错误"
			return c.Data
		}
		start_time, _ := strconv.ParseInt(maps4[0]["start_time"].(string), 10, 0)
		maps[0]["work_start_time"] = start_time
	}

	var maps5 []orm.Params
	num5, err5 := o.Raw("SELECT * FROM gr_pos_shop a  WHERE a.id = ? ", shop_id).Values(&maps5)
	if err5 != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["message"] = "查询支付信息表失败"
		return c.Data
	}

	if num5 == 1 {
		alipay_store_id := maps5[0]["alipay_store_id"]
		pos_recharge_validate := maps5[0]["pos_recharge_validate"]
		beego.Error(pos_recharge_validate)
		maps[0]["alipay_store_id"] = alipay_store_id
		maps[0]["pos_recharge_validate"] = pos_recharge_validate
	} else {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["message"] = ""
		return c.Data
	}

	o.Commit()

	if num > 0 {
		c.Data["status"] = "success"
		c.Data["data"] = maps
		c.Data["num"] = 1
		c.Data["message"] = "登录验证成功"

	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "用户名或密码错误"
	}

	return c.Data
}

//收银员离线后登录
func (c *UserController) OfflineLogin(event hh.Event) map[string]interface{} {
	c.Prepare()
	uid, checkerr1 := event.Content.Data.Get("uid").Int()
	shop_id, checkerr2 := event.Content.Data.Get("shop_id").Int()
	createAt, checkerr3 := event.Content.Data.Get("createAt").Int()

	o := orm.NewOrm()
	o.Begin()

	if checkerr1 != nil || checkerr2 != nil || checkerr3 != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	var maps []orm.Params
	num2, err2 := o.Raw("SELECT a.* FROM gr_pos_change_shift_record a  WHERE a.shop_id = ? and a.is_end = 0 and start_time < ?", shop_id, createAt).Values(&maps)
	if err2 != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["message"] = "登录失败,上下班表操作错误"
		return c.Data
	}

	if num2 > 0 { //已经登录过之前断开过,服务端有记录，返回服务端时间

		var maps1 []orm.Params
		num, err := o.Raw("SELECT a.*,b.nickname FROM gr_ucenter_member a,gr_member b WHERE a.username = ? and a.id = b.uid", uid).Values(&maps1)
		if err != nil || num < 1 {
			o.Rollback()
			c.Data["status"] = "fail"
			c.Data["message"] = "登录失败,用户表查询错误"
			return c.Data
		}
		maps[0]["nickname"] = maps1[0]["nickname"]
	} else { //服务端无记录，或者记录时间大于本地的登陆时间
		_, err5 := o.Raw("delete from `gr_pos_change_shift_record` where shop_id = ? and is_end = 0", shop_id).Exec() //先把原纪录删除
		if err5 != nil {
			o.Rollback()
			c.Data["status"] = "fail"
			c.Data["message"] = "登录失败,上下班表操作错误"
			return c.Data
		}

		var maps3 []orm.Params
		_, err7 := o.Raw("select * from `gr_ucenter_member` where username = ? ", uid).Values(&maps3) //取uid
		if err7 != nil {
			o.Rollback()
			c.Data["status"] = "fail"
			c.Data["message"] = "登录失败,上下班表操作错误"
			return c.Data
		}

		res, err3 := o.Raw("INSERT INTO `gr_pos_change_shift_record` "+
			"(`shop_id`, `start_time`, `end_time`, `is_end`, `create_by`, `status`, `createAt`, `updateAt`, `delflag`) "+
			"VALUES (?, ?, 0,0, ?, '1', unix_timestamp(), unix_timestamp(), '0')", shop_id, createAt, maps3[0]["id"]).Exec()

		if err3 != nil {
			o.Rollback()
			c.Data["status"] = "fail"
			c.Data["message"] = "登录失败,上下班表操作错误"
			return c.Data
		}

		lastInsertId, _ := res.LastInsertId()
		o.Commit()
		_, err4 := o.Raw("SELECT a.* ,c.nickname "+
			"FROM gr_pos_change_shift_record a "+
			"LEFT join gr_ucenter_member b on a.create_by = b.id  "+
			"left join gr_member c on b.id = c.uid "+
			"WHERE a.id = ? ", lastInsertId).Values(&maps)
		if err4 != nil {
			o.Rollback()
			c.Data["status"] = "fail"
			c.Data["message"] = "登录失败,上下班表操作错误"
			return c.Data
		}
	}

	c.Data["status"] = "success"
	c.Data["data"] = maps
	c.Data["num"] = 1
	c.Data["message"] = "登录验证成功"

	return c.Data
}

//经销商账号登录
func (c *UserController) ShopLogin(event hh.Event) map[string]interface{} {

	c.Prepare()

	name, checkerr1 := event.Content.Data.Get("username").String()
	pwd, checkerr2 := event.Content.Data.Get("userpwd").String()

	if checkerr1 != nil || checkerr2 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	var ucauthkey = beego.AppConfig.String("UC_AUTH_KEY")
	var passwordmd5 = think_ucenter_md5(pwd, ucauthkey);

	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw("SELECT a.* FROM gr_pos_shop a WHERE a.account = ? and a.login_password = ? and a.status = 1", name, passwordmd5).Values(&maps)
	if err != nil {
		return nil
	}

	if num > 0 {
		c.Data["status"] = "success"
		c.Data["message"] = "登录验证成功"
		c.Data["num"] = 1
		c.Data["data"] = maps
	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "登录验证失败"
	}

	return c.Data
}
