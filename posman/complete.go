package posman

import (
	"freshlife.io/PosmanServer/hh"
	"github.com/astaxie/beego/orm"
	"strconv"
	"github.com/astaxie/beego/logs"
	"errors"

	"github.com/astaxie/beego"
)

type CompleteController struct {
	BaseController
}

//code 1000

//交接班 日结 控制器

//交接班操作
func (c *CompleteController) ChangeShiftAction(event hh.Event) map[string]interface{} {
	c.Prepare()

	shop_id, checkerr1 := event.Content.Data.Get("shop_id").Int()             //店铺id
	create_by, checkerr2 := event.Content.Data.Get("create_by").Int()         //交接班人员名称
	submit_cash, checkerr3 := event.Content.Data.Get("submit_cash").Float64() //钱箱现金
	address, checkerr3 := event.Content.Data.Get("address").String()          //店铺地址

	if checkerr1 != nil || checkerr2 != nil || checkerr3 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -99991
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()

	//SQL准备
	o.Begin()

	change_shift_id, _, err := c.changeShift(o, shop_id, create_by, submit_cash, address)

	if err == nil {
		o.Commit()
	} else {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["code"] = -10008
		c.Data["message"] = "执行交接班操作失败。"

		hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(shop_id) + "，交接班操作失败了！")

		return c.Data
	}

	data := make(map[string]interface{})

	var maps []orm.Params
	_, err1 := o.Raw("SELECT * FROM `gr_pos_change_shift` WHERE `id` = ?", change_shift_id).Values(&maps)
	if err1 != nil {
		c.Data["status"] = "fail"
		c.Data["message"] = "未查到交接班记录。"
		return c.Data
	}

	data["change_shift"] = maps

	var maps2 []orm.Params
	_, err2 := o.Raw("SELECT * FROM `gr_pos_change_shift_detail` WHERE `change_shift_id` = ?", change_shift_id).Values(&maps2)
	if err2 != nil {
		c.Data["status"] = "fail"
		c.Data["message"] = "未查到交接班明细记录。"
		return c.Data
	}

	data["change_shift_detail"] = maps2

	c.Data["data"] = data
	c.Data["status"] = "success"
	c.Data["message"] = "交接班执行成功。"
	return c.Data
}

//日结操作
func (c *CompleteController) OverAllDaliyAction(event hh.Event) map[string]interface{} {
	c.Prepare()

	shop_id, checkerr1 := event.Content.Data.Get("shop_id").Int()             //店铺id
	create_by, checkerr2 := event.Content.Data.Get("create_by").Int()         //交接班人员名称
	submit_cash, checkerr3 := event.Content.Data.Get("submit_cash").Float64() //钱箱现金
	address, checkerr3 := event.Content.Data.Get("address").String()          //店铺地址

	logs.Error("shop_id" +strconv.Itoa(shop_id)+"日结开始" )

	if checkerr1 != nil || checkerr2 != nil || checkerr3 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -99991
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()

	//SQL准备
	o.Begin()

	change_shift_id, _, err := c.changeShift(o, shop_id, create_by, submit_cash, address)

	if err != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["code"] = -10008
		c.Data["message"] = "执行交接班操作失败。"
		hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(shop_id) + "，日结时，交接班操作失败了！")
		return c.Data
	}

	data := make(map[string]interface{})

	var maps []orm.Params
	_, err1 := o.Raw("SELECT * FROM `gr_pos_change_shift` WHERE `id` = ?", change_shift_id).Values(&maps)
	if err1 != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["message"] = "未查到交接班记录。"
		logs.Error("shop_id" +strconv.Itoa(shop_id)+"未查到交接班记录。" )
		return c.Data
	}

	data["change_shift"] = maps

	var maps2 []orm.Params
	_, err2 := o.Raw("SELECT * FROM `gr_pos_change_shift_detail` WHERE `change_shift_id` = ?", change_shift_id).Values(&maps2)
	if err2 != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["message"] = "未查到交接班明细记录。"
		logs.Error("shop_id" +strconv.Itoa(shop_id)+"未查到交接班明细记录。" )
		return c.Data
	}

	data["change_shift_detail"] = maps2
	//交接班完成

	//*********************开始统计日结数据************************
	//1.获得上一次日结开始时间 end_at
	var maps3 []orm.Params
	_, err3 := o.Raw("SELECT end_at FROM `gr_pos_overall_daliy` WHERE `shop_id` = ? ORDER BY id desc", strconv.Itoa(shop_id)).Values(&maps3)
	if err3 != nil {
		c.Data["status"] = "fail"
		c.Data["message"] = "未查到上一次日结结束时间。"
		logs.Error("shop_id" +strconv.Itoa(shop_id)+"未查到上一次日结结束时间。" )
		return c.Data
	}

	var overall_daliy_begin_at int64

	if len(maps3) < 1 {
		overall_daliy_begin_at = 0
	} else {
		overall_daliy_begin_at, _ = strconv.ParseInt(maps3[0]["end_at"].(string), 10, 0) //日结开始时间
	}

	overall_daliy_end_at, _ := strconv.ParseInt(hh.NowTime(), 10, 0) //日结结束时间

	db_overall_daliy_insert, _ := o.Raw("INSERT INTO `gr_pos_overall_daliy` (`shop_id`, `begin_at`, `end_at`, `shift_count`, `total_sell_amount`," +
		" `total_order_count`, `total_reject_order_count`, `total_charge_amount`, `total_cash_amount`, `total_submit_cash`, `total_payable_cash`, " +
		"`total_charge_count`, `stock_check_id`,`check_stock_count`, `no_check_stock_count`, `profit_check_stock_count`, `loss_check_count`, `check_stock_by`, " +
		"`submit_to_correct`, `submit_by`, `status`, `createAt`, `updateAt`, `delflag`) VALUES" +
		" (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,1,"+hh.NowTime()+","+hh.NowTime()+",'0')").Prepare()
	defer db_overall_daliy_insert.Close()

	//日结报表数据计算
	var overall_daliy_shift_count int64              //班次数量
	var overall_daliy_total_sell_amount float64      //总销售额（总销售额）
	var overall_daliy_total_order_count int64        //正常销售总单数（总正常单据）
	var overall_daliy_total_reject_order_count int64 //总退单数（总退款单据）
	var overall_daliy_total_charge_amount float64    //充值总金额（总会员充值）
	var overall_daliy_total_cash_amount float64      //应收现金总额（总应有现金）
	var overall_daliy_total_submit_cash float64      //实际提交金额（总钱箱现金）
	var overall_daliy_total_payable_cash float64     //总应交现金
	var overall_daliy_total_charge_count int64       //总充值笔数

	var overall_daliy_stock_check_id int64           //盘点数据id
	var overall_daliy_check_stock_count int64        //盘点商品数量
	var overall_daliy_no_check_stock_count int64     //未盘数量
	var overall_daliy_profit_check_stock_count int64 //盘盈数量
	var overall_daliy_loss_check_count int64         //盘亏数量
	var overall_daliy_check_stock_by int64           //盘点人ID

	var overall_daliy_submit_to_correct int64 //是否提交校准账面数量 0未提交 1已提交
	var overall_daliy_submit_by string        //提交校准人（签名）

	var maps4 []orm.Params
	//num4, err4 := o.Raw("SELECT count(1) as overall_daliy_shift_count ,ifnull(sum(total_sell_amount),0) as overall_daliy_total_sell_amount ," +
	//	"ifnull(sum(total_order_count),0) as overall_daliy_total_order_count," +
	//	"ifnull(sum(total_reject_order_count),0) as overall_daliy_total_reject_order_count," +
	//	"ifnull(sum(total_charge_amount),0) as overall_daliy_total_charge_amount,ifnull(sum(total_cash_amount),0) as overall_daliy_total_cash_amount," +
	//	"ifnull(sum(submit_cash),0) as overall_daliy_total_submit_cash," +
	//	"ifnull(sum(payable_cash),0) as overall_daliy_total_payable_cash," +
	//	"ifnull(sum(total_charge_count),0) as overall_daliy_total_charge_count " +
	//	"FROM `gr_pos_change_shift` " +
	//	"WHERE shop_id=? AND begin_at >= ? AND end_at <= ? ",shop_id,overall_daliy_begin_at,overall_daliy_end_at).Values(&maps4)
	num4, err4 := o.Raw("SELECT count(1) as overall_daliy_shift_count ,ifnull(sum(total_sell_amount),0) as overall_daliy_total_sell_amount ,"+
		"ifnull(sum(total_order_count),0) as overall_daliy_total_order_count,"+
		"ifnull(sum(total_reject_order_count),0) as overall_daliy_total_reject_order_count,"+
		"ifnull(sum(total_charge_amount),0) as overall_daliy_total_charge_amount,ifnull(sum(total_cash_amount),0) as overall_daliy_total_cash_amount,"+
		"ifnull(sum(submit_cash),0) as overall_daliy_total_submit_cash,"+
		"ifnull(sum(payable_cash),0) as overall_daliy_total_payable_cash,"+
		"ifnull(sum(total_charge_count),0) as overall_daliy_total_charge_count "+
		"FROM `gr_pos_change_shift` "+
		"WHERE shop_id=? AND is_overalled = 0", shop_id).Values(&maps4)
	if err4 != nil || num4 < 1 {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["message"] = "未查到区间的交接班数据。"
		hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(shop_id) + "，日接班操作失败了！未查到区间的交接班数据。")
		logs.Error("shop_id" +strconv.Itoa(shop_id)+"未查到区间的交接班数据。" )
		return c.Data
	}

	if num4 > 0 {

		overall_daliy_shift_count, _ = strconv.ParseInt(maps4[0]["overall_daliy_shift_count"].(string), 10, 0)
		overall_daliy_total_sell_amount, _ = strconv.ParseFloat(maps4[0]["overall_daliy_total_sell_amount"].(string), 64)
		overall_daliy_total_order_count, _ = strconv.ParseInt(maps4[0]["overall_daliy_total_order_count"].(string), 10, 0)
		overall_daliy_total_reject_order_count, _ = strconv.ParseInt(maps4[0]["overall_daliy_total_reject_order_count"].(string), 10, 0)
		overall_daliy_total_charge_amount, _ = strconv.ParseFloat(maps4[0]["overall_daliy_total_charge_amount"].(string), 64)
		overall_daliy_total_cash_amount, _ = strconv.ParseFloat(maps4[0]["overall_daliy_total_cash_amount"].(string), 64)
		overall_daliy_total_submit_cash, _ = strconv.ParseFloat(maps4[0]["overall_daliy_total_submit_cash"].(string), 64)
		overall_daliy_total_payable_cash, _ = strconv.ParseFloat(maps4[0]["overall_daliy_total_payable_cash"].(string), 64)
		overall_daliy_total_charge_count, _ = strconv.ParseInt(maps4[0]["overall_daliy_total_charge_count"].(string), 10, 0)

	}
	//获取最新一个盘点记录

	var maps5 []orm.Params
	num5, err5 := o.Raw("SELECT * FROM `gr_pos_stock_check` WHERE shop_id = ? AND createAt  > ? AND createAt < ?  ORDER BY id DESC limit 1", shop_id, overall_daliy_begin_at, overall_daliy_end_at).Values(&maps5)
	if err5 != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["message"] = "未查到最新的盘点记录。"
		hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(shop_id) + "，日接班操作失败了！未查到最新的盘点记录。")
		logs.Error("shop_id" +strconv.Itoa(shop_id)+"未查到最新的盘点记录。" )
		return c.Data
	}
	if num5 > 0 {
		overall_daliy_stock_check_id, _ = strconv.ParseInt(maps5[0]["id"].(string), 10, 0)
		overall_daliy_check_stock_count, _ = strconv.ParseInt(maps5[0]["checked_count"].(string), 10, 0)
		overall_daliy_no_check_stock_count, _ = strconv.ParseInt(maps5[0]["no_check_count"].(string), 10, 0)
		overall_daliy_profit_check_stock_count, _ = strconv.ParseInt(maps5[0]["profit_check_count"].(string), 10, 0)
		overall_daliy_loss_check_count, _ = strconv.ParseInt(maps5[0]["loss_check_count"].(string), 10, 0)
		overall_daliy_check_stock_by, _ = strconv.ParseInt(maps5[0]["create_by"].(string), 10, 0)
		overall_daliy_submit_to_correct, _ = strconv.ParseInt(maps5[0]["submit_to_correct"].(string), 10, 0)

		if maps5[0]["submit_by"] != nil || maps5[0]["submit_by"] != "" {
			overall_daliy_submit_by, _ = maps5[0]["submit_by"].(string)
		} else {
			overall_daliy_submit_by = ""
		}
	}

	res1, err7 := db_overall_daliy_insert.Exec(shop_id, overall_daliy_begin_at, overall_daliy_end_at, overall_daliy_shift_count, overall_daliy_total_sell_amount, overall_daliy_total_order_count, overall_daliy_total_reject_order_count, overall_daliy_total_charge_amount, overall_daliy_total_cash_amount, overall_daliy_total_submit_cash, overall_daliy_total_payable_cash, overall_daliy_total_charge_count, overall_daliy_stock_check_id, overall_daliy_check_stock_count, overall_daliy_no_check_stock_count, overall_daliy_profit_check_stock_count, overall_daliy_loss_check_count, overall_daliy_check_stock_by, overall_daliy_submit_to_correct, overall_daliy_submit_by)

	if err7 != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["message"] = "插入日结表数据失败。"
		hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(shop_id) + "，日接班操作失败了！插入日结表数据失败。")
		logs.Error("shop_id" +strconv.Itoa(shop_id)+"插入日结表数据失败。" )
		return c.Data
	}

	lastInsertId, _ := res1.LastInsertId()

	_, err8 := o.Raw("update gr_pos_change_shift set is_overalled = 1 where shop_id = ?", shop_id).Exec()
	if err8 != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["message"] = "修改交接班表状态失败"
		hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(shop_id) + "，日接班操作失败了！修改交接班表状态失败。")
		logs.Error("shop_id" +strconv.Itoa(shop_id)+"修改交接班表状态失败" )
		return c.Data
	}
	o.Commit()
	var maps8 []orm.Params
	o.Raw("SELECT * FROM `gr_pos_overall_daliy` WHERE id = ?", lastInsertId).Values(&maps8)
	data["overall_daliy"] = maps8

	c.Data["data"] = data
	c.Data["status"] = "success"
	c.Data["message"] = "日结执行成功。"
	logs.Error("shop_id" +strconv.Itoa(shop_id)+"日结执行成功。" )
	return c.Data
}

//交接班操作带事务的
func (c *CompleteController) changeShift(o orm.Ormer, shop_id int, create_by int, submit_cash float64, address string) (int64, bool, error) {
	logs.Error("shop_id" +strconv.Itoa(shop_id)+"交接班开始" )
	//交接班明细插入准备
	db_change_shift_insert, _ := o.Raw("INSERT INTO `gr_pos_change_shift` (`shop_id`, `begin_at`, `end_at`, `address`, `create_by`," +
		" `total_sell_amount`, `total_charge_amount`, `total_order_count`, `total_reject_order_count`, `total_charge_count`, `total_cash_amount`," +
		" `submit_cash`, `payable_cash`, `status`, `createAt`, `updateAt`, `delflag`)" +
		" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,1, "+hh.NowTime()+" ,"+hh.NowTime()+", '0')").Prepare()
	defer db_change_shift_insert.Close()

	//交接班明细金额插入
	db_change_shift_detail_insert, _ := o.Raw("INSERT INTO `gr_pos_change_shift_detail` ( `change_shift_id`, `payway_id`, `type`, `amount`, " +
		"`status`, `createAt`, `updateAt`, `delflag`) VALUES (?, ?, ?, ?, 1, unix_timestamp() ,unix_timestamp(), '0')").Prepare()
	defer db_change_shift_detail_insert.Close()

	//上下班记录表更新

	db_change_shift_record_update, _ := o.Raw("UPDATE `gr_pos_change_shift_record` SET `end_time` = unix_timestamp(), `is_end` = '1' ,`updateAt` = unix_timestamp() WHERE `shop_id` = ? and is_end = 0 and start_time= ?").Prepare()
	defer db_change_shift_record_update.Close()

	var maps []orm.Params
	var start_time int64
	num, err1 := o.Raw("SELECT * FROM `gr_pos_change_shift_record` WHERE `shop_id` = ? and is_end= 0 ORDER BY id DESC", shop_id).Values(&maps)
	if err1 != nil || num < 1 {
		logs.Error("shop_id" +strconv.Itoa(shop_id)+"未查到本店正在进行中的上班表。")
		start_time, _ = strconv.ParseInt(hh.NowTime(), 10, 0)
	} else {
		//查出开始时间和结束时间
		logs.Error("shop_id" +strconv.Itoa(shop_id)+"查出上班表中开始时间" +strconv.FormatInt(start_time,10))
		start_time, _ = strconv.ParseInt(maps[0]["start_time"].(string), 10, 0)
	}

	var maps_last_shift []orm.Params
	num_last_shift, err_last_shift := o.Raw("SELECT end_at FROM `gr_pos_change_shift` WHERE `shop_id` = ? ORDER BY id DESC  limit 1 ", shop_id).Values(&maps_last_shift) //判断时间有无重合
	if err_last_shift != nil {
		o.Rollback()
		logs.Error("shop_id" +strconv.Itoa(shop_id)+"查询上一个班次失败")
		return 0, false, errors.New("查询上一个班次失败。")
	} else if num_last_shift > 0 { //判断与上个班次之间有无订单未统计（防止断网情况出现，客户端已处理，防止极端情况）
		last_shift_end_time, _ := strconv.ParseInt(maps_last_shift[0]["end_at"].(string), 10, 64)
		var maps_unorder []orm.Params
		num_unorder, err_unorder := o.Raw("SELECT * FROM `gr_pos_order` WHERE `shop_id` = ? and createAt > ? and createAt < ? ORDER BY id DESC", shop_id, maps_last_shift[0]["end_at"], start_time).Values(&maps_unorder) //判断时间有无重合
		if err_unorder != nil {
			o.Rollback()
			logs.Error("shop_id" +strconv.Itoa(shop_id)+"查询上个班次之间有无订单失败")
			return 0, false, errors.New("查询上个班次之间有无订单失败。")
		} else if num_unorder > 0 {
			logs.Error("shop_id" +strconv.Itoa(shop_id)+"与上个班次之间有订单，开始时间设为上个班次结束时间")
			start_time = last_shift_end_time //开始时间设为上个班次结束时间
		}
	}
	var maps9 []orm.Params
	num, err9 := o.Raw("SELECT * FROM `gr_pos_change_shift_record` WHERE `shop_id` = ? and end_time > ? ORDER BY id DESC", shop_id, start_time).Values(&maps9) //判断时间有无重合
	if err9 != nil {
		o.Rollback()
		logs.Error("shop_id" +strconv.Itoa(shop_id)+"判断时间有无重合失败")
		return 0, false, errors.New("判断时间有无重合失败。")
	} else if num > 0 {
		_, err9 := o.Raw("update `gr_pos_change_shift_record` set start_time = ? where id = ? ", maps9[0]["end_time"], maps[0]["id"]).Exec() //先把原纪录删除
		if err9 != nil {
			o.Rollback()
			logs.Error("shop_id" +strconv.Itoa(shop_id)+"修改时间失败。")
			return 0, false, errors.New("修改时间失败。")
		}
		logs.Error("shop_id" +strconv.Itoa(shop_id)+"修改时间失败。")
		start_time, _ = strconv.ParseInt(maps9[0]["end_time"].(string), 10, 0)
	}

	end_time, _ := strconv.ParseInt(hh.NowTime(), 10, 0)

	logs.Error("计算——开始时间" +strconv.FormatInt(start_time,10))
	logs.Error("计算——结束时间" +strconv.FormatInt(end_time,10))
	var total_charge_amount float64      //充值总金额
	var total_charge_cash_amount float64 //充值现金总金额
	var total_charge_count int           //总充值笔数

	var total_cash_amount float64 //应收现金总额

	//充值数据查询
	var maps2 []orm.Params
	num2, err2 := o.Raw("SELECT sum(a.final_amount) as final_amount,a.payway_id,b.name FROM `gr_pos_member_charge_order` a LEFT JOIN `gr_pos_payway` b ON a.payway_id =b.id  WHERE a.shop_id=? and a.createAt > ? and a.createAt < ? GROUP BY a.payway_id", shop_id, start_time, end_time).Values(&maps2)

	if err2 != nil {
		o.Rollback()
		logs.Error("shop_id" +strconv.Itoa(shop_id)+"本地充值数据查询失败。")
		return 0, false, errors.New("本地充值数据查询失败。")
	}

	if num2 > 0 {
		//有充值汇总数据
		total_charge_count = len(maps2)
		for i := 0; i < len(maps2); i++ {

			final_amount, _ := strconv.ParseFloat(maps2[i]["final_amount"].(string), 64)
			payway_id, _ := strconv.ParseInt(maps2[i]["payway_id"].(string), 10, 0)

			if payway_id == 1 || payway_id == 2 {
				total_charge_cash_amount = total_charge_cash_amount + final_amount //累计现金充值金额
			}

			total_charge_amount = total_charge_amount + final_amount //累计充值总金额
		}
	}

	var total_sell_amount float64      //销售总金额
	var total_sell_cash_amount float64 //销售现金总金额
	var total_order_count int64        //正单数量
	var total_reject_order_count int64 //退单数量

	//订单总销售额汇总
	var maps3 []orm.Params
	num3, err3 := o.Raw("SELECT SUM(a.payway_amount) as payway_sum_amount,a.payway_id,b.name,c.type "+
		"FROM `gr_pos_order_pay` a "+
		"LEFT JOIN `gr_pos_payway` b ON a.payway_id = b.id "+
		"LEFT JOIN `gr_pos_order` c ON a.order_num = c.order_num  "+
		"WHERE c.shop_id=? and a.createAt > ? and a.createAt < ? "+
		"GROUP BY a.payway_id ,c.type", shop_id, start_time, end_time).Values(&maps3)
	beego.Error(maps3)
	if err3 != nil {
		o.Rollback()
		logs.Error("shop_id" +strconv.Itoa(shop_id)+ "订单支付数据查询失败。")
		return 0, false, errors.New("订单支付数据查询失败。")
	}

	if num3 > 0 {
		for i := 0; i < len(maps3); i++ {

			payway_sum_amount, _ := strconv.ParseFloat(maps3[i]["payway_sum_amount"].(string), 64)
			payway_id, _ := strconv.ParseInt(maps3[i]["payway_id"].(string), 10, 0)

			if payway_id == 1 || payway_id == 3 {

				total_sell_cash_amount = total_sell_cash_amount + payway_sum_amount //累计现金销售金额

			}

			total_sell_amount = total_sell_amount + payway_sum_amount //累计销售总金额
		}
	}

	//订单真单数量，退单数量
	var maps4 []orm.Params
	num4, err4 := o.Raw("SELECT type,count(1) as num FROM `gr_pos_order` WHERE shop_id=? and createAt > ? and createAt < ? GROUP BY type ", shop_id, start_time, end_time).Values(&maps4)

	if err4 != nil {
		o.Rollback()
		logs.Error("shop_id" +strconv.Itoa(shop_id)+"订单类型数据查询失败。")
		return 0, false, errors.New("订单类型数据查询失败。")
	}

	if num4 > 0 {
		for i := 0; i < len(maps4); i++ {
			order_type, _ := strconv.ParseInt(maps4[i]["type"].(string), 10, 0)
			order_type_num, _ := strconv.ParseInt(maps4[i]["num"].(string), 10, 0)

			if order_type == 1 {
				total_order_count = order_type_num
			}
			if order_type == 2 {
				total_reject_order_count = order_type_num
			}
		}
	}

	total_cash_amount = total_sell_cash_amount + total_charge_cash_amount

	var payable_cash float64 //应交现金总嗯

	if submit_cash > total_cash_amount {
		payable_cash = submit_cash
	} else {
		payable_cash = total_cash_amount
	}

	//以上各种都计算好了，开始插入数据
	res, err5 := db_change_shift_insert.Exec(shop_id, start_time, end_time, address, create_by, total_sell_amount, total_charge_amount, total_order_count, total_reject_order_count, total_charge_count, total_cash_amount, submit_cash, payable_cash)

	if err5 != nil {
		o.Rollback()
		logs.Error("shop_id" +strconv.Itoa(shop_id)+ "交接班数据插入失败。")
		return 0, false, errors.New("日结数据插入失败。")
	}

	change_shift_id, err6 := res.LastInsertId()
	if err6 != nil {
		o.Rollback()
		logs.Error("shop_id" +strconv.Itoa(shop_id)+"交接班数据插入失败2。")
		return 0, false, errors.New("日结数据插入失败2。")
	}

	//开始插入充值交接班明细
	for i := 0; i < len(maps2); i++ {

		final_amount, _ := strconv.ParseFloat(maps2[i]["final_amount"].(string), 64)
		payway_id, _ := strconv.ParseInt(maps2[i]["payway_id"].(string), 10, 0)

		_, err7 := db_change_shift_detail_insert.Exec(change_shift_id, payway_id, 3, final_amount)

		if err7 != nil {
			o.Rollback()
			logs.Error("shop_id" +strconv.Itoa(shop_id)+"交接班支付明细插入失败。")
			return 0, false, errors.New("交接班支付明细插入失败。")
		}

	}
	//开始插入 订单交接班明细

	for i := 0; i < len(maps3); i++ {

		payway_sum_amount, _ := strconv.ParseFloat(maps3[i]["payway_sum_amount"].(string), 64)
		payway_id, _ := strconv.ParseInt(maps3[i]["payway_id"].(string), 10, 0)

		order_type, _ := strconv.ParseInt(maps3[i]["type"].(string), 10, 0)

		temp_type := -1

		if order_type == 1 {
			temp_type = 1
		}

		if order_type == 2 {
			temp_type = 2
		}

		_, err7 := db_change_shift_detail_insert.Exec(change_shift_id, payway_id, temp_type, payway_sum_amount)

		if err7 != nil {
			o.Rollback()
			logs.Error("shop_id" +strconv.Itoa(shop_id)+"日结支付明细插入失败。")
			return 0, false, errors.New("日结支付明细插入失败。")
		}
	}

	//班次结束状态修改
	_, err8 := db_change_shift_record_update.Exec(shop_id, start_time)
	if err8 != nil {
		o.Rollback()
		logs.Error("shop_id" +strconv.Itoa(shop_id)+ "班次结束失败。")
		return 0, false, errors.New("班次结束失败。")
	}

	return change_shift_id, true, nil
}