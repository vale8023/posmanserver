package posman

import (
	"github.com/astaxie/beego/orm"
	"freshlife.io/PosmanServer/hh"

	"math"
	"strconv"

	"time"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
)

//code 2000
type SynchController struct {
	BaseController
}

//下行同步接口
func (c *SynchController) DownDataTest(event hh.Event) map[string]interface{} {
	c.Prepare()
	clientrowversion, _ := event.Content.Data.Get("client_row_version").Int()

	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM ws_qq WHERE row_version > ?", clientrowversion).Values(&maps)
	if err != nil {
		return nil
	}

	if num > 0 {
		c.Data["status"] = "success"
		c.Data["num"] = num
		c.Data["data"] = maps
		c.Data["message"] = "数据同步成功"
	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "数据同步失败"
	}

	return c.Data
}

//上行同步接口
func (c *SynchController) UpDataTest(event hh.Event) map[string]interface{} {

	c.Prepare()
	o := orm.NewOrm()
	//数据行数
	//rownumber ,_ := event.Content.Data.Get("num").Int()

	//数据包
	data := event.Content.Data.Get("data")
	row, _ := data.Array()

	//sql预先准备
	db_order, _ := o.Raw("INSERT INTO `ws_order` (`order_id`, `order_amount`, `order_address`, `shop_id`, `row_version`) VALUES (?, ?, ?, ?,?)").Prepare()
	defer db_order.Close()
	db_order_goods, _ := o.Raw("INSERT INTO `ws_order_goods` (`order_id`, `goods_name`, `goods_num`, `goods_amount`) VALUES (?,?,?,?)").Prepare()
	defer db_order_goods.Close()

	//循环遍历数据

	var successdataarray []map[string]interface{}
	successdata := make(map[string]interface{})

	for i := 0; i < len(row); i++ {
		o := orm.NewOrm()
		o.Begin()
		order := data.GetIndex(i)
		oid, _ := order.Get("order_id").String()
		amount, _ := order.Get("order_amount").Float64()
		address, _ := order.Get("order_address").String()
		sid, _ := order.Get("shop_id").Int64()
		rowverison, _ := order.Get("row_version").Int64()

		ordergoods := order.Get("order_goods")

		//插入订单
		_, err := db_order.Exec(oid, amount, address, sid, rowverison)
		if err == nil {

			//插入订单明细
			oga, _ := ordergoods.Array()
			ordergoodsunmber := len(oga)

			for j := 0; j < ordergoodsunmber; j++ {
				goods := ordergoods.GetIndex(j)
				goods_name, _ := goods.Get("goods_name").String()
				goods_num, _ := goods.Get("goods_num").Int64()
				goods_amount, _ := goods.Get("goods_amount").Float64()
				_, err := db_order_goods.Exec(oid, goods_name, goods_num, goods_amount)
				if err != nil {
					//明细插入遇到故障
					o.Rollback()
				}
			}

			o.Commit()
			successdata["order_id"] = oid
			successdata["row_version"] = rowverison
			successdataarray = append(successdataarray, successdata)
		} else {
			//已经存在
			o.Rollback()
			successdata["order_id"] = oid
			successdata["row_version"] = rowverison
			successdataarray = append(successdataarray, successdata)
		}
	}

	num := len(successdataarray)

	if num > 0 {
		c.Data["status"] = "success"
		c.Data["num"] = num
		c.Data["data"] = successdataarray
		c.Data["message"] = "数据同步成功"

	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "数据同步失败"
	}

	return c.Data
}

//下载商品分类数据同步接口
func (c *SynchController) DownProductCategor(event hh.Event) map[string]interface{} {
	c.Prepare()
	clientrowversion, _ := event.Content.Data.Get("client_row_version").Int()
	ctype, _ := event.Content.Data.Get("client_mode").Int()
	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM gr_pos_product_category WHERE row_version > ? and type=?", clientrowversion, ctype).Values(&maps)
	if err != nil {
		return nil
	}

	if num > 0 {
		c.Data["status"] = "success"
		c.Data["num"] = num
		c.Data["data"] = maps
		c.Data["message"] = "数据同步成功"
	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "数据同步失败"
	}

	return c.Data
}

//下载商品数据同步接口
func (c *SynchController) DownProduct(event hh.Event) map[string]interface{} {
	c.Prepare()
	clientrowversion, checkerr1 := event.Content.Data.Get("client_row_version").Int()
	//ctype , checkerr2:= event.Content.Data.Get("client_mode").Int()
	cshopid, checkerr3 := event.Content.Data.Get("shopid").Int()

	if checkerr1 != nil || checkerr3 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	//num, err := o.Raw("SELECT a.*,b.name AS category_name " +
	//							"FROM gr_pos_product a " +
	//							"INNER JOIN gr_pos_product_category b ON a.category_id =b.id  " +
	//							"WHERE a.row_version > ? and b.type=?",clientrowversion,ctype).Values(&maps)

	//num, err := o.Raw("select c.id,c.`code`,c.category_id,c.`name`,c.spec_name,c.min_spec_num,c.unit,c.instock_price,a.sell_price, "+
	//	"a.min_sell_price,c.cup_tip,c.row_version,c.`status`,c.createAt,c.updateAt,c.delflag,c.weighing_needed "+
	//	"from gr_pos_city_price a "+
	//	"left join gr_pos_shop b on a.city_id = b.city_id and a.price_attr_id = b.price_attr_id "+
	//	"LEFT JOIN gr_pos_product c on a.product_id = c.id "+
	//	"where b.id = ? and c.status = 1 and c.row_version > ?", cshopid, clientrowversion).Values(&maps)
	
	num, err := o.Raw("select c.id,c.`code`,c.category_id,c.`name`,c.spec_name,c.min_spec_num,c.unit,c.instock_price,a.sell_price, "+
		"0 min_sell_price,c.cup_tip,a.row_version,a.`status`,c.createAt,c.updateAt,a.delflag,c.weighing_needed "+
		"from gr_pos_product_shop_permission a "+
		"left join gr_pos_shop b  on a.shop_id = b.id "+
		"LEFT JOIN gr_pos_product c on a.product_id = c.id "+
		"where b.id = ?    and c.type = 1 and a.row_version > ?", cshopid, clientrowversion).Values(&maps)

	// num, err := o.Raw("SELECT *  FROM gr_pos_product a " +
	// 	"WHERE ((a.is_self_product = 1 AND a.shop_id = 0) " +
	// 	"OR (a.is_self_product = 0 AND a.shop_id = ?)) and  a.row_version > ? ", cshopid, clientrowversion).Values(&maps)

	if err != nil {
		return nil
	}

	if num > 0 {
		c.Data["status"] = "success"
		c.Data["num"] = num
		c.Data["data"] = maps
		c.Data["message"] = "数据同步成功"
	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "数据同步失败"
	}

	return c.Data
}

//下载商品库存数据同步接口
func (c *SynchController) DownShopStock(event hh.Event) map[string]interface{} {
	c.Prepare()
	clientrowversion, checkerr1 := event.Content.Data.Get("client_row_version").Int()
	shopid, checkerr2 := event.Content.Data.Get("client_shop_id").Int()

	if checkerr1 != nil || checkerr2 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM gr_pos_shop_stock WHERE row_version > ? and shop_id=?", clientrowversion, shopid).Values(&maps)
	if err != nil {
		return nil
	}

	if num > 0 {
		c.Data["status"] = "success"
		c.Data["num"] = num
		c.Data["data"] = maps
		c.Data["message"] = "数据同步成功"
	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "数据同步失败"
	}

	return c.Data
}

//下载支付方式数据同步接口
func (c *SynchController) DownPayWay(event hh.Event) map[string]interface{} {
	c.Prepare()
	clientrowversion, checkerr1 := event.Content.Data.Get("client_row_version").Int()
	shopid, checkerr2 := event.Content.Data.Get("client_shop_id").Int()

	if checkerr1 != nil || checkerr2 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM gr_pos_payway WHERE row_version > ? and shop_id=? OR shop_id=0 ", clientrowversion, shopid).Values(&maps)
	if err != nil {
		return nil
	}

	if num > 0 {
		c.Data["status"] = "success"
		c.Data["num"] = num
		c.Data["data"] = maps
		c.Data["message"] = "数据同步成功"
	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "数据同步失败"
	}

	return c.Data
}

//订单上传接口
func (c *SynchController) UpOrderPackage(event hh.Event) map[string]interface{} {
	t1 := time.Now() // get current time

	beego.Error("UpOrderPackage 开始啦")

	c.Prepare()

	order_package_num, checkerr1 := event.Content.Data.Get("order_package_num").Int()
	order_package := event.Content.Data.Get("order_package")
	order_package_array, checkerr2 := order_package.Array()

	if checkerr1 != nil || checkerr2 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()

	var order_success_array = make([]string, 0)

	for i := 0; i < len(order_package_array); i++ {

		thisOrderCanResume := true

		order := order_package.GetIndex(i)
		order_order_num, _ := order.Get("order_num").String()
		order_shop_id, _ := order.Get("shop_id").Int()
		order_type, _ := order.Get("type").Int()
		order_create_by, _ := order.Get("create_by").Int()
		order_member_id, _ := order.Get("member_id").Int()
		order_member_type, _ := order.Get("member_type").Int()
		order_product_count, _ := order.Get("product_count").Float64()
		order_origin_price, _ := order.Get("origin_price").Float64()
		order_final_price, _ := order.Get("final_price").Float64()
		order_final_cost, _ := order.Get("final_cost").Float64() //订单的成本
		order_profit, _ := order.Get("profit").Float64()         //订单的利润
		order_is_discount, _ := order.Get("is_discount").Int()
		order_is_group_pay, _ := order.Get("is_group_pay").Int()
		order_remark, _ := order.Get("remark").String()
		order_origin_order_num, _ := order.Get("origin_order_num").String()
		discount_price, _ := order.Get("discount_price").Float64()
		promotion_type, _ := order.Get("promotion_type").Float64()
		order_couponid, _ := order.Get("couponid").Float64()
		promotionid, _ := order.Get("promotionid").Float64()
		small_change, _ := order.Get("small_change").Float64()
		order_scan_field, _ := order.Get("scan_field").String()
		order_scan_info, _ := order.Get("scan_info").String()
		miniapp_order_couponid, _ := order.Get("coupon_id").Int()

		var maps []orm.Params
		num, err := o.Raw("SELECT * FROM `gr_pos_order` WHERE `shop_id` = ? AND `order_num` = ?", order_shop_id, order_order_num).Values(&maps)
		if err != nil {

			logs.Error(-2001, "同步订单编号:", order_order_num, "订单查询过程中报错了。")
			logs.Error(order)
			hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(order_shop_id) + "，线下订单同步失败啦，订单号：" + order_order_num + "，错误码：2001")
			thisOrderCanResume = false //发现错误本条订单暂停执行
			continue
		}

		if num > 0 {
			//这个订单存在了
			order_success_array = append(order_success_array, order_order_num)
		} else {
			beego.Error("UpOrderPackage 开始处理订单啦！")
			//连接数据库准备查询
			o := orm.NewOrm()
			o.Begin()

			//SQL准备
			now_time := hh.NowTime()
			db_order_insert, _ := o.Raw("INSERT INTO `gr_pos_order` (`shop_id`, `order_num`, `type`, `create_by`, `member_id`,`member_type` ," +
				" `product_count`, `origin_price`, `final_price`, `final_cost`, `profit`, `is_discount`, `is_group_pay`, " +
				"`remark`,`origin_order_num`,`discount_price`,`promotion_type`,`promotionid`,coupon_id,`small_change`, `status`, `createAt`, `updateAt`, `delflag`) VALUES" +
				" (?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?,?,?,?,?, ?,'1', ?, ?, '0')").Prepare()
			//beego.Error(err1111)
			defer db_order_insert.Close()

			db_order_goods_insert, _ := o.Raw("INSERT INTO `gr_pos_order_goods` (`order_num`, `product_id`, `product_name`, `count`, " +
				"`origin_single_price`, `finnal_single_price`, `final_single_cost`, `origin_total_price`, `finnal_total_price`, `profit`," +
				"`activity_type`, `activity_id`, `is_group`, coupon_id," +
				"`status`, `createAt`, `updateAt`, `delflag`) VALUES (?, ?, ?, ?, " +
				"?, ?, ?, ?, ?, ?,?,?,?,?,1, " + now_time + ", " + now_time + ", '0')").Prepare()
			defer db_order_goods_insert.Close()

			db_order_pay_insert, _ := o.Raw("INSERT INTO `gr_pos_order_pay` (`order_num`, `payway_id`, `payway_amount`, " +
				"`status`, `createAt`, `updateAt`, `delflag`) VALUES (?, ?, ?, '1', " + now_time + ", " + now_time + ", '0')").Prepare()
			defer db_order_pay_insert.Close()

			db_order_update, _ := o.Raw("UPDATE `gr_pos_order` SET `final_cost` = ?, `profit` = ? WHERE `gr_pos_order`.`order_num` = ?").Prepare()
			defer db_order_update.Close()

			////2.更新出库商品的总表
			db_out_stock_update, _ := o.Raw("UPDATE `gr_pos_shop_stock` SET `count` = ?,  `stock_total_price` = ?, `updateAt` = unix_timestamp(),`row_version` = unix_timestamp() WHERE `gr_pos_shop_stock`.`product_id` = ? and `gr_pos_shop_stock`.`shop_id` = ?").Prepare()
			defer db_out_stock_update.Close()
			//3.更新出库商品明细表
			db_out_stock_detail_insert, _ := o.Raw("INSERT INTO `gr_pos_shop_stock_detail` (`shop_id`, `product_id`, `origin_stock`, `change_count`, `current_count`, `type`, `order_num`, `stock_single_price`, `origin_stock_total_price`, `change_stock_total_price`, `current_stock_total_price`, `status`, `createAt`, `updateAt`, `delflag`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1, unix_timestamp() ,unix_timestamp(), '0')").Prepare()
			defer db_out_stock_detail_insert.Close()
			////入库的语句

			////5.更新入库商品的总表
			db_in_stock_update, _ := o.Raw("UPDATE `gr_pos_shop_stock` SET `count` = ?, `stock_single_price` = ?, `stock_total_price` = ?, `updateAt` = unix_timestamp(),`row_version` = unix_timestamp() WHERE `gr_pos_shop_stock`.`product_id` = ? and `gr_pos_shop_stock`.`shop_id` = ?").Prepare()
			defer db_in_stock_update.Close()

			//5.1 插入商品总表
			//db_in_stock_insert,_:= o.Raw("INSERT INTO `gr_pos_shop_stock` (`shop_id`, `product_id`, `product_category_id`, `count`, `stock_single_price`, `stock_total_price`, `first_instock_at`, `recent_instock_at`, `recent_outstock_at`, `sale_display`, `status`, `createAt`, `updateAt`, `delflag`, `row_version`) VALUES (?, ?, ?, ?, ?, ?, unix_timestamp() , null, null, 0,1, unix_timestamp() ,unix_timestamp(), '0', unix_timestamp())").Prepare()
			//defer db_in_stock_insert.Close()

			////6.更新入库商品明细表
			db_in_stock_detail_insert, _ := o.Raw("INSERT INTO `gr_pos_shop_stock_detail` (`shop_id`, `product_id`, `origin_stock`, `change_count`, `current_count`, `type`, `order_num`, `stock_single_price`, `origin_stock_total_price`, `change_stock_total_price`, `current_stock_total_price`, `status`, `createAt`, `updateAt`, `delflag`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  1, unix_timestamp() ,unix_timestamp(), '0');").Prepare()
			defer db_in_stock_detail_insert.Close()

			////7.订单商品组合表插入
			db_goods_group_insert, _ := o.Raw("INSERT INTO `gr_pos_order_goods_group` (`order_num`, `order_goods_id`, `product_id`, `product_name`, `single_price`, `single_cost`, `count`, `total_price`, `status`, `createAt`, `updateAt`, `delflag`) VALUES (?, ?, ?, ?, ?, ?, ?, ?,  1, unix_timestamp() ,unix_timestamp(), '0');").Prepare()
			defer db_goods_group_insert.Close()

			//这个订单不存在
			//插入订单

			//整单优惠券
			if order_couponid > 0 {
				db_user_coupon_update, _ := o.Raw("UPDATE `gr_ec`.`gr_user_coupon` SET `used` = 1, `useddate` = unix_timestamp(), `updateAt` = unix_timestamp() WHERE `id` = ?").Prepare()
				defer db_user_coupon_update.Close()

				_, err := db_user_coupon_update.Exec(order_couponid)

				if err != nil {
					o.Rollback()
					logs.Error(-3001, "同步订单编号:", order_order_num, "修改优惠券信息失败。")
					hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(order_shop_id) + "，线下订单同步失败啦，订单号：" + order_order_num + "，错误码：3001")
					logs.Error(order)
					thisOrderCanResume = false //发现错误本条订单暂停执行
					continue
				}
			}

			if miniapp_order_couponid > 0 { //小程序会员
				db_user_coupon_update, _ := o.Raw("UPDATE `gr_ec_user_coupon` SET used_type = 2,order_num=?,`status` = 2, `update_at` = unix_timestamp() WHERE `id` = ?").Prepare()
				defer db_user_coupon_update.Close()

				_, err := db_user_coupon_update.Exec(order_order_num,miniapp_order_couponid)

				if err != nil {
					o.Rollback()
					logs.Error(-3001, "同步订单编号:", order_order_num, "修改会员优惠券信息失败。")
					hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(order_shop_id) + "，线下订单同步失败啦，订单号：" + order_order_num + "，错误码：3001")
					logs.Error(order)
					thisOrderCanResume = false //发现错误本条订单暂停执行
					continue
				}
			}

			//营销活动处理
			//beego.Error(promotion_type)
			if promotion_type == 0 && promotionid > 0 {
				_, err9 := o.Raw("UPDATE gr_pos_maketing_order SET sell_count = sell_count + 1, `updateAt` = unix_timestamp() "+
					"WHERE id = ?", promotionid).Exec()
				if err9 != nil {
					o.Rollback()
					logs.Error(-2018, "修改活动参与表失败")
					hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(order_shop_id) + "，线下订单同步失败啦，订单号：" + order_order_num + "，错误码：2018")
					logs.Error(order)
					thisOrderCanResume = false //发现错误本条订单暂停执行
					break
				}

				_, err10 := o.Raw("INSERT INTO gr_pos_maketing_record "+
					"(type, maketing_id, order_num,product_id, scan_field, scan_info, status, createAt, updateAt, delflag) "+
					"VALUES ( 2, ?, ?,0, ?, ?, 1, unix_timestamp(), unix_timestamp(), 0)", promotionid, order_order_num, order_scan_field, order_scan_info).Exec()
				if err10 != nil {
					o.Rollback()
					logs.Error(-2019, "插入活动记录表失败")
					hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(order_shop_id) + "，线下订单同步失败啦，订单号：" + order_order_num + "，错误码：2019")
					logs.Error(order)
					thisOrderCanResume = false //发现错误本条订单暂停执行
					break
				}
			}
			_, err := db_order_insert.Exec(order_shop_id, order_order_num, order_type, order_create_by, order_member_id, order_member_type,
				order_product_count, order_origin_price, order_final_price, order_final_cost, order_profit,
				order_is_discount, order_is_group_pay, order_remark, order_origin_order_num, discount_price,
				promotion_type, promotionid, miniapp_order_couponid, small_change, now_time, now_time)

			if err != nil {

				o.Rollback()
				logs.Error(-2002, "同步订单编号:", order_order_num, "订单插入过程中报错了。")
				hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(order_shop_id) + "，线下订单同步失败啦，订单号：" + order_order_num + "，错误码：2002")
				logs.Error(order)
				thisOrderCanResume = false //发现错误本条订单暂停执行
				continue
			}

			//获取订单详情
			order_goods := order.Get("goods")
			order_goods_array, _ := order_goods.Array()
			var order_goods_id int64
			//遍历商品明
			for j := 0; j < len(order_goods_array); j++ {
				good := order_goods.GetIndex(j)
				good_order_num, _ := good.Get("order_num").String()
				good_product_id, _ := good.Get("product_id").Int()
				good_product_name, _ := good.Get("product_name").String()
				good_count, _ := good.Get("count").Float64()
				good_origin_single_price, _ := good.Get("origin_single_price").Float64()
				good_finnal_single_price, _ := good.Get("finnal_single_price").Float64()
				good_origin_total_price, _ := good.Get("origin_total_price").Float64()
				good_finnal_total_price, _ := good.Get("finnal_total_price").Float64()

				good_final_single_cost, _ := good.Get("final_single_cost").Float64() //获取传入的成本
				good_profit, _ := good.Get("profit").Float64()                       //获取传入的利润
				good_activity_type, _ := good.Get("activity_type").Int()
				good_activity_id, _ := good.Get("activity_id").Int()
				good_scan_field, _ := good.Get("scan_field").String()
				good_scan_info, _ := good.Get("scan_info").String()
				good_is_group, _ := good.Get("is_group").Float64()
				jump_stock_operation, _ := good.Get("jump_stock_operation").Int() //判断传过来的商品明细是否是 果汁。。等无库存商品类型
				miniapp_goods_coupon_id, _ := good.Get("coupon_id").Int()         //优惠券

				if order_type == 1 && jump_stock_operation == 0 {
					good_final_single_cost, _ = getStockProductPrice(order_shop_id, good_product_id)   //获取单个商品的成本价格
					good_profit = good_finnal_total_price - good_final_single_cost*float64(good_count) //计算单笔的利润
					order_final_cost = order_final_cost + good_final_single_cost*float64(good_count)
				}

				if order_type == 2 && jump_stock_operation == 0 {
					good_profit = good_finnal_total_price - good_final_single_cost*float64(good_count) //计算单笔的利润
				}

				sql_result, err2 := db_order_goods_insert.Exec(good_order_num, good_product_id, good_product_name, good_count,
					good_origin_single_price, good_finnal_single_price, good_final_single_cost, good_origin_total_price,
					good_finnal_total_price, good_profit, good_activity_type, good_activity_id,
					good_is_group, miniapp_goods_coupon_id)

				order_goods_id, _ = sql_result.LastInsertId() //订单商品表id
				if err2 != nil {
					o.Rollback()
					logs.Error(-2003, "同步订单编号:", order_order_num, "订单明细插入过程中报错了。")
					hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(order_shop_id) + "，线下订单同步失败啦，订单号：" + order_order_num + "，错误码：2003")
					logs.Error(order)
					thisOrderCanResume = false //发现错误本条订单暂停执行
					break
				}
				//如果是果汁这种无库存类型的商品跳过出入库操作

				//4.活动商品处理
				if good_activity_type == 2 {
					n := orm.NewOrm()
					n.Using("ECV3")
					var maps1 []orm.Params
					num1, err1 := n.Raw("SELECT a.uid,b.id,a.orderId "+
						"FROM gr_activity_draw_order a "+
						"LEFT JOIN gr_activity_draw_info b on a.specId = b.productId "+
						"WHERE a.id = ?", good_activity_id).Values(&maps1)

					if err1 != nil || num1 == 0 {
						o.Rollback()
						logs.Error(-3000, "同步订单编号:", order_order_num, "查询活动商品信息失败。")
						hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(order_shop_id) + "，线下订单同步失败啦，订单号：" + order_order_num + "，错误码：3000")
						logs.Error(order)
						thisOrderCanResume = false //发现错误本条订单暂停执行
						continue
					}

					uid := maps1[0]["uid"]
					belongToActId := maps1[0]["id"]
					activity_orderid := maps1[0]["orderId"]

					_, err3 := o.Raw("update gr_ec.gr_activity_draw_order set shopId = ?, status = 2 where id = ?", order_shop_id, good_activity_id).Exec()
					if err3 != nil {
						o.Rollback()
						logs.Error(-3001, "同步订单编号:", order_order_num, "更改真会玩订单状态失败")
						hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(order_shop_id) + "，线下订单同步失败啦，订单号：" + order_order_num + "，错误码：3001")
						logs.Error(order)
						thisOrderCanResume = false //发现错误本条订单暂停执行
						continue
					}
					_, err4 := o.Raw("INSERT INTO gr_ec.gr_activity_draw_drawtimes ( uid, belongToActId, orderId, status, createAt, updateAt, delflag)  VALUES (?, ?, ?, 1, unix_timestamp(), unix_timestamp(), 0)", uid, belongToActId, activity_orderid).Exec()
					if err4 != nil {
						o.Rollback()
						logs.Error(-3002, "同步订单编号:", order_order_num, "增加抽奖次数失败")
						hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(order_shop_id) + "，线下订单同步失败啦，订单号：" + order_order_num + "，错误码：3002")
						logs.Error(order)
						thisOrderCanResume = false //发现错误本条订单暂停执行
						continue
					}
				}

				//营销活动处理
				if good_activity_type == 0 && good_activity_id > 0 {
					_, err7 := o.Raw("UPDATE gr_pos_maketing_goods_detail SET sell_count = sell_count + ?, `updateAt` = unix_timestamp() "+
						"WHERE maketing_goods_id = ? and product_id = ?", good_count, good_activity_id, good_product_id).Exec()
					if err7 != nil {
						o.Rollback()
						logs.Error(-2018, "修改活动参与表失败")
						hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(order_shop_id) + "，线下订单同步失败啦，订单号：" + order_order_num + "，错误码：2018")
						logs.Error(order)
						thisOrderCanResume = false //发现错误本条订单暂停执行
						break
					}

					_, err8 := o.Raw("INSERT INTO gr_pos_maketing_record "+
						"(type, maketing_id, order_num,product_id, scan_field, scan_info, status, createAt, updateAt, delflag) "+
						"VALUES ( 1, ?, ?,?, ?, ?, 1, unix_timestamp(), unix_timestamp(), 0)", good_activity_id, order_order_num, good_product_id, good_scan_field, good_scan_info).Exec()
					if err8 != nil {
						o.Rollback()
						logs.Error(-2019, "插入活动记录表失败")
						hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(order_shop_id) + "，线下订单同步失败啦，订单号：" + order_order_num + "，错误码：2019")
						logs.Error(order)
						thisOrderCanResume = false //发现错误本条订单暂停执行
						break
					}
				}

				if miniapp_goods_coupon_id > 0 { //有优惠券
					db_user_coupon_update, _ := o.Raw("UPDATE `gr_ec_user_coupon` SET used_type = 2,order_num=?,`status` = 2, `update_at` = unix_timestamp() WHERE `id` = ?").Prepare()
					defer db_user_coupon_update.Close()

					_, err := db_user_coupon_update.Exec(good_order_num,miniapp_goods_coupon_id)

					if err != nil {
						o.Rollback()
						logs.Error(-3001, "同步订单编号:", order_order_num, "修改会员优惠券信息失败。")
						hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(order_shop_id) + "，线下订单同步失败啦，订单号：" + order_order_num + "，错误码：3001")
						logs.Error(order)
						thisOrderCanResume = false //发现错误本条订单暂停执行
						continue
					}
				}

				if jump_stock_operation == 1 {
					continue
				}

				if order_type == 1 {
					//是销售订单
					//读取当前库存
					var maps1 []orm.Params
					num1, err1 := o.Raw("SELECT * FROM `gr_pos_shop_stock` WHERE `shop_id` =? AND `product_id` =?", order_shop_id, good_product_id).Values(&maps1)

					var out_stock_single_price, out_stock_total_price, out_stock_count, out_count_total_price float64

					if err1 == nil && num1 > 0 {
						out_stock_single_price, _ = strconv.ParseFloat(maps1[0]["stock_single_price"].(string), 64)
						out_stock_total_price, _ = strconv.ParseFloat(maps1[0]["stock_total_price"].(string), 64)
						out_stock_count, _ = strconv.ParseFloat(maps1[0]["count"].(string), 64)
					} else {

						o.Rollback()
						logs.Error(2004, "同步订单编号:", good_order_num, "出库商品总表查询失败。")
						hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(order_shop_id) + "，线下订单同步失败啦，订单号：" + order_order_num + "，错误码：2004")
						logs.Error(order)
						thisOrderCanResume = false //发现错误本条订单暂停执行
						break
					}

					//修改商品总库存表
					//本次出库总成本
					out_count_total_price = float64(good_count) * out_stock_single_price
					//本次出库后 数量
					out_stock_count_after := out_stock_count - float64(good_count)
					//本次出库后 总金额
					out_stock_total_price_after := out_stock_total_price - out_count_total_price

					////2.更新出库商品的总表
					_, err2 := db_out_stock_update.Exec(out_stock_count_after, out_stock_total_price_after, good_product_id, order_shop_id)
					if err2 != nil {
						o.Rollback()
						logs.Error(-2005, "同步订单编号:", order_order_num, "更新出库总表数据执行失败。")
						hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(order_shop_id) + "，线下订单同步失败啦，订单号：" + order_order_num + "，错误码：2005")
						logs.Error(order)
						thisOrderCanResume = false //发现错误本条订单暂停执行
						break
					}

					//3.更新出库商品明细表
					_, err3 := db_out_stock_detail_insert.Exec(order_shop_id, good_product_id, out_stock_count, good_count, out_stock_count_after, 2000, good_order_num, out_stock_single_price, out_stock_total_price, out_count_total_price, out_stock_total_price_after)
					if err3 != nil {
						o.Rollback()
						logs.Error(-2006, "同步订单编号:", order_order_num, "插入出库明细表数据执行失败。")
						hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(order_shop_id) + "，线下订单同步失败啦，订单号：" + order_order_num + "，错误码：2006")
						logs.Error(order)
						thisOrderCanResume = false //发现错误本条订单暂停执行
						break
					}

				}

				if order_type == 2 {
					//是退货订单
					var maps2 []orm.Params
					num4, err4 := o.Raw("SELECT * FROM `gr_pos_shop_stock` WHERE `shop_id` =? AND `product_id` =?", order_shop_id, good_product_id).Values(&maps2)

					if err4 != nil || num4 == 0 {
						o.Rollback()
						logs.Error(-2007, "同步订单编号:", order_order_num, "入商品总表查询失败。")
						hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(order_shop_id) + "，线下订单同步失败啦，订单号：" + order_order_num + "，错误码：2007")
						logs.Error(order)
						thisOrderCanResume = false //发现错误本条订单暂停执行
						break

					}

					var in_stock_total_price, in_stock_count, in_stock_count_after, in_stock_total_price_after, in_stock_single_price_after float64
					//入库商品存在
					in_stock_total_price, _ = strconv.ParseFloat(maps2[0]["stock_total_price"].(string), 64) //总价格
					in_stock_count, _ = strconv.ParseFloat(maps2[0]["count"].(string), 64)                   //总数量
					//本次入库后 数量
					in_stock_count_after = in_stock_count + float64(good_count)
					//本次入库后 总金额
					in_stock_total_price_after = in_stock_total_price + math.Abs(good_final_single_cost)*float64(good_count) //原总成本 + 新增加成本
					//本次入库后 动态单价
					in_stock_single_price_after = in_stock_total_price_after / in_stock_count_after

					//in_stock_profit := good_finnal_total_price-float64(good_count) *good_final_single_cost

					//5.更新入库商品的总表
					_, err5 := db_in_stock_update.Exec(in_stock_count_after, in_stock_single_price_after, in_stock_total_price_after, good_product_id, order_shop_id)
					if err5 != nil {
						o.Rollback()
						logs.Error(-2008, "同步订单编号:", order_order_num, "更新入库总表数据执行失败。")
						hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(order_shop_id) + "，线下订单同步失败啦，订单号：" + order_order_num + "，错误码：2008")
						logs.Error(order)
						thisOrderCanResume = false //发现错误本条订单暂停执行
						break
					}

					//6.更新入库商品明细表
					_, err6 := db_in_stock_detail_insert.Exec(order_shop_id, good_product_id, in_stock_count, good_count, in_stock_count_after, 2050, good_order_num, in_stock_single_price_after, in_stock_total_price, good_final_single_cost*float64(good_count), in_stock_total_price_after)
					if err6 != nil {
						o.Rollback()

						logs.Error(-2009, "同步订单编号:", order_order_num, "插入入库明细表数据执行失败。")
						hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(order_shop_id) + "，线下订单同步失败啦，订单号：" + order_order_num + "，错误码：2009")
						logs.Error(order)
						thisOrderCanResume = false //发现错误本条订单暂停执行
						break
					}

				}

			}

			if thisOrderCanResume == false {
				continue
			}

			//遍历支付方式插入支付明细
			order_pay := order.Get("pay_ways")
			order_pay_array, _ := order_pay.Array()

			if len(order_pay_array) < 1 {
				o.Rollback()
				logs.Error(-2017, "同步订单编号:", order_order_num, "获取支付信息数据执行失败。")
				hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(order_shop_id) + "，线下订单同步失败啦，订单号：" + order_order_num + "，错误码：2017")
				logs.Error(order)
				thisOrderCanResume = false //发现错误本条订单暂停执行
				continue
			}

			//遍历支付明细
			for k := 0; k < len(order_pay_array); k++ {

				payway := order_pay.GetIndex(k)
				payway_order_num, _ := payway.Get("order_num").String()
				payway_payway_id, _ := payway.Get("payway_id").Int()
				payway_payway_amount, _ := payway.Get("payway_amount").Float64()

				_, err98 := db_order_pay_insert.Exec(payway_order_num, payway_payway_id, payway_payway_amount)
				if err98 != nil {
					o.Rollback()

					logs.Error(-2010, "同步订单编号:", order_order_num, "插入订单支付表过程中报错了。")
					hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(order_shop_id) + "，线下订单同步失败啦，订单号：" + order_order_num + "，错误码：2010")
					logs.Error(order)
					thisOrderCanResume = false //发现错误本条订单暂停执行
					continue
				}

				//判断是不是会员卡支付方式
				if payway_payway_id == 2 || payway_payway_id == 4 {

					//是会员卡支付方式
					if order_member_id == 0 {
						o.Rollback()
						logs.Error(-2011, "同步订单编号:", order_order_num, "支付方式是会员卡支付，但是没有会员卡id,错误数据。")
						hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(order_shop_id) + "，线下订单同步失败啦，订单号：" + order_order_num + "，错误码：2011")
						logs.Error(order)
						thisOrderCanResume = false //发现错误本条订单暂停执行
						continue
					} else {

						//beego.Error("开始进行会员卡的金额操作")
						//进行会员卡的金额操作
						_, err := Card.RechargePayAction(o, order_shop_id, order_member_id, payway_payway_amount, order_type, order_order_num)
						if err != nil {
							o.Rollback()
							logs.Error(-2016, "同步订单编号:", order_order_num, "支付方式是会员卡支付，整个会员卡金额调整过程出错了。")
							hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(order_shop_id) + "，线下订单同步失败啦，订单号：" + order_order_num + "，错误码：2016")
							logs.Error(order)
							thisOrderCanResume = false //发现错误本条订单暂停执行
							continue
						}
					}

				}

			}

			//组合商品
			order_goods_group := order.Get("goods_group")
			order_goods_group_array, _ := order_goods_group.Array()

			if (len(order_goods_group_array) > 0) {
				for k := 0; k < len(order_goods_group_array); k++ {
					goods_group := order_goods_group.GetIndex(k)
					goods_group_order_num, _ := goods_group.Get("order_num").String()
					//goods_group_order_goods_pid, _ := goods_group.Get("order_goods_pid").Int()
					goods_group_product_id, _ := goods_group.Get("product_id").Int()
					goods_group_product_name, _ := goods_group.Get("product_name").String()
					goods_group_single_price, _ := goods_group.Get("single_price").Float64()
					goods_group_single_cost, _ := goods_group.Get("single_cost").Float64()
					goods_group_count, _ := goods_group.Get("count").Float64()

					_, err_goods_group := db_goods_group_insert.Exec(goods_group_order_num, order_goods_id, goods_group_product_id,
						goods_group_product_name, goods_group_single_price, goods_group_single_cost, goods_group_count, goods_group_count*goods_group_single_price)
					if err_goods_group != nil {
						o.Rollback()

						logs.Error(-2013, "同步订单编号:", order_order_num, "插入订单商品组合表过程中报错了。")
						hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(order_shop_id) + "，线下订单同步失败啦，订单号：" + order_order_num + "，错误码：2013")
						logs.Error(order)
						thisOrderCanResume = false //发现错误本条订单暂停执行
						continue
					}
				}
			}

			if thisOrderCanResume == false {
				continue
			}

			//如果是正单更新订单表的利润和成本字段
			if order_type == 1 {
				order_profit = order_final_price - order_final_cost
				_, err99 := db_order_update.Exec(order_final_cost, order_profit, order_order_num)
				if err99 != nil {
					o.Rollback()
					logs.Error(-2017, "同步订单编号:", order_order_num, "订单更新成本价和利润过程中报错了。")
					hh.DingTalk.RebotSendMsgToIT("大哥！店铺ID" + strconv.Itoa(order_shop_id) + "，线下订单同步失败啦，订单号：" + order_order_num + "，错误码：2017")
					logs.Error(order)
					thisOrderCanResume = false //发现错误本条订单暂停执行
					continue
				}

			}

			order_success_array = append(order_success_array, order_order_num)
			o.Commit()

		}

	}

	elapsed := time.Since(t1)

	beego.Info("订单同步执行成功，共", order_package_num, "条订单数据， 耗时：", elapsed)

	c.Data["status"] = "success"
	c.Data["num"] = len(order_success_array)
	c.Data["data"] = order_success_array
	c.Data["message"] = "订单数据同步成功"
	return c.Data
}

//下载限量促销活动数据
func (c *SynchController) DownActivityLimitRule(event hh.Event) map[string]interface{} {
	c.Prepare()
	clientrowversion, _ := event.Content.Data.Get("client_row_version").Int()
	shop_id, _ := event.Content.Data.Get("shop_id").Int()
	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM gr_pos_activity_limit_rule WHERE row_version > ? and shop_id=?", clientrowversion, shop_id).Values(&maps)
	if err != nil {
		return nil
	}

	if num > 0 {
		c.Data["status"] = "success"
		c.Data["num"] = num
		c.Data["data"] = maps
		c.Data["message"] = "数据同步成功"
	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "数据同步失败"
	}

	return c.Data
}

//商品营销主表同步接口
func (c *SynchController) DownMaketingGoods(event hh.Event) map[string]interface{} {
	c.Prepare()
	clientrowversion, checkerr1 := event.Content.Data.Get("client_row_version").Int()
	shopid, checkerr2 := event.Content.Data.Get("client_shop_id").Int()

	if checkerr1 != nil || checkerr2 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw("SELECT a.*,b.maketing_name as name,b.begin_time,b.end_time,b.rule "+
		" FROM gr_pos_maketing_goods a "+
		" left join gr_pos_maketing_relation b on a.relation_id = b.id and b.maketing_type = 1 "+
		" WHERE a.row_version > ? and a.shop_id=? ", clientrowversion, shopid).Values(&maps)
	if err != nil {
		return nil
	}

	if num > 0 {
		c.Data["status"] = "success"
		c.Data["num"] = num
		c.Data["data"] = maps
		c.Data["message"] = "数据同步成功"
	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "数据同步失败"
	}

	return c.Data
}

//商品营销子表同步接口
func (c *SynchController) DownMaketingGoodsDetail(event hh.Event) map[string]interface{} {
	c.Prepare()
	clientrowversion, checkerr1 := event.Content.Data.Get("client_row_version").Int()
	shopid, checkerr2 := event.Content.Data.Get("client_shop_id").Int()

	if checkerr1 != nil || checkerr2 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw(" SELECT * FROM gr_pos_maketing_goods_detail a "+
		" WHERE a.row_version > ? and a.maketing_goods_id in "+
		" (select b.id from gr_pos_maketing_goods b where shop_id = ?) ", clientrowversion, shopid).Values(&maps)
	if err != nil {
		return nil
	}

	if num > 0 {
		c.Data["status"] = "success"
		c.Data["num"] = num
		c.Data["data"] = maps
		c.Data["message"] = "数据同步成功"
	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "数据同步失败"
	}

	return c.Data
}

//下载自助点单商品营养成分表同步接口
func (c *SynchController) DownProductNutrition(event hh.Event) map[string]interface{} {
	c.Prepare()
	clientrowversion, checkerr1 := event.Content.Data.Get("client_row_version").Int()

	if checkerr1 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM `gr_pos_self_nutrition_info` WHERE row_version > ?", clientrowversion).Values(&maps)
	if err != nil {
		return nil
	}

	if num > 0 {
		c.Data["status"] = "success"
		c.Data["num"] = num
		c.Data["data"] = maps
		c.Data["message"] = "数据同步成功"
	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "数据同步失败"
	}

	return c.Data
}

//下载自助点单推荐组合表
func (c *SynchController) DownSelfServiceGroup(event hh.Event) map[string]interface{} {
	c.Prepare()
	clientrowversion, checkerr1 := event.Content.Data.Get("client_row_version").Int()

	if checkerr1 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM `gr_pos_self_service_group` WHERE row_version > ?", clientrowversion).Values(&maps)
	if err != nil {
		return nil
	}

	if num > 0 {
		c.Data["status"] = "success"
		c.Data["num"] = num
		c.Data["data"] = maps
		c.Data["message"] = "数据同步成功"
	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "数据同步失败"
	}

	return c.Data
}

//下载自助点单推荐组合商品表
func (c *SynchController) DownSelfServiceGroupProduct(event hh.Event) map[string]interface{} {
	c.Prepare()
	clientrowversion, checkerr1 := event.Content.Data.Get("client_row_version").Int()

	if checkerr1 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM `gr_pos_self_service_group_product` WHERE row_version > ?", clientrowversion).Values(&maps)
	if err != nil {
		return nil
	}

	if num > 0 {
		c.Data["status"] = "success"
		c.Data["num"] = num
		c.Data["data"] = maps
		c.Data["message"] = "数据同步成功"
	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "数据同步失败"
	}

	return c.Data
}

//下载自助点单商品表
func (c *SynchController) DownSelfServiceProduct(event hh.Event) map[string]interface{} {
	c.Prepare()
	clientrowversion, checkerr1 := event.Content.Data.Get("client_row_version").Int()

	if checkerr1 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM `gr_pos_self_service_product` WHERE row_version > ?", clientrowversion).Values(&maps)
	if err != nil {
		return nil
	}

	if num > 0 {
		c.Data["status"] = "success"
		c.Data["num"] = num
		c.Data["data"] = maps
		c.Data["message"] = "数据同步成功"
	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "数据同步失败"
	}

	return c.Data
}

//下载自助点单商品表
func (c *SynchController) DownSelfServiceProductNutrition(event hh.Event) map[string]interface{} {
	c.Prepare()
	clientrowversion, checkerr1 := event.Content.Data.Get("client_row_version").Int()

	if checkerr1 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM `gr_pos_self_service_product_nutrition` WHERE row_version > ?", clientrowversion).Values(&maps)
	if err != nil {
		return nil
	}

	if num > 0 {
		c.Data["status"] = "success"
		c.Data["num"] = num
		c.Data["data"] = maps
		c.Data["message"] = "数据同步成功"
	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "数据同步失败"
	}

	return c.Data
}

//***************************
//***************************
//***************************
