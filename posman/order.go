package posman

import (
	"freshlife.io/PosmanServer/hh"
	"github.com/astaxie/beego/orm"
	"strconv"

	"github.com/astaxie/beego"
)

type OrderController struct {
	BaseController
}

//code 4000

//订单查询接口
func (c *OrderController) Search(event hh.Event) map[string]interface{} {
	c.Prepare()

	order_num, checkerr1 := event.Content.Data.Get("order_num").String()
	order_time_start, checkerr2 := event.Content.Data.Get("order_time_start").Int()
	order_time_end, checkerr3 := event.Content.Data.Get("order_time_end").Int()
	order_shop_id, checkerr4 := event.Content.Data.Get("client_shop_id").Int()

	if checkerr1 != nil || checkerr2 != nil || checkerr3 != nil || checkerr4 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -99991
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw("SELECT a.*,(SELECT COUNT(id) FROM `gr_pos_order` WHERE `origin_order_num` = a.order_num) as have_cancel_order FROM `gr_pos_order` a WHERE a.order_num LIKE '%"+order_num+"%' AND a.createAt >= ? AND a.createAt <= ? AND a.shop_id = ? AND a.type=1", order_time_start, order_time_end, order_shop_id).Values(&maps)
	if err != nil {
		c.Data["status"] = "fail"
		c.Data["message"] = "订单查询错误"
		return c.Data
	}

	if num > 0 {
		c.Data["status"] = "success"
		c.Data["data"] = maps
		c.Data["num"] = num
		c.Data["message"] = "订单查询成功"

	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "订单查询失败"
	}

	return c.Data
}

//订单明细接口
func (c *OrderController) OrderGoodsSearch(event hh.Event) map[string]interface{} {
	c.Prepare()

	order_num, checkerr1 := event.Content.Data.Get("order_num").String()

	if checkerr1 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -99991
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM `gr_pos_order_goods` WHERE `order_num` = ?", order_num).Values(&maps)
	if err != nil {
		c.Data["status"] = "fail"
		c.Data["message"] = "订单明细查询错误"
		return c.Data
	}

	var maps2 []orm.Params
	num2, err2 := o.Raw("SELECT a.*,b.member_id,b.member_type FROM `gr_pos_order_pay` a LEFT JOIN `gr_pos_order` b ON a.order_num = b.order_num WHERE a.order_num  = ?", order_num).Values(&maps2)
	if err2 != nil || num2 < 1 {
		c.Data["status"] = "fail"
		c.Data["message"] = order_num + "订单支付详情查询错误"
		return c.Data
	}

	data := make(map[string]interface{})

	var maps3 []orm.Params

	member_id, _ := strconv.ParseInt(maps2[0]["member_id"].(string), 10, 0)
	member_type, _ := strconv.ParseInt(maps2[0]["member_type"].(string), 10, 0)
	beego.Error(member_id, member_type)
	beego.Error(member_type > 0)
	beego.Error(member_type == 1)
	if member_id > 0 && member_type == 1 {
		num3, err3 := o.Raw("SELECT a.* FROM `gr_pos_member_card` a WHERE a.id  = ?", member_id).Values(&maps3)
		if err3 != nil {
			c.Data["status"] = "fail"
			c.Data["message"] = order_num + "会员卡详情查询错误"
			return c.Data
		}
		if num3 > 0 {
			data["member_card"] = maps3[0]
		}

	} else if member_id > 0 && member_type == 2 {
		num3, err3 := o.Raw("SELECT a.* FROM `gr_ec_user_member` a WHERE a.uid  = ?", member_id).Values(&maps3)
		if err3 != nil {
			c.Data["status"] = "fail"
			c.Data["message"] = order_num + "会员卡详情查询错误"
			return c.Data
		}
		if num3 > 0 {
			data["member_card"] = maps3[0]
		}
	}

	var maps1 []orm.Params
	_, err1 := o.Raw("SELECT * FROM `gr_pos_order` WHERE `order_num` = ?", order_num).Values(&maps1)
	if err1 != nil {
		c.Data["status"] = "fail"
		c.Data["message"] = "订单查询错误"
		return c.Data
	}

	data["order"] = maps1[0]
	data["pay_ways"] = maps2
	data["order_goods"] = maps

	if num > 0 {
		c.Data["status"] = "success"
		c.Data["data"] = data
		c.Data["num"] = num
		c.Data["message"] = "订单明细查询成功"

	} else {
		beego.Error(2)
		c.Data["status"] = "fail"
		c.Data["message"] = "订单明细查询失败"
	}

	return c.Data
}

//支付方式接口
func (c *OrderController) GetPayWay(event hh.Event) map[string]interface{} {
	c.Prepare()

	client_shop_id, checkerr1 := event.Content.Data.Get("client_shop_id").Int()
	client_mode, checkerr2 := event.Content.Data.Get("client_mode").Int()

	if checkerr1 != nil || checkerr2 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -99991
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM `gr_pos_payway` WHERE (shop_id=? OR shop_id=0) AND type=? AND status=1", client_shop_id, client_mode).Values(&maps)
	if err != nil {
		c.Data["status"] = "fail"
		c.Data["message"] = "支付方式查询错误"
		return c.Data
	}

	if num > 0 {
		c.Data["status"] = "success"
		c.Data["data"] = maps
		c.Data["num"] = num
		c.Data["message"] = "支付方式查询成功"

	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "支付方式查询失败"
	}

	return c.Data
}
