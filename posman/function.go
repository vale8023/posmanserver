package posman

import (
	"crypto/md5"
	"io"
	"fmt"
	"crypto/sha1"
)

func think_ucenter_md5(password string,uc_auth_key string) string{
	return md5str(sha1str(password)+uc_auth_key);
}


//对字符串进行MD5哈希
func md5str(data string) string {
	t := md5.New();
	io.WriteString(t,data);
	return fmt.Sprintf("%x",t.Sum(nil));
}

//对字符串进行SHA1哈希
func sha1str(data string) string {
	t := sha1.New();
	io.WriteString(t,data);
	return fmt.Sprintf("%x",t.Sum(nil));
}