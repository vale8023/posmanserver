package posman

import (
	"freshlife.io/PosmanServer/hh"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego"
)

type ShopController struct {
	BaseController
}

//code 4000

//订单查询接口
func (c *ShopController) SearchOrder(event hh.Event) map[string]interface{} {
	c.Prepare()

	id, checkerr1 := event.Content.Data.Get("id").Int()
	shopid, checkerr2 := event.Content.Data.Get("shopid").Int()

	if checkerr1 != nil || checkerr2 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -99991
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()
	o.Using("ECV3")

	var maps1 []orm.Params
	//num1, err1 := o.Raw("SELECT b.*,c.code FROM gr_sale_order a left join gr_sale_ordergoods b on a.orderId = b.orderId left join gr_erpv2.gr_pos_product c on b.pid = c.id where a.id = ? and a.shopId = ?", id, shopid).Values(&maps1)
	num1, err1 := o.Raw("SELECT b.*,c.code " +
								"FROM gr_sale_order a " +
								"left join gr_sale_ordergoods b on a.orderId = b.orderId " +
								"left join gr_erpv2.gr_pos_product c on b.pid = c.id " +
								"where a.id = ? and a.shopId = ?", id, shopid).Values(&maps1)
	if err1 != nil {
		c.Data["status"] = "fail"
		c.Data["message"] = "订单查询错误"
		return c.Data
	}

	if num1 > 0 {
		c.Data["status"] = "success"
		c.Data["data"] = maps1
		c.Data["num"] = num1
		c.Data["message"] = "订单查询成功"
	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "订单查询失败"
	}

	return c.Data
}

//当日改价
func (c *ShopController) PriceChanged(event hh.Event) map[string]interface{} {
	c.Prepare()
	starttime, checkerr1 := event.Content.Data.Get("starttime").Int()
	endtime, checkerr2 := event.Content.Data.Get("endtime").Int()

	if checkerr1 != nil || checkerr2 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -99991
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM gr_pos_product WHERE update_price_time >=? and update_price_time <=? order by update_price_time",starttime,endtime).Values(&maps)
	if err != nil {
		c.Data["status"] = "fail"
		c.Data["message"] = "查询失败"
		return c.Data
	}

	if num > 0 {
		beego.Error(hh.NowTime())
		c.Data["status"] = "success"
		c.Data["data"] = maps
		c.Data["num"] = num
		c.Data["message"] = "订单查询成功"
	} else {
		beego.Error("fail")
		c.Data["status"] = "fail"
		c.Data["message"] = "无结果"
	}

	return c.Data
}
//func (c *ShopController)FinishOrder(event hh.Event)  map[string] interface{}{
//	c.Prepare()
//	id , checkerr1 := event.Content.Data.Get("id").String()
//	shopid , checkerr2 := event.Content.Data.Get("shop_id").Int()
//
//	if checkerr1 != nil || checkerr2 != nil {
//		c.Data["status"]="fail"
//		c.Data["code"]=-99991
//		c.Data["message"]="传入的参数有误。"
//		return c.Data
//	}
//	//连接数据库准备查询
//	o := orm.NewOrm()
//	o.Begin()
//	db_order_num_update, _ := o.Raw("UPDATE `gr_sale_order` SET `status` = ?, `updateAt` = unix_timestamp() WHERE `gr_sale_order`.`orderId` = ? and `gr_sale_order`.`shopId` = ?").Prepare()
//	defer db_order_num_update.Close()
//
//
//	_,err := db_order_num_update.Exec(id,shopid)
//
//	if err != nil{
//		o.Rollback()
//		c.Data["status"]="fail"
//		c.Data["message"]="结算错误"
//		return  c.Data
//	}
//
//	o.Commit()
//	c.Data["num"]=0
//	c.Data["status"]="success"
//	c.Data["message"]="结算成功"
//
//	return c.Data
//}
