package posman

import (
	"errors"
	"fmt"
	"freshlife.io/PosmanServer/hh"
	"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
	"strconv"

	"github.com/astaxie/beego"
	//"golang.org/x/tools/go/gcimporter15/testdata"
)

type ProductController struct {
	BaseController
}

//code 3000

//查询商品信息
func (c *ProductController) Search(event hh.Event) map[string]interface{} {

	c.Prepare()
	productname, checkerr1 := event.Content.Data.Get("product_name").String()

	if checkerr1 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM `gr_pos_product` WHERE `name` LIKE '%" + productname + "%'").Values(&maps)
	if err != nil {
		return nil
	}

	if num > 0 {
		c.Data["status"] = "success"
		c.Data["num"] = num
		c.Data["data"] = maps
		c.Data["message"] = "数据查询成功"
	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "数据查询失败"
	}

	return c.Data
}

//获取某门店库存信息数据
func (c *ProductController) StockSearch(event hh.Event) map[string]interface{} {

	c.Prepare()
	productname, checkerr1 := event.Content.Data.Get("product_name").String()
	shopid, checkerr2 := event.Content.Data.Get("client_shop_id").Int()
	client_model, checkerr3 := event.Content.Data.Get("client_model").Int()

	if checkerr1 != nil || checkerr2 != nil || checkerr3 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw("SELECT a.*,b.name,b.spec_name,b.code,b.min_spec_num,b.cup_tip,b.sell_price,b.min_sell_price "+
		"FROM gr_pos_shop_stock a "+
		"LEFT JOIN gr_pos_product b ON a.product_id = b.id  "+
		"Left join gr_pos_product_category c on a.product_category_id = c.id  "+
		"WHERE  a.shop_id=? and b.type = 1 and b.name LIKE '%"+productname+"%' and c.type = ?", shopid, client_model).Values(&maps)
	if err != nil {
		return nil
	}

	if num > 0 {
		c.Data["status"] = "success"
		c.Data["num"] = num
		c.Data["data"] = maps
		c.Data["message"] = "数据查询成功"
	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "数据查询失败"
	}

	return c.Data
}

//获取某门店库存信息数据
func (c *ProductController) StockAndProductSearch(event hh.Event) map[string]interface{} {
	c.Prepare()
	productname, checkerr1 := event.Content.Data.Get("product_name").String()
	shopid, checkerr2 := event.Content.Data.Get("client_shop_id").Int()
	//client_model, checkerr3 := event.Content.Data.Get("client_model").Int()

	if checkerr1 != nil || checkerr2 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	//var maps []orm.Params

	shop_id_str := strconv.Itoa(shopid)

	var sql = "SELECT" +
		"	a.count," +
		"	b.weighing_needed," +
		"	b.id product_id," +
		"	b.name," +
		"	b.spec_name," +
		"	b.code," +
		"	b.min_spec_num," +
		"	b.cup_tip," +
		"	d.sell_price," +
		"	ifnull(b.min_sell_price,0) min_sell_price," +
		"	c.`statistic_classify` " +
		"FROM" +
		"	gr_pos_shop_stock a" +
		"	LEFT JOIN gr_pos_product b ON a.product_id = b.id" +
		"	LEFT JOIN gr_pos_product_category c ON a.product_category_id = c.id " +
		"   LEFT JOIN gr_pos_product_shop_permission d on a.shop_id = d.shop_id and a.product_id = d.product_id " +
		"WHERE" +
		"	a.STATUS = 1 " +
		"	AND a.delflag = 0 " +
		"	AND c.`statistic_classify` != 1" +
		"	AND d.sell_price IS NOT NULL " +
		"	and a.shop_id = " + shop_id_str +
		"   AND  b.name like '%" + productname + "%'" +
		"	AND b.type = 1 and a.count >0 " +
		"	UNION ALL " +
		"SELECT 999 AS count, b.weighing_needed, b.id AS product_id, b.name, b.spec_name " +
		"	, b.code, b.min_spec_num, b.cup_tip, a.sell_price, 0 AS min_sell_price " +
		"	, c.`statistic_classify` " +
		"FROM gr_pos_product_shop_permission a " +
		"	LEFT JOIN gr_pos_product b ON a.product_id = b.id " +
		"	LEFT JOIN gr_pos_product_category c ON b.category_id = c.id " +
		"WHERE b.STATUS = 1 " +
		"	AND b.delflag = 0 " +
		"	AND c.`statistic_classify` = 1 " +
		"	AND a.sell_price IS NOT NULL " +
		"	AND a.shop_id =  " + shop_id_str +
		"	AND b.type = 1 " +
		"   AND b.name like '%" + productname + "%'"

	var maps []orm.Params
	num, err := o.Raw(sql).Values(&maps)

	if err != nil {
		return nil
	}

	if num > 0 {
		c.Data["status"] = "success"
		c.Data["num"] = num
		c.Data["data"] = maps
		c.Data["message"] = "数据查询成功"
	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "数据查询失败"
	}

	return c.Data
}

//获取商品规格转换规则
func (c *ProductController) GetSpecChangeRule(event hh.Event) map[string]interface{} {

	c.Prepare()
	productid, _ := event.Content.Data.Get("origin_product_id").Int()
	shop_id, _ := event.Content.Data.Get("shop_id").Int()

	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	//num, err := o.Raw("SELECT a.* ,b.name as origin_product_name,b.spec_name as origin_spec_name,b.code  as origin_code,"+
	//	" b.min_spec_num  as origin_min_spec_num,c.name as switchto_product_name ,c.spec_name as switchto_spec_name,"+
	//	" c.code  as switchto_code,c.min_spec_num  as switchto_min_spec_num "+
	//	" FROM `gr_pos_product_spec_change_rule` a "+
	//	" INNER JOIN `gr_pos_product` b "+
	//	" INNER JOIN `gr_pos_product` c "+
	//	" WHERE a.`origin_product_id` = ? and a.origin_product_id = b.id and a.switchto_product_id = c.id and a.status = 1 and a.delflag = 0 "+
	//	" and b.status = 1 and b.delflag = 0 and c.status = 1 and c.delflag = 0 ", productid).Values(&maps)

	num, err := o.Raw("SELECT a.* ,b.name as origin_product_name,b.spec_name as origin_spec_name,b.code  as origin_code,"+
		" b.min_spec_num  as origin_min_spec_num,c.name as switchto_product_name ,c.spec_name as switchto_spec_name,"+
		" c.code  as switchto_code,c.min_spec_num  as switchto_min_spec_num  "+
		"FROM `gr_pos_product_spec_change_rule` a  "+
		"INNER JOIN `gr_pos_product` b  "+
		"INNER JOIN `gr_pos_product` c  "+
		"INNER JOIN `gr_pos_product_shop_permission` d "+
		"WHERE a.`origin_product_id` = ? and a.origin_product_id = b.id and a.switchto_product_id = c.id and a.status = 1 "+
		"and d.product_id = c.id and d.shop_id = ? "+
		"and a.delflag = 0  and b.status = 1 and b.delflag = 0 and c.status = 1 and c.delflag = 0", productid, shop_id).Values(&maps)
	if err != nil {
		return nil
	}

	if num > 0 {
		c.Data["status"] = "success"
		c.Data["num"] = num
		c.Data["data"] = maps
		c.Data["message"] = "数据查询成功"
	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "数据查询失败"
	}

	return c.Data
}

//商品规格转换操作
func (c *ProductController) SpecChangeAction(event hh.Event) map[string]interface{} {

	c.Prepare()
	out_product_id, checkerr1 := event.Content.Data.Get("out_product_id").Int()
	out_count, checkerr2 := event.Content.Data.Get("out_count").Int()
	in_product_id, checkerr3 := event.Content.Data.Get("in_product_id").Int()
	in_count, checkerr4 := event.Content.Data.Get("in_count").Int()
	shopid, checkerr5 := event.Content.Data.Get("client_shop_id").Int()

	if checkerr1 != nil || checkerr2 != nil || checkerr3 != nil || checkerr4 != nil || checkerr5 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()

	//SQL准备
	o.Begin()
	//0.插入转换记录表准备
	db_spec_change_list_insert, _ := o.Raw("INSERT INTO `gr_pos_product_spec_change_list` (`shop_id`, `out_product_id`, `out_count`, `in_product_id`, `in_count`, `status`, `createAt`, `updateAt`, `delflag`) VALUES (?, ?, ?, ?, ?, 1, unix_timestamp() ,unix_timestamp(), '0')").Prepare()
	defer db_spec_change_list_insert.Close()

	//出库的语句
	//1.得到出库商品的库存数据

	////2.更新出库商品的总表
	db_out_stock_update, _ := o.Raw("UPDATE `gr_pos_shop_stock` SET `count` = ?,  `stock_total_price` = ?, `row_version` = unix_timestamp() WHERE `gr_pos_shop_stock`.`product_id` = ? and `gr_pos_shop_stock`.`shop_id` = ?").Prepare()
	defer db_out_stock_update.Close()
	//3.更新出库商品明细表
	db_out_stock_detail_insert, _ := o.Raw("INSERT INTO `gr_pos_shop_stock_detail` (`shop_id`, `product_id`, `origin_stock`, `change_count`, `current_count`, `type`, `order_num`, `stock_single_price`, `origin_stock_total_price`, `change_stock_total_price`, `current_stock_total_price`, `status`, `createAt`, `updateAt`, `delflag`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1, unix_timestamp() ,unix_timestamp(), '0')").Prepare()
	defer db_out_stock_detail_insert.Close()
	////入库的语句

	////5.更新入库商品的总表
	db_in_stock_update, _ := o.Raw("UPDATE `gr_pos_shop_stock` SET `count` = ?, `stock_single_price` = ?, `stock_total_price` = ?, `row_version` = unix_timestamp() WHERE `gr_pos_shop_stock`.`product_id` = ? and `gr_pos_shop_stock`.`shop_id` = ?").Prepare()
	defer db_in_stock_update.Close()

	//5.1 插入商品总表
	db_in_stock_insert, _ := o.Raw("INSERT INTO `gr_pos_shop_stock` (`shop_id`, `product_id`, `product_category_id`, `count`, `stock_single_price`, `stock_total_price`, `first_instock_at`, `recent_instock_at`, `recent_outstock_at`, `sale_display`, `status`, `createAt`, `updateAt`, `delflag`, `row_version`) VALUES (?, ?, ?, ?, ?, ?, unix_timestamp() , null, null, 0,1, unix_timestamp() ,unix_timestamp(), '0', unix_timestamp())").Prepare()
	defer db_in_stock_insert.Close()

	////6.更新入库商品明细表
	db_in_stock_detail_insert, _ := o.Raw("INSERT INTO `gr_pos_shop_stock_detail` (`shop_id`, `product_id`, `origin_stock`, `change_count`, `current_count`, `type`, `order_num`, `stock_single_price`, `origin_stock_total_price`, `change_stock_total_price`, `current_stock_total_price`, `status`, `createAt`, `updateAt`, `delflag`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1, unix_timestamp() ,unix_timestamp(), '0');").Prepare()
	defer db_in_stock_detail_insert.Close()

	var changeID int64
	var out_stock_single_price float64
	var out_stock_total_price float64
	var out_stock_count float64
	var out_count_total_price float64

	var in_stock_count float64
	var in_stock_count_after float64
	var in_stock_single_price_after float64
	var in_stock_total_price float64
	var in_stock_total_price_after float64

	//0记录下数据转化记录
	res0, err0 := db_spec_change_list_insert.Exec(shopid, out_product_id, out_count, in_product_id, in_count)
	if err0 != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["code"] = -3001
		c.Data["message"] = "规格转化操作失败，插入规格转化表执行失败。"
		return c.Data
	} else {
		changeID, _ = res0.LastInsertId() //获取转换记录id
	}

	//1.得到出库商品的库存数据
	var maps1 []orm.Params

	num1, err1 := o.Raw("SELECT * FROM `gr_pos_shop_stock` WHERE `shop_id` =? AND `product_id` =?", shopid, out_product_id).Values(&maps1)
	if err1 == nil && num1 > 0 {

		out_stock_single_price, _ = strconv.ParseFloat(maps1[0]["stock_single_price"].(string), 64)
		out_stock_total_price, _ = strconv.ParseFloat(maps1[0]["stock_total_price"].(string), 64)
		out_stock_count, _ = strconv.ParseFloat(maps1[0]["count"].(string), 64)

		//当前库存数量小于要出库的数量 得报错
		if out_stock_count < float64(out_count) {
			o.Rollback()
			c.Data["status"] = "fail"
			c.Data["code"] = -3000
			c.Data["message"] = "规格转化失败，" + string(out_product_id) + "出库商品库存量不购。"
			return c.Data
		}

	} else {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["code"] = -3002
		c.Data["message"] = "出库商品总表查询失败。"
		return c.Data
	}

	//本次出库总成本
	out_count_total_price = float64(out_count) * out_stock_single_price
	//本次出库后 数量
	out_stock_count_after := out_stock_count - float64(out_count)
	//本次出库后 总金额
	out_stock_total_price_after := out_stock_total_price - out_count_total_price

	////2.更新出库商品的总表
	_, err2 := db_out_stock_update.Exec(out_stock_count_after, out_stock_total_price_after, out_product_id, shopid)
	if err2 != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["code"] = -3003
		c.Data["message"] = "更新出库总比数据执行失败。"
		return c.Data
	}

	//3.更新出库商品明细表
	_, err3 := db_out_stock_detail_insert.Exec(shopid, out_product_id, out_stock_count, out_count, out_stock_count_after, 7000, changeID, out_stock_single_price, out_stock_total_price, out_count_total_price, out_stock_total_price_after)
	if err3 != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["code"] = -3004
		c.Data["message"] = "插入出库明细表数据执行失败。"
		return c.Data
	}

	//4.判断待入库商品是否存在
	var maps2 []orm.Params
	num4, err4 := o.Raw("SELECT * FROM `gr_pos_shop_stock` WHERE `shop_id` =? AND `product_id` =?", shopid, in_product_id).Values(&maps2)
	if err4 != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["code"] = -3005
		c.Data["message"] = "入商品总表查询失败。"
		return c.Data
	} else {
		if num4 > 0 {
			//入库商品存在
			in_stock_total_price, _ = strconv.ParseFloat(maps2[0]["stock_total_price"].(string), 64) //总价格
			in_stock_count, _ = strconv.ParseFloat(maps2[0]["count"].(string), 64)                   //总数量
			//本次入库后 数量
			in_stock_count_after = in_stock_count + float64(in_count)
			//本次入库后 总金额
			in_stock_total_price_after = in_stock_total_price + out_count_total_price //原总成本 + 新增加成本
			//本次入库后 动态单价
			in_stock_single_price_after = in_stock_total_price_after / in_stock_count_after

			//5.更新入库商品的总表
			_, err5 := db_in_stock_update.Exec(in_stock_count_after, in_stock_single_price_after, in_stock_total_price_after, in_product_id, shopid)
			if err5 != nil {
				o.Rollback()
				c.Data["status"] = "fail"
				c.Data["code"] = -3006
				c.Data["message"] = "更新入库总表数据执行失败。"
				return c.Data
			}

		} else {

			//查询入库商品所在的分类
			var maps []orm.Params
			_, err52 := o.Raw("SELECT category_id FROM gr_pos_product WHERE id =?", in_product_id).Values(&maps)
			if err52 != nil {
				o.Rollback()
				c.Data["status"] = "fail"
				c.Data["code"] = -3009
				c.Data["message"] = "获取商品分类错误。"
				return c.Data
			}

			in_category_id := maps[0]["category_id"]

			//入库商品不存在
			//单个商品价格

			in_stock_total_price = 0
			in_stock_count = 0
			//本次入库后 数量
			in_stock_count_after = in_stock_count + float64(in_count)
			//本次入库后 总金额
			in_stock_total_price_after = in_stock_total_price + out_count_total_price //原总成本 + 新增加成本
			//本次入库后 动态单价
			in_stock_single_price_after = in_stock_total_price_after / in_stock_count_after

			_, err51 := db_in_stock_insert.Exec(shopid, in_product_id, in_category_id, in_stock_count_after, in_stock_single_price_after, in_stock_total_price_after)
			if err51 != nil {
				o.Rollback()
				c.Data["status"] = "fail"
				c.Data["code"] = -3007
				c.Data["message"] = "插入库总表数据执行失败。"
				return c.Data
			}

		}

	}

	//6.更新入库商品明细表
	_, err6 := db_in_stock_detail_insert.Exec(shopid, in_product_id, in_stock_count, in_count, in_stock_count_after, 7050, changeID, in_stock_single_price_after, in_stock_total_price, out_count_total_price, in_stock_total_price_after)
	if err6 != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["code"] = -3008
		c.Data["message"] = "插入入库明细表数据执行失败。"
		return c.Data
	}

	o.Commit()

	c.Data["status"] = "success"
	c.Data["message"] = "规格转化执行成功"

	return c.Data

}

//鲜果切制作转化操作
func (c *ProductController) FreshcutfruitsMakeAction(event hh.Event) map[string]interface{} {

	c.Prepare()
	//连接数据库准备查询
	o := orm.NewOrm()
	shopid, checkerr1 := event.Content.Data.Get("client_shop_id").Int()
	client_user_id, checkerr2 := event.Content.Data.Get("client_user_id").Int()
	//数据包
	use_products_list := event.Content.Data.Get("use_products_list")
	use_products_list_data, checkerr3 := use_products_list.Array()
	make_products_list := event.Content.Data.Get("make_products_list")
	make_products_list_data, checkerr4 := make_products_list.Array()

	if checkerr1 != nil || checkerr2 != nil || checkerr3 != nil || checkerr4 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	fmt.Println(use_products_list_data)
	fmt.Println(len(use_products_list_data))

	fmt.Println(make_products_list_data)
	fmt.Println(len(make_products_list_data))

	o.Begin()

	//1。鲜果切制作订单表准备
	db_fruitcut_order_insert, _ := o.Raw("INSERT INTO `gr_pos_fruitcut_order` (`order_num`, `type`, `create_by`, `product_count`, `total_price`, `shop_id`, `remark`, `status`, `createAt`, `delflag`) VALUES (?, '1', ?, '0', '0', ?, '鲜果切制作订单', '1', unix_timestamp(), '0')").Prepare()
	defer db_fruitcut_order_insert.Close()

	db_fruitcut_order_update, _ := o.Raw("UPDATE `gr_pos_fruitcut_order` SET `product_count` = ?, `total_price` = ?, `updateAt` = unix_timestamp() WHERE `gr_pos_fruitcut_order`.`id` = ?").Prepare()
	defer db_fruitcut_order_update.Close()

	db_fruitcut_order_goods_insert, _ := o.Raw("INSERT INTO `gr_pos_fruitcut_order_goods` (`order_num`, `type`, `product_id`, `product_name`, `count`, `single_price`, `total_price`, `status`, `createAt`,  `delflag`) VALUES (?, ?, ?, ?, ?, ?, ?, '1', unix_timestamp(), '0')").Prepare()
	defer db_fruitcut_order_goods_insert.Close()

	////2.更新出库商品的总表
	db_out_stock_update, _ := o.Raw("UPDATE `gr_pos_shop_stock` SET `count` = ?,  `stock_total_price` = ?, `row_version` = unix_timestamp() WHERE `gr_pos_shop_stock`.`product_id` = ? and `gr_pos_shop_stock`.`shop_id` = ?").Prepare()
	defer db_out_stock_update.Close()
	//3.更新出库商品明细表
	db_out_stock_detail_insert, _ := o.Raw("INSERT INTO `gr_pos_shop_stock_detail` (`shop_id`, `product_id`, `origin_stock`, `change_count`, `current_count`, `type`, `order_num`, `stock_single_price`, `origin_stock_total_price`, `change_stock_total_price`, `current_stock_total_price`, `status`, `createAt`, `updateAt`, `delflag`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1, unix_timestamp() ,unix_timestamp(), '0')").Prepare()
	defer db_out_stock_detail_insert.Close()
	////入库的语句

	////5.更新入库商品的总表
	db_in_stock_update, _ := o.Raw("UPDATE `gr_pos_shop_stock` SET `count` = ?, `stock_single_price` = ?, `stock_total_price` = ?, `row_version` = unix_timestamp() WHERE `gr_pos_shop_stock`.`product_id` = ? and `gr_pos_shop_stock`.`shop_id` = ?").Prepare()
	defer db_in_stock_update.Close()

	//5.1 插入商品总表
	db_in_stock_insert, _ := o.Raw("INSERT INTO `gr_pos_shop_stock` (`shop_id`, `product_id`, `product_category_id`, `count`, `stock_single_price`, `stock_total_price`, `first_instock_at`, `recent_instock_at`, `recent_outstock_at`, `sale_display`, `status`, `createAt`, `updateAt`, `delflag`, `row_version`) VALUES (?, ?, ?, ?, ?, ?, unix_timestamp() , null, null, 0,1, unix_timestamp() ,unix_timestamp(), '0', unix_timestamp())").Prepare()
	defer db_in_stock_insert.Close()

	////6.更新入库商品明细表
	db_in_stock_detail_insert, _ := o.Raw("INSERT INTO `gr_pos_shop_stock_detail` (`shop_id`, `product_id`, `origin_stock`, `change_count`, `current_count`, `type`, `order_num`, `stock_single_price`, `origin_stock_total_price`, `change_stock_total_price`, `current_stock_total_price`, `status`, `createAt`, `updateAt`, `delflag`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1, unix_timestamp() ,unix_timestamp(), '0');").Prepare()
	defer db_in_stock_detail_insert.Close()

	//创建一个鲜果切制作单

	fruitcut_order_num := string(shopid) + "XGQ" + hh.NowTime()

	res, err := db_fruitcut_order_insert.Exec(fruitcut_order_num, client_user_id, shopid)
	if err != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["code"] = -3100
		c.Data["message"] = "创建一个鲜果切制作单失败。"
		return c.Data
	}
	fruitcut_order_id, _ := res.LastInsertId()

	var maps []orm.Params
	var totalprice float64 //出库总成本
	var product_count int  //出库的商品总数量
	//出库环节
	for i := 0; i < len(use_products_list_data); i++ {

		use_product_id, _ := use_products_list.GetIndex(i).Get("product_id").Int()

		use_product_num, _ := use_products_list.GetIndex(i).Get("product_num").Int()

		product_count = product_count + use_product_num

		num, err := o.Raw("SELECT a.*,b.name as product_name  FROM `gr_pos_shop_stock` a INNER JOIN gr_pos_product b WHERE a.`shop_id` = ? AND a.`product_id` = ? and a.`product_id`=b.id", shopid, use_product_id).Values(&maps)
		if err == nil && num > 0 {

			stock_count, _ := strconv.ParseFloat(maps[0]["count"].(string), 64)
			stock_single_price, _ := strconv.ParseFloat(maps[0]["stock_single_price"].(string), 64)
			stock_total_price, _ := strconv.ParseFloat(maps[0]["stock_total_price"].(string), 64)
			product_name := maps[0]["product_name"].(string)

			//当前库存数量小于要出库的数量 得报错
			if stock_count < float64(use_product_num) {
				o.Rollback()
				c.Data["status"] = "fail"
				c.Data["code"] = -3102
				c.Data["message"] = "鲜果切制作失败，" + string(use_product_id) + "出库商品库存量不购。"
				return c.Data
			}

			totalprice = totalprice + stock_single_price*float64(use_product_num)
			out_stock_amount := stock_single_price * float64(use_product_num)
			//本商品的出库金额
			use_total_price := stock_single_price * float64(use_product_num)
			//出库后的剩余金额

			stock_total_price_after := stock_total_price - use_total_price

			//出库后的剩余数量
			stock_count_after := stock_count - float64(use_product_num)

			//更新库存表

			////2.更新出库商品的总表
			_, err2 := db_out_stock_update.Exec(stock_count_after, stock_total_price_after, use_product_id, shopid)
			if err2 != nil {
				o.Rollback()
				c.Data["status"] = "fail"
				c.Data["code"] = -3103
				c.Data["message"] = "更新出库总表数据执行失败。"
				return c.Data
			}

			//更新库存明细表

			//3.更新出库商品明细表
			_, err3 := db_out_stock_detail_insert.Exec(shopid, use_product_id, stock_count, use_product_num, stock_count_after, 6000, 0, stock_single_price, stock_total_price, out_stock_amount, stock_total_price_after)
			if err3 != nil {
				o.Rollback()
				c.Data["status"] = "fail"
				c.Data["code"] = -3104
				c.Data["message"] = "插入出库明细表数据执行失败。"
				return c.Data
			}

			//插入鲜果切明细数据

			_, err31 := db_fruitcut_order_goods_insert.Exec(fruitcut_order_num, 1, use_product_id, product_name, use_product_num, stock_single_price, out_stock_amount)
			if err31 != nil {
				o.Rollback()
				c.Data["status"] = "fail"
				c.Data["code"] = -3110
				c.Data["message"] = "插入鲜果切明细数据失败。"
				return c.Data
			}

		} else {
			//出库商品不存在
			o.Rollback()
			c.Data["status"] = "fail"
			c.Data["code"] = -3101
			c.Data["message"] = "鲜果切制作失败，出库商品不存在。"
			return c.Data
		}

	}

	//计算出平均价格
	var totalnumber float64
	for i := 0; i < len(make_products_list_data); i++ {
		make_product_num, _ := make_products_list.GetIndex(i).Get("product_num").Int()
		totalnumber = totalnumber + float64(make_product_num)
	}

	make_product_single_price := totalprice / totalnumber

	//出库完成，开始入库
	var maps2 []orm.Params
	for i := 0; i < len(make_products_list_data); i++ {
		make_product_id, _ := make_products_list.GetIndex(i).Get("product_id").Int()
		make_product_num, _ := make_products_list.GetIndex(i).Get("product_num").Int()

		var in_stock_count float64
		var in_stock_count_after float64
		var in_stock_single_price_after float64
		var in_stock_total_price float64
		var in_stock_total_price_after float64
		var in_stock_amount float64
		var in_stock_product_name string
		num, err := o.Raw("SELECT a.*,b.name as product_name  FROM `gr_pos_shop_stock` a INNER JOIN gr_pos_product b WHERE a.`shop_id` = ? AND a.`product_id` = ? and a.`product_id`=b.id", shopid, make_product_id).Values(&maps2)
		if err == nil && num > 0 {
			//在本地库存的商品中存在

			in_stock_total_price, _ = strconv.ParseFloat(maps2[0]["stock_total_price"].(string), 64) //总价格
			in_stock_count, _ = strconv.ParseFloat(maps2[0]["count"].(string), 64)                   //总数量
			in_stock_product_name = maps2[0]["product_name"].(string)
			//本次入库后 数量
			in_stock_count_after = in_stock_count + float64(make_product_num)

			in_stock_amount = make_product_single_price * float64(make_product_num)

			//本次入库后 总金额
			in_stock_total_price_after = in_stock_total_price + in_stock_amount //原总成本 + 新增加成本
			//本次入库后 动态单价
			in_stock_single_price_after = in_stock_total_price_after / in_stock_count_after

			//5.更新出库商品的总表
			_, err5 := db_in_stock_update.Exec(in_stock_count_after, in_stock_single_price_after, in_stock_total_price_after, make_product_id, shopid)
			if err5 != nil {
				o.Rollback()
				c.Data["status"] = "fail"
				c.Data["code"] = -3106
				c.Data["message"] = "更新入库总表数据执行失败。"
				return c.Data
			}

		} else {

			//查询入库商品所在的分类
			var maps []orm.Params
			_, err52 := o.Raw("SELECT category_id,name FROM gr_pos_product WHERE id =?", make_product_id).Values(&maps)
			if err52 != nil {
				o.Rollback()
				c.Data["status"] = "fail"
				c.Data["code"] = -3109
				c.Data["message"] = "获取商品分类错误。"
				return c.Data
			}

			in_category_id := maps[0]["category_id"]
			in_stock_product_name = maps[0]["name"].(string)

			//在本店库存的商品中不存在
			in_stock_total_price = 0
			in_stock_count := 0
			//本次入库后 数量
			in_stock_count_after = float64(in_stock_count) + float64(make_product_num)
			in_stock_amount = make_product_single_price * float64(make_product_num)
			//本次入库后 总金额
			in_stock_total_price_after = float64(in_stock_total_price) + in_stock_amount //原总成本 + 新增加成本
			//本次入库后 动态单价
			in_stock_single_price_after = in_stock_total_price_after / float64(in_stock_count_after)

			_, err51 := db_in_stock_insert.Exec(shopid, make_product_id, in_category_id, in_stock_count_after, in_stock_single_price_after, in_stock_total_price_after)
			if err51 != nil {
				o.Rollback()
				c.Data["status"] = "fail"
				c.Data["code"] = -3107
				c.Data["message"] = "插入库总表数据执行失败。"
				return c.Data
			}
		}

		//6.更新入库商品明细表
		_, err6 := db_in_stock_detail_insert.Exec(shopid, make_product_id, in_stock_count, make_product_num, in_stock_count_after, 6050, 0, in_stock_single_price_after, in_stock_total_price, in_stock_amount, in_stock_total_price_after)
		if err6 != nil {
			o.Rollback()
			c.Data["status"] = "fail"
			c.Data["code"] = -3108
			c.Data["message"] = "插入入库明细表数据执行失败。"
			return c.Data
		}

		//11插入鲜果切明细数据

		_, err31 := db_fruitcut_order_goods_insert.Exec(fruitcut_order_num, 2, make_product_id, in_stock_product_name, make_product_num, make_product_single_price, in_stock_amount)
		if err31 != nil {
			o.Rollback()
			c.Data["status"] = "fail"
			c.Data["code"] = -3110
			c.Data["message"] = "插入鲜果切明细数据失败。"
			return c.Data
		}

	}

	//7.更新鲜果切制作单总表
	_, err0 := db_fruitcut_order_update.Exec(product_count, totalprice, fruitcut_order_id)
	if err0 != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["code"] = -3109
		c.Data["message"] = "更新鲜果切制作单总表失败。"
		return c.Data
	}

	o.Commit()
	c.Data["status"] = "success"
	c.Data["message"] = "鲜果切制作执行成功"
	return c.Data
}

//商品批量出库 转榨汁、报损出库用。
func (c *ProductController) StockOutAction(event hh.Event) map[string]interface{} {
	c.Prepare()

	//连接数据库准备查询
	o := orm.NewOrm()

	//SQL准备
	o.Begin()
	//0.插入转换记录表准备

	db_outstock_order_insert, _ := o.Raw("INSERT INTO `gr_pos_outstock_order` (`order_num`, `type`, `category`, `create_by`, `product_count`," +
		" `total_price`, `shop_id`, `remark`, `status`, `createAt`, `updateAt`, `delflag`) " +
		"VALUES (?, ?, ?, ?, ?, ?, ?, ?, '1', unix_timestamp() , unix_timestamp() , '0')").Prepare()
	defer db_outstock_order_insert.Close()

	db_outstock_order_goods_insert, _ := o.Raw("INSERT INTO `gr_pos_outstock_order_goods` ( `order_num`, `product_id`, `product_name`, " +
		"`count`, `single_price`, `total_price`, `status`, `createAt`, `updateAt`, `delflag`)" +
		" VALUES (?, ?,?, ?, ?, ?, '1', unix_timestamp() , unix_timestamp() , '0')").Prepare()
	defer db_outstock_order_goods_insert.Close()

	db_outstock_order_update, _ := o.Raw("UPDATE `gr_pos_outstock_order` SET `total_price` = ?, `updateAt` = unix_timestamp()  WHERE `gr_pos_outstock_order`.`order_num` = ?").Prepare()
	defer db_outstock_order_update.Close()

	order_shop_id, checkerr1 := event.Content.Data.Get("client_shop_id").Int()
	order_product_count, checkerr2 := event.Content.Data.Get("product_count").Int()
	order_category, checkerr3 := event.Content.Data.Get("category").String()
	order_create_by, checkerr4 := event.Content.Data.Get("create_by").Int()
	order_remark, checkerr5 := event.Content.Data.Get("remark").String()
	var order_total_price float64
	order_type, checkerr7 := event.Content.Data.Get("type").Int()

	if checkerr1 != nil || checkerr2 != nil || checkerr3 != nil || checkerr4 != nil || checkerr5 != nil || checkerr7 != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	order_order_num := strconv.Itoa(order_shop_id) + hh.NowTime()

	_, err := db_outstock_order_insert.Exec(order_order_num, order_type, order_category, order_create_by, order_product_count, order_total_price, order_shop_id, order_remark)
	if err != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["code"] = -4001
		c.Data["message"] = "插入出库订单失败。"
		return c.Data
	}

	order_goods := event.Content.Data.Get("goods")

	order_goods_array, _ := order_goods.Array()

	for i := 0; i < len(order_goods_array); i++ {
		good := order_goods.GetIndex(i)
		good_product_id, _ := good.Get("product_id").Int()
		good_product_name, _ := good.Get("product_name").String()
		good_count, _ := good.Get("count").Float64()
		good_single_price, _ := getStockProductPrice(order_shop_id, good_product_id)
		good_total_price := good_count * good_single_price
		order_total_price = order_total_price + good_total_price

		//插入出库明细
		_, err := db_outstock_order_goods_insert.Exec(order_order_num, good_product_id, good_product_name, good_count, good_single_price, good_total_price)
		if err != nil {
			o.Rollback()
			c.Data["status"] = "fail"
			c.Data["code"] = -4002
			c.Data["message"] = "插入出库订单明细失败。"
			return c.Data
		}

		var out_type int
		if order_type == 1 {

			out_type = 4000
		}
		if order_type == 2 {
			out_type = 4050
		}

		_, err9 := c.productStcokOutAction(o, order_shop_id, good_product_id, good_count, out_type, order_order_num)

		if err9 != nil {
			fmt.Println(err9.Error())
			o.Rollback()
			c.Data["status"] = "fail"
			c.Data["code"] = -4003
			c.Data["message"] = "出库操作执行失败。"
			return c.Data
		}

	}

	_, err99 := db_outstock_order_update.Exec(order_total_price, order_order_num)
	if err99 != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["code"] = -4009
		c.Data["message"] = "更新出库订单总表总成本金额失败。"
		return c.Data
	}

	o.Commit()
	c.Data["status"] = "success"
	c.Data["message"] = "商品出库 报损、转榨汁执行成功"
	return c.Data
}

//商品出库操作带事务的
func (c *ProductController) productStcokOutAction(o orm.Ormer, shop_id int, product_id int, out_count float64, out_type int, out_order_num string) (bool, error) {

	////2.更新出库商品的总表
	db_out_stock_update, _ := o.Raw("UPDATE `gr_pos_shop_stock` SET `count` = ?,  `stock_total_price` = ?, `row_version` = unix_timestamp() WHERE `gr_pos_shop_stock`.`product_id` = ? and `gr_pos_shop_stock`.`shop_id` = ?").Prepare()
	defer db_out_stock_update.Close()
	//3.更新出库商品明细表
	db_out_stock_detail_insert, _ := o.Raw("INSERT INTO `gr_pos_shop_stock_detail` (`shop_id`, `product_id`, `origin_stock`, `change_count`, `current_count`, `type`, `order_num`, `stock_single_price`, `origin_stock_total_price`, `change_stock_total_price`, `current_stock_total_price`, `status`, `createAt`, `updateAt`, `delflag`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1, unix_timestamp() ,unix_timestamp(), '0')").Prepare()
	defer db_out_stock_detail_insert.Close()

	//开始出库操作
	var maps []orm.Params

	num, err := o.Raw("SELECT a.*,b.name as product_name  FROM `gr_pos_shop_stock` a INNER JOIN gr_pos_product b WHERE a.`shop_id` = ? AND a.`product_id` = ? and a.`product_id`=b.id", shop_id, product_id).Values(&maps)
	if err == nil && num > 0 {

		stock_count, _ := strconv.ParseFloat(maps[0]["count"].(string), 64)
		stock_single_price, _ := strconv.ParseFloat(maps[0]["stock_single_price"].(string), 64)
		stock_total_price, _ := strconv.ParseFloat(maps[0]["stock_total_price"].(string), 64)

		//当前库存数量小于要出库的数量 得报错
		if stock_count < float64(out_count) {
			o.Rollback()
			logs.Error(-4999, "出库单出库失败，店铺", shop_id, product_id, "，出库商品库存量不购减。")
			return false, errors.New("出库失败，出库商品库存量不购减。")
		}

		//本商品的出库金额

		out_stock_amount := stock_single_price * float64(out_count)

		//出库后的剩余金额

		stock_total_price_after := stock_total_price - out_stock_amount

		//出库后的剩余数量
		stock_count_after := stock_count - float64(out_count)

		//更新库存表

		////2.更新出库商品的总表
		_, err2 := db_out_stock_update.Exec(stock_count_after, stock_total_price_after, product_id, shop_id)
		if err2 != nil {

			o.Rollback()
			logs.Error(-4999, "出库单出库失败，", out_order_num, "，更新出库总表数据执行失败。")
			return false, errors.New("出库失败，更新出库总表数据执行失败。")

		}

		//更新库存明细表

		//3.更新出库商品明细表
		_, err3 := db_out_stock_detail_insert.Exec(shop_id, product_id, stock_count, out_count, stock_count_after, out_type, out_order_num, stock_single_price, stock_total_price, out_stock_amount, stock_total_price_after)
		if err3 != nil {

			o.Rollback()
			logs.Error(-4999, "出库单出库失败，", out_order_num, "，插入出库明细表数据执行失败。")

			return false, errors.New("出库失败，插入出库明细表数据执行失败。")
		}

	}

	return true, nil

}

//获取商品属性
func (c *ProductController) GetProductPriceTag(event hh.Event) map[string]interface{} {

	c.Prepare()
	bin, _ := event.Content.Data.Get("bin").String()
	shopid, _ := event.Content.Data.Get("shopid").Int()

	beego.Error(bin, shopid)
	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var map1 []orm.Params
	num1, err1 := o.Raw("select * from gr_pos_shop where id = ?", shopid).Values(&map1)
	if err1 != nil || num1 == 0 {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "数据查询失败"
		return c.Data
	}

	var maps []orm.Params
	num, err := o.Raw("SELECT name,spec_name,b.place_of_production,b.effect,ifnull(b.sweetness,0) as sweetness,d.nickname,c.sell_price,b.remark "+
		"FROM gr_pos_product a "+
		"left join gr_pos_product_property b on a.id = b.product_id "+
		"left join gr_pos_product_shop_permission c on a.id = c.product_id  "+
		"left join gr_member d on c.update_by = d.uid "+
		"WHERE a.code in("+bin+") and c.shop_id = ?", shopid).Values(&maps)

	if err != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "数据查询失败"
		return c.Data
	}
	//beego.Error(maps)
	if num > 0 {
		c.Data["status"] = "success"
		c.Data["num"] = num
		c.Data["data"] = maps
		c.Data["message"] = "数据查询成功"
	} else {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "数据查询失败"
		return c.Data
	}

	return c.Data
}
