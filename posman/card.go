package posman

import (
	"freshlife.io/PosmanServer/hh"
	"github.com/astaxie/beego/orm"
	"strconv"
	"github.com/astaxie/beego/logs"
	"errors"
	"math"
	"strings"
)

type CardController struct {
	BaseController
}

//code 5000

var Card CardController;

//通过 手机号码，姓名，卡号 查询会员卡 信息
func (c *CardController) Search(event hh.Event) map[string]interface{} {
	c.Prepare()
	card_type, checkerr1 := event.Content.Data.Get("card_type").Int()
	card_search, checkerr2 := event.Content.Data.Get("card_search").String()

	if checkerr1 != nil || checkerr2 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	if card_search == "果然" || strings.Contains(card_search, "果然1") {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM `gr_pos_member_card` WHERE type=? and phone = '"+card_search+"'", card_type).Values(&maps)
	if err != nil {
		c.Data["status"] = "fail"
		c.Data["message"] = "会员卡查询错误"
		return c.Data
	}

	if num > 0 {
		c.Data["status"] = "success"
		c.Data["data"] = maps
		c.Data["num"] = num
		c.Data["message"] = "会员卡查询成功"

	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "会员卡查询失败"
	}

	return c.Data
}

//通过 会员卡硬件编码 查询会员卡 信息
func (c *CardController) SearchByHardWareNum(event hh.Event) map[string]interface{} {
	c.Prepare()
	card_type, checkerr1 := event.Content.Data.Get("card_type").Int()
	card_hard_num, checkerr2 := event.Content.Data.Get("card_hard_num").String()

	if checkerr1 != nil || checkerr2 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM `gr_pos_member_card` WHERE type=? and card_hard_num=?", card_type, card_hard_num).Values(&maps)
	if err != nil {
		c.Data["status"] = "fail"
		c.Data["message"] = "会员卡查询错误"
		return c.Data
	}

	if num > 0 {
		c.Data["status"] = "success"
		c.Data["data"] = maps
		c.Data["num"] = num
		c.Data["message"] = "会员卡查询成功"

	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "会员卡查询失败"
	}

	return c.Data
}

//添加会员卡
func (c *CardController) Add(event hh.Event) map[string]interface{} {
	c.Prepare()

	shop_id, checkerr5 := event.Content.Data.Get("client_shop_id").Int()
	card_num, checkerr1 := event.Content.Data.Get("card_num").String()
	card_hard_num, checkerr2 := event.Content.Data.Get("card_hard_num").String()
	card_type, checkerr3 := event.Content.Data.Get("card_type").Int()
	card_password, checkerr5 := event.Content.Data.Get("card_password").String()
	card_name, checkerr6 := event.Content.Data.Get("card_name").String()
	card_phone, checkerr7 := event.Content.Data.Get("card_phone").String()
	card_address, checkerr4 := event.Content.Data.Get("card_address").String()
	card_create_by, checkerr8 := event.Content.Data.Get("card_create_by").Int()
	card_remark, checkerr9 := event.Content.Data.Get("card_remark").String()

	if checkerr1 != nil || checkerr2 != nil || checkerr3 != nil || checkerr4 != nil || checkerr5 != nil || checkerr6 != nil || checkerr7 != nil || checkerr8 != nil || checkerr9 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()

	var maps0 []orm.Params
	num0, err0 := o.Raw("SELECT * FROM `gr_pos_member_card` WHERE card_hard_num=?", card_hard_num).Values(&maps0)
	if err0 != nil {

		c.Data["status"] = "fail"
		c.Data["message"] = "会员卡查询错误"
		return c.Data
	}
	if num0 > 0 {
		c.Data["status"] = "fail"
		c.Data["message"] = "会员卡已经存在，无法继续添加"
		return c.Data
	}

	//SQL准备
	o.Begin()
	//0.插入转换记录表准备
	db_member_card_insert, _ := o.Raw("INSERT INTO `gr_pos_member_card` ( `card_num`, `card_hard_num`, `type`, `password`, " +
		"`level`, `name`, `phone`, `address`, `amount`, `open_card_shop_id`, `create_by`, `remark`, `status`, `createAt`, `updateAt`, `delflag`)" +
		" VALUES ( ?, ?, ?, ?, 0 , ? , ? , ? , 0, ? , ? , ? , 1, unix_timestamp() ,unix_timestamp(), '0')").Prepare()
	defer db_member_card_insert.Close()

	if len(card_password) > 0 {
		card_password = md5str(card_password)
	}

	_, err := db_member_card_insert.Exec(card_num, card_hard_num, card_type, card_password, card_name, card_phone, card_address, shop_id, card_create_by, card_remark)

	if err != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["code"] = -5001
		c.Data["message"] = "新增会员卡数据执行失败。"
		return c.Data
	}

	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM `gr_pos_member_card` WHERE card_hard_num=?", card_hard_num).Values(&maps)
	if err != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["message"] = "会员卡查询错误"
		return c.Data
	}

	o.Commit()
	c.Data["status"] = "success"
	c.Data["data"] = maps
	c.Data["num"] = num
	c.Data["message"] = "会员卡创建成功"

	return c.Data
}

//添加会员卡
func (c *CardController) Edit(event hh.Event) map[string]interface{} {
	c.Prepare()
	card_id, checkerr3 := event.Content.Data.Get("card_id").Int()
	card_num, checkerr1 := event.Content.Data.Get("card_num").String()
	card_hard_num, checkerr2 := event.Content.Data.Get("card_hard_num").String()
	card_password, checkerr5 := event.Content.Data.Get("card_password").String()
	card_name, checkerr6 := event.Content.Data.Get("card_name").String()
	card_phone, checkerr7 := event.Content.Data.Get("card_phone").String()
	card_address, checkerr4 := event.Content.Data.Get("card_address").String()
	card_remark, checkerr9 := event.Content.Data.Get("card_remark").String()

	if checkerr1 != nil || checkerr2 != nil || checkerr3 != nil || checkerr4 != nil || checkerr5 != nil || checkerr6 != nil || checkerr7 != nil || checkerr9 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -9999
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()

	//SQL准备
	o.Begin()
	//0.插入转换记录表准备
	db_member_card_update, _ := o.Raw("UPDATE `gr_pos_member_card` SET `card_num` = ?, `password` = ?, `name` = ?, `phone` = ?, `address` = ?, `remark` =?, `updateAt` = unix_timestamp() WHERE `gr_pos_member_card`.`id` = ? and `gr_pos_member_card`.`card_hard_num` = ?").Prepare()
	defer db_member_card_update.Close()

	if len(card_password) > 0 {
		card_password = md5str(card_password)
	}
	_, err := db_member_card_update.Exec(card_num, card_password, card_name, card_phone, card_address, card_remark, card_id, card_hard_num)

	if err != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["message"] = "会员卡编辑错误"
		return c.Data
	}

	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM `gr_pos_member_card` WHERE card_hard_num=?", card_hard_num).Values(&maps)
	if err != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["message"] = "会员卡查询错误"
		return c.Data
	}

	o.Commit()
	c.Data["data"] = maps
	c.Data["num"] = num
	c.Data["status"] = "success"
	c.Data["message"] = "会员卡编辑成功"

	return c.Data
}

//获取充值方案接口
func (c *CardController) GetRechargeRule(event hh.Event) map[string]interface{} {
	c.Prepare()

	client_shop_id, checkerr1 := event.Content.Data.Get("client_shop_id").Int()
	//client_mode , checkerr2 := event.Content.Data.Get("client_mode").Int()

	if checkerr1 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -99991
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw("SELECT a.`id`, a.`name`, a.`type`, a.`charge_amount`, a.`give_amount`, b.`shop_id`, a.`status`, a.`createAt`, a.`updateAt`, a.`delflag`"+
		" FROM `gr_pos_member_charge` a"+
		" left join  gr_pos_member_charge_shop b on a.id = b.member_charge_id"+
		" WHERE b.shop_id = ? and a.status = 1 and a.delflag = 0"+
		" order by a.charge_amount desc", client_shop_id).Values(&maps)
	if err != nil {
		c.Data["status"] = "fail"
		c.Data["message"] = "充值方案查询错误"
		return c.Data
	}

	if num > 0 {
		c.Data["status"] = "success"
		c.Data["data"] = maps
		c.Data["num"] = num
		c.Data["message"] = "充值方案查询成功"

	} else {
		c.Data["status"] = "fail"
		c.Data["message"] = "充值方案查询失败"
	}

	return c.Data
}

//会员卡充值接口
func (c *CardController) RechargeAction(event hh.Event) map[string]interface{} {

	c.Prepare()

	order_num, checkerr1 := event.Content.Data.Get("order_num").String()
	order_type, checkerr2 := event.Content.Data.Get("type").Int()
	create_by, checkerr3 := event.Content.Data.Get("create_by").Int()
	member_id, checkerr31 := event.Content.Data.Get("member_id").Int()

	charge_amount, checkerr4 := event.Content.Data.Get("charge_amount").Float64()
	give_amount, checkerr5 := event.Content.Data.Get("give_amount").Float64()
	total_amount, checkerr6 := event.Content.Data.Get("total_amount").Float64()
	payway_id, checkerr7 := event.Content.Data.Get("payway_id").Int()
	final_amount, checkerr8 := event.Content.Data.Get("final_amount").Float64()
	shop_id, checkerr9 := event.Content.Data.Get("shop_id").Int()
	client_id, checkerr10 := event.Content.Data.Get("client_id").String()
	remark, checkerr11 := event.Content.Data.Get("remark").String()
	//notify_flag , checkerr12 := event.Content.Data.Get("notify_flag").Int()

	if checkerr1 != nil || checkerr2 != nil || checkerr3 != nil || checkerr31 != nil || checkerr4 != nil || checkerr5 != nil || checkerr6 != nil || checkerr7 != nil || checkerr8 != nil || checkerr9 != nil || checkerr10 != nil || checkerr11 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -99991
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	//连接数据库准备查询
	o := orm.NewOrm()
	//SQL准备
	o.Begin()
	//0.插入充值订单
	db_member_charge_order_insert, _ := o.Raw("INSERT INTO `gr_pos_member_charge_order` (`order_num`, `type`, `create_by`, " +
		"`member_id`, `charge_amount`, `give_amount`, `total_amount`, `payway_id`, `final_amount`, `shop_id`, `client_id`, `remark`, " +
		"`notify_flag`, `status`, `createAt`, `updateAt`, `delflag`)" +
		" VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, '0',  1, unix_timestamp() ,unix_timestamp(), '0');").Prepare()
	defer db_member_charge_order_insert.Close()

	//修改会员卡的金额
	db_member_card_update, _ := o.Raw("UPDATE `gr_pos_member_card` SET `amount` = ? WHERE `gr_pos_member_card`.`id` = ?").Prepare()
	defer db_member_card_update.Close()

	//插入会员卡的金额变动流水
	db_member_card_detail_insert, _ := o.Raw("INSERT INTO `gr_pos_member_card_detail` (`card_id`, `card_num`," +
		" `card_hard_num`, `origin_amount`, `change_amount`, `current_amount`, `type`, `order_num`, `charge_amount`, `give_amount`," +
		" `status`, `createAt`, `updateAt`, `delflag`)" +
		" VALUES (?, ?, ?, ?, ?, ?, '2', ?, ?, ?, 1, unix_timestamp() ,unix_timestamp(), '0')").Prepare()
	defer db_member_card_detail_insert.Close()

	_, err := db_member_charge_order_insert.Exec(order_num, order_type, create_by, member_id, charge_amount, give_amount, total_amount, payway_id, final_amount, shop_id, client_id, remark)

	if err != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["message"] = "会员充值订单插入失败"
		return c.Data
	}

	//查出会员卡原金额
	var maps []orm.Params
	num, err1 := o.Raw("SELECT * FROM `gr_pos_member_card` WHERE `id` = ?", member_id).Values(&maps)
	if err1 != nil || num < 1 {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["message"] = "会员卡查询失败"
		return c.Data
	}

	//读出会员卡原始余额
	var origin_amount float64
	var current_amount float64
	origin_amount, _ = strconv.ParseFloat(maps[0]["amount"].(string), 64)
	current_amount = origin_amount + total_amount
	//读出会员卡ic编码
	card_num := maps[0]["card_num"].(string)
	card_hard_num := maps[0]["card_hard_num"].(string)

	_, err2 := db_member_card_detail_insert.Exec(member_id, card_num, card_hard_num, origin_amount, total_amount, current_amount, order_num, charge_amount, give_amount)
	if err2 != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["message"] = "会员卡充值流水插入失败"
		return c.Data
	}

	_, err3 := db_member_card_update.Exec(current_amount, member_id)
	if err3 != nil {
		o.Rollback()
		c.Data["status"] = "fail"
		c.Data["message"] = "会员卡金额修改失败"
		return c.Data
	}

	o.Commit()

	var mapsok []orm.Params
	num2, err2 := o.Raw("SELECT d.card_num,d.name as card_name,d.phone as card_phone,d.amount as card_amount, a.member_id,a.order_num,a.type,a.shop_id,a.client_id,a.notify_flag,a.create_by,c.nickname as create_by_name,a.charge_amount,a.give_amount,a.total_amount,a.payway_id,b.name as payway_name,a.final_amount from (select * FROM `gr_pos_member_charge_order` WHERE shop_id=? AND type =? AND notify_flag=0) a LEFT JOIN `gr_pos_payway` b ON a.payway_id = b.id LEFT JOIN `gr_member` c ON a.create_by = c.uid LEFT JOIN `gr_pos_member_card` d ON a.member_id = d.id", shop_id, order_type).Values(&mapsok)
	if err2 != nil {
		c.Data["status"] = "fail"
		c.Data["message"] = "会员卡订单查询错误"
		return c.Data
	}

	c.Data["status"] = "success"
	c.Data["num"] = num2
	c.Data["data"] = mapsok
	c.Data["message"] = "会员卡充值成功！将充值成功的订单推送给客户端！"
	return c.Data

}

//充值通知确认接口
func (c *CardController) RechargeNotify(event hh.Event) map[string]interface{} {

	c.Prepare()
	shop_id, checkerr1 := event.Content.Data.Get("shop_id").Int()
	order_num, checkerr2 := event.Content.Data.Get("order_num").String()
	if checkerr1 != nil || checkerr2 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -99991
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}
	//连接数据库准备查询
	o := orm.NewOrm()

	//修改会员卡的金额
	db_member_charge_order_update, _ := o.Raw("UPDATE `gr_pos_member_charge_order` SET `notify_flag` = '1' WHERE `order_num` = ? AND `shop_id`= ?").Prepare()
	defer db_member_charge_order_update.Close()

	res, err := db_member_charge_order_update.Exec(order_num, shop_id)
	num, _ := res.RowsAffected()
	if err != nil || num < 1 {
		c.Data["status"] = "fail"
		c.Data["message"] = "会员卡订单通知回调错误"
		return c.Data
	}

	c.Data["status"] = "success"
	c.Data["message"] = "会员卡订单通知回调成功！"
	return c.Data
}

//销售订单扣减会员卡金额、退款订单增加会员卡金额操作带事务的
func (c *CardController) RechargePayAction(o orm.Ormer, shop_id int, member_id int, total_amount float64, order_type int, order_num string) (bool, error) {

	//准备SQL语句

	//修改会员卡的金额
	db_member_card_update, _ := o.Raw("UPDATE `gr_pos_member_card` SET `amount` = ? WHERE `gr_pos_member_card`.`id` = ? and amount = ?").Prepare()
	defer db_member_card_update.Close()

	//插入会员卡的金额变动流水
	db_member_card_detail_insert, _ := o.Raw("INSERT INTO `gr_pos_member_card_detail` (`card_id`, `card_num`," +
		" `card_hard_num`, `origin_amount`, `change_amount`, `current_amount`, `type`, `order_num`, `charge_amount`, `give_amount`," +
		" `status`, `createAt`, `updateAt`, `delflag`)" +
		" VALUES (?, ?, ?, ?, ?, ?, ?, ?, '0','0',1, unix_timestamp() ,unix_timestamp(), '0')").Prepare()
	defer db_member_card_detail_insert.Close()

	//查出会员卡原金额
	var maps []orm.Params
	num, err1 := o.Raw("SELECT * FROM `gr_pos_member_card` WHERE `id` = ?", member_id).Values(&maps)
	if err1 != nil || num < 1 {
		o.Rollback()
		logs.Error(-2012, "同步订单编号:", order_num, " 会员卡id 居然不存在！！！。")
		return false, errors.New("会员卡id 居然不存在。")
	}

	//读出会员卡原始余额
	var origin_amount float64
	var current_amount float64
	var member_card_detail_type int32
	origin_amount, _ = strconv.ParseFloat(maps[0]["amount"].(string), 64)

	if order_type == 1 {
		current_amount = origin_amount - total_amount //销售订单
		member_card_detail_type = 1
	}
	if order_type == 2 {
		current_amount = origin_amount + math.Abs(total_amount) //退货订单
		member_card_detail_type = 3
	}

	//读出会员卡ic编码
	card_num := maps[0]["card_num"].(string)
	card_hard_num := maps[0]["card_hard_num"].(string)

	_, err2 := db_member_card_detail_insert.Exec(member_id, card_num, card_hard_num, origin_amount, total_amount, current_amount, member_card_detail_type, order_num)
	if err2 != nil {
		o.Rollback()
		logs.Error(-2013, "同步订单编号:", order_num, " 会员卡充值流水插入失败。")
		return false, errors.New("会员卡充值流水插入失败。")
	}

	_, err3 := db_member_card_update.Exec(current_amount, member_id,origin_amount)
	if err3 != nil {
		o.Rollback()
		logs.Error(-2014, "同步订单编号:", order_num, " 会员卡金额修改失败。")
		return false, errors.New("会员卡金额修改失败。")
	}

	return true, nil

}
