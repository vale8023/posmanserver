package posman

import (
	"freshlife.io/PosmanServer/hh"
	"github.com/astaxie/beego/orm"

	"github.com/astaxie/beego"
	"time"
	"fmt"
)

type StockController struct {
	BaseController
}

//code 5000

//判断这个日结区间是否有盘点数据
func (c *StockController) WhetherStockCheck(event hh.Event) map[string]interface{} {
	c.Prepare()

	shop_id, checkerr1 := event.Content.Data.Get("shop_id").Int() //店铺id

	if checkerr1 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -99991
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}
	beego.Error(shop_id)

	//
	////连接数据库准备查询
	//o := orm.NewOrm()
	//
	////1.获得上一次日结开始时间 end_at
	//var maps3 []orm.Params
	//_, err3 := o.Raw("SELECT end_at FROM `gr_pos_overall_daliy` WHERE `shop_id` = ? ORDER BY id desc",shop_id).Values(&maps3)
	//if err3 != nil  {
	//	c.Data["status"]="fail"
	//	c.Data["message"]="未查到上一次日结结束时间。"
	//	return  c.Data
	//}
	//
	//var overall_daliy_begin_at int64
	//if len(maps3) < 1 {
	//	overall_daliy_begin_at = 0//日结开始时间
	//}else{
	//	overall_daliy_begin_at ,_ =  strconv.ParseInt(maps3[0]["end_at"].(string),10,0) //日结开始时间
	//}
	//
	//
	//
	//overall_daliy_end_at ,_ :=  strconv.ParseInt(hh.NowTime(),10,0) //日结结束时间
	//
	//
	//var maps4 []orm.Params
	//num4, err4 := o.Raw("SELECT createAt FROM `gr_pos_stock_check`  WHERE `shop_id` = ? AND createAt > ? AND createAt < ? ",shop_id,overall_daliy_begin_at,overall_daliy_end_at).Values(&maps4)
	//if err4 != nil  {
	//	c.Data["status"]="fail"
	//	c.Data["message"]="未查到本次日结时间段内有盘点数据。"
	//	return  c.Data
	//}
	//
	//
	//if num4 < 1 {
	//	c.Data["status"]="fail"
	//	c.Data["message"]="未查到本次日结时间段内有盘点数据。"
	//	return  c.Data
	//}

	c.Data["status"] = "success"
	c.Data["message"] = "本次日结时间段内已经有盘点数据"
	return c.Data

}

//获取待盘点数据
func (c *StockController) GetStockCheckData(event hh.Event) map[string]interface{} {
	c.Prepare()
	shop_id, checkerr1 := event.Content.Data.Get("shop_id").Int() //店铺id

	if checkerr1 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -99991
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	o := orm.NewOrm()
	//获取店铺信息
	var maps1 []orm.Params
	_, err1 := o.Raw("SELECT * FROM `gr_pos_shop`  WHERE  id =? ", shop_id).Values(&maps1)

	if err1 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -99991
		c.Data["message"] = "查询店铺信息失败。"
		return c.Data
	}

	a := "SELECT " +
		" a.*,p.name as product_name,p.code as product_code,p.spec_name as product_spec_name,p.unit,f.name product_category_name,t1.sell_price " +
		" FROM gr_pos_shop_stock a,gr_pos_product p,gr_pos_product_category f,gr_pos_product_shop_permission t1 " +
		" WHERE a.product_id=p.id" +
		" AND a.product_category_id = f.id " +
		" and a.product_id=t1.product_id " +
		" and a.shop_id=t1.shop_id " +
		" AND a.shop_id= ?" +
		" AND (" +
		" a.count > 0" +
		" OR " +
		" EXISTS(SELECT 1 FROM gr_pos_shop_stock_detail b " +
		" WHERE b.change_count != 0 " +
		" AND b.type IN (2000,4000,4050,5050,6000,7000,8000) " +
		" AND b.createAt BETWEEN ? and ? AND b.shop_id = a.shop_id  " +
		" AND a.product_id = b.product_id AND a.product_id = b.product_id)" +
		") order by a.product_category_id,p.code"

	timeStr := time.Now().Format("2006-01-02")
	fmt.Println(timeStr)

	//使用Parse 默认获取为UTC时区 需要获取本地时区 所以使用ParseInLocation
	t, _ := time.ParseInLocation("2006-01-02 15:04:05", timeStr+" 23:59:59", time.Local)
	//t2, _ := time.ParseInLocation("2006-01-02", timeStr, time.Local)

	t3 := t.Unix() - 2
	//fmt.Println(t2.AddDate(0, 0, 1).Unix())

	var maps2 []orm.Params
	_, err2 := o.Raw(a,shop_id , t3, hh.NowTime()).Values(&maps2)
	beego.Error(maps2[0])

	if err2 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -99991
		c.Data["message"] = "查询盘点表失败。"
		return c.Data
	}

	c.Data["status"] = "success"
	c.Data["data"] = maps2
	c.Data["message"] = "待盘点数据查询成功"
	return c.Data

}

//通过id获取盘点数据
func (c *StockController) GetStockCheckDataByID(event hh.Event) map[string]interface{} {
	c.Prepare()

	//shop_id , checkerr1 := event.Content.Data.Get("shop_id").Int()     //店铺id
	stock_check_id, checkerr2 := event.Content.Data.Get("stock_check_id").Int() //店铺id

	if checkerr2 != nil {
		c.Data["status"] = "fail"
		c.Data["code"] = -99991
		c.Data["message"] = "传入的参数有误。"
		return c.Data
	}

	data := make(map[string]interface{})

	//连接数据库准备查询
	o := orm.NewOrm()

	//1.获得上一次日结开始时间 end_at
	var maps3 []orm.Params
	num3, err3 := o.Raw("SELECT * FROM `gr_pos_stock_check`  WHERE  id =? ", stock_check_id).Values(&maps3)
	if err3 != nil || num3 < 1 {
		c.Data["status"] = "fail"
		c.Data["message"] = "未查到盘点数据。"
		return c.Data
	}

	beego.Error(maps3)

	data["stock_check"] = maps3[0]

	var maps4 []orm.Params
	num4, err4 := o.Raw("SELECT a.*,b.name as product_name,b.spec_name as product_spec_name,b.code as product_code "+
		"FROM `gr_pos_stock_check_detail` a "+
		"LEFT JOIN `gr_pos_product` b ON a.product_id = b.id  "+
		"WHERE a.check_id = ? and a.check_count is not null and (profit_check_count > 0 or loss_check_count < 0)"+
		" order by b.category_id,b.code", stock_check_id).Values(&maps4)
	if err4 != nil {
		c.Data["status"] = "fail"
		c.Data["message"] = "未查到盘点明细数据。"
		return c.Data
	}

	if num4 < 1 {
		c.Data["status"] = "fail"
		c.Data["message"] = "未查到盘点明细数据。"
		return c.Data
	}

	data["stock_check_detail"] = maps4

	c.Data["status"] = "success"
	c.Data["data"] = data
	c.Data["message"] = "盘点数据查询成功"
	return c.Data

}
