package controllers

import (
	"github.com/astaxie/beego"
	"encoding/json"
	"time"
	"freshlife.io/PosmanServer/hh"
	"github.com/astaxie/beego/orm"
	"strconv"
	"github.com/astaxie/beego/logs"
	"errors"
)

// 材料库相关接口
type OnlineOrderController struct{
	beego.Controller
}

// URLMapping...
func (p *OnlineOrderController) URLMapping(){

}

func (p *OnlineOrderController) Prepare(){
	var hhResult hh.HHResult
	var f interface{}
	err1 := json.Unmarshal(p.Ctx.Input.RequestBody, &f)
	if err1 != nil{
		hhResult.Status = "fail"
		hhResult.Code = -9999
		hhResult.Message = "body 传入的JSON数据格式不正确！"
		hhResult.Timestamp = time.Now().Unix()
		p.Data["json"] = hhResult
		p.ServeJSON()
		return
	}
	p.Data["code"] = int(0)
	p.Data["Message"] = "Query Success"
	p.Data["params"] = f
}



// 处理线上订单
func (p *OnlineOrderController) OnlineOrderHandling(){
	params := p.Data["params"].(map[string]interface{})
	ordernum  :=params["ordernum"]

	beego.Error(ordernum)
	p.OnlineOrderBroadcast(ordernum.(string))
}

//在线订单定时处理任务
func (p *OnlineOrderController) OnlineOrderBroadcast(ordernum string) {

		o := orm.NewOrm()

		//查询出需要待出库的线上订单
		var maps []orm.Params
		num, err := o.Raw("SELECT * FROM `gr_pos_online_order` WHERE order_num = ?",ordernum).Values(&maps)
		if err != nil {
			p.Data["errors"] = "fail"
			p.Data["Status"] = "fail"
			p.Data["Code"] = -1
			p.Data["Num"] = int64(0)
			p.Data["Message"] = "查询订单失败"
			return
		}
		if num < 1 {
			p.Data["errors"] = "fail"
			p.Data["Status"] = "fail"
			p.Data["Code"] = -1
			p.Data["Num"] = int64(0)
			p.Data["Message"] = "无此订单"
			return
		}

		if maps[0]["synch"] == "9"{
			beego.Error("success")
			p.Data["errors"] = nil
			p.Data["Status"] = "success"
			p.Data["Code"] = 0
			p.Data["Num"] = int64(1)
			p.Data["Message"] = "成功"
			return
		}
		beego.Error(222)
		////1.更新线上订单总表
		db_online_order_update, _ := o.Raw("UPDATE `gr_pos_online_order` SET `synch` = '9', `updateAt` = unix_timestamp() WHERE `id` = ? AND updateAt=?").Prepare()
		defer db_online_order_update.Close()

		////2.更新线上订单明细表
		db_online_order_goods_update, _ := o.Raw("UPDATE `gr_pos_online_order_goods` SET `final_single_cost` = ?,`stock_count` = ?, `profit` = ?, `updateAt` = unix_timestamp() WHERE `id` = ?").Prepare()
		defer db_online_order_goods_update.Close()

		//遍历这些店铺
		for k := 0; k < len(maps); k++ {
			order_id, _ := strconv.ParseInt(maps[k]["id"].(string), 10, 0)
			shop_id, _ := strconv.ParseInt(maps[k]["shop_id"].(string), 10, 0)
			order_num, _ := maps[k]["order_num"].(string)
			order_updateAt, _ := strconv.ParseInt(maps[k]["updateAt"].(string), 10, 0)

			o.Begin()

			//查询线上订单的详情
			var maps2 []orm.Params
			num2, err2 := o.Raw("SELECT ifnull(b.id,0) as pos_product_id ,e.statistic_classify,b.name as pos_product_name,b.spec_name as pos_product_spec_name,a.* " +
				"FROM gr_pos_online_order c " +
				"left join `gr_pos_online_order_goods` a on c.order_num = a.order_num " +
				"LEFT JOIN `gr_pos_product` b ON a.product_code = b.code " +
				"LEFT JOIN gr_pos_product_category e on b.category_id = e.id " +
				"WHERE a.order_num = ? AND c.type = 1", order_num).Values(&maps2)
			if err2 != nil {
				o.Rollback()
				p.Data["errors"] = "fail"
				p.Data["Status"] = "fail"
				p.Data["Code"] = -1
				p.Data["Num"] = int64(0)
				p.Data["Message"] = "线上订单详情查询失败"
				return
			}
			if num2 < 1 {
				o.Rollback()
				p.Data["errors"] = "fail"
				p.Data["Status"] = "fail"
				p.Data["Code"] = -1
				p.Data["Num"] = int64(0)
				p.Data["Message"] = "线上订单没有详情信息"
				return
			}

			//遍历订单商品的详情
			for i := 0; i < len(maps2); i++ {
				goods_id, _ := strconv.ParseInt(maps2[i]["id"].(string), 10, 0)

				pos_product_id, _ := strconv.ParseInt(maps2[i]["pos_product_id"].(string), 10, 0)
				statistic_classify,_:= strconv.ParseInt(maps2[i]["statistic_classify"].(string), 10, 0)

				//无库存商品不判断
				if statistic_classify == 1{
					continue;
				}

				//判断线上商品与收银系统中的商品是否匹配

				if (pos_product_id < 1) {
					beego.Error("大哥，线上订单同步失败啦，线上订单商品与收银系统中的商品编码不匹配啊！,订单号：", order_num)
					hh.DingTalk.RebotSendMsgToIT("大哥，线上订单同步失败啦，线上订单商品与收银系统中的商品编码不匹配啊！,订单号：" + order_num)
					o.Rollback()
					p.Data["errors"] = "fail"
					p.Data["Status"] = "fail"
					p.Data["Code"] = -1
					p.Data["Num"] = int64(0)
					p.Data["Message"] = "线上订单商品与收银系统中的商品编码不匹配"
					return
				}

				out_count, _ := strconv.ParseFloat(maps2[i]["count"].(string), 64)
				finnal_total_price, _ := strconv.ParseFloat(maps2[i]["finnal_total_price"].(string), 64)

				var shop_stock_count float64;
				stock_single_price, shop_stock_count, resultbool, err3 := p.productStcokOutActionReturnStockSinglePrice(o, shop_id, pos_product_id, out_count, 8000, order_num)

				if err3 != nil || resultbool == false {
					o.Rollback()
					p.Data["errors"] = "fail"
					p.Data["Status"] = "fail"
					p.Data["Code"] = -1
					p.Data["Num"] = int64(0)
					p.Data["Message"] = err3.Error()
					return
				}
				//反更新线上订单明细
				_, err4 := db_online_order_goods_update.Exec(stock_single_price, shop_stock_count, finnal_total_price-out_count*stock_single_price, goods_id)
				if err4 != nil {
					o.Rollback()
					p.Data["errors"] = "fail"
					p.Data["Status"] = "fail"
					p.Data["Code"] = -1
					p.Data["Num"] = int64(0)
					p.Data["Message"] = "反更新线上订单明细失败"
					return
				}
			}

			//反更新线上订单完成同步
			_, err5 := db_online_order_update.Exec(order_id, order_updateAt)

			if err5 != nil {
				o.Rollback()
				p.Data["errors"] = err
				p.Data["Status"] = "fail"
				p.Data["Code"] = -1
				p.Data["Num"] = int64(0)
				p.Data["Message"] = "反更新线上订单失败"
				return
			}

			beego.Error("线上订单"+ordernum+"处理成功")
			p.Data["errors"] = nil
			p.Data["Status"] = "success"
			p.Data["Code"] = 0
			p.Data["Num"] = int64(1)
			p.Data["Message"] = "成功"
			o.Commit()
			return
		}
}

//商品出库操作带事务的
func (p *OnlineOrderController) productStcokOutActionReturnStockSinglePrice(o orm.Ormer, shop_id int64, product_id int64, out_count float64, out_type int, out_order_num string) (float64, float64, bool, error) {

	////2.更新出库商品的总表
	db_out_stock_update, _ := o.Raw("UPDATE `gr_pos_shop_stock` SET `count` = ?,  `stock_total_price` = ?, `row_version` = unix_timestamp() WHERE `gr_pos_shop_stock`.`product_id` = ? and `gr_pos_shop_stock`.`shop_id` = ?").Prepare()
	defer db_out_stock_update.Close()
	//3.更新出库商品明细表
	db_out_stock_detail_insert, _ := o.Raw("INSERT INTO `gr_pos_shop_stock_detail` (`shop_id`, `product_id`, `origin_stock`, `change_count`, `current_count`, `type`, `order_num`, `stock_single_price`, `origin_stock_total_price`, `change_stock_total_price`, `current_stock_total_price`, `status`, `createAt`, `updateAt`, `delflag`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1, unix_timestamp() ,unix_timestamp(), '0')").Prepare()
	defer db_out_stock_detail_insert.Close()

	//开始出库操作
	var maps []orm.Params

	var stock_single_price float64
	var stock_count float64

	num, err := o.Raw("SELECT a.*,b.name as product_name  " +
		"FROM `gr_pos_shop_stock` a " +
			"INNER JOIN gr_pos_product b " +
			"WHERE a.`shop_id` = ? AND a.`product_id` = ? and a.`product_id`=b.id", shop_id, product_id).Values(&maps)
	if err == nil && num > 0 {

		stock_count, _ = strconv.ParseFloat(maps[0]["count"].(string), 64)

		if maps[0]["stock_single_price"] == nil {
			if maps[0]["stock_total_price"] == nil {
				o.Rollback()
				logs.Error(-4999, "出库单出库失败，", out_order_num, "，库存金额，和库存总价为空。")
				hh.DingTalk.RebotSendMsgToIT("大哥，线上订单同步失败啦，商品ID：" + strconv.FormatInt(product_id, 10) + " ，的居然库存金额，和库存总价为空。订单号：" + out_order_num)
				return 0.00, stock_count, false, errors.New("出库失败，出库商品库存金额，和库存总价为空。")
			}
		}

		stock_single_price, _ = strconv.ParseFloat(maps[0]["stock_single_price"].(string), 64)
		stock_total_price, _ := strconv.ParseFloat(maps[0]["stock_total_price"].(string), 64)

		//当前库存数量小于要出库的数量 得报错
		if stock_count < float64(out_count) {
			o.Rollback()
			logs.Error(-4999, "出库单出库失败，", out_order_num, "，出库商品库存量不购减。")
			hh.DingTalk.RebotSendMsgToIT("大哥，线上订单同步失败啦，商品ID：" + strconv.FormatInt(product_id, 10) + " ，的库存数量不购减。订单号：" + out_order_num)
			return 0.00, stock_count, false, errors.New("出库失败，出库商品库存量不购减。")
		}

		//本商品的出库金额

		out_stock_amount := stock_single_price * float64(out_count)

		//出库后的剩余金额

		stock_total_price_after := stock_total_price - out_stock_amount

		//出库后的剩余数量
		stock_count_after := stock_count - float64(out_count)

		//更新库存表

		////2.更新出库商品的总表
		_, err2 := db_out_stock_update.Exec(stock_count_after, stock_total_price_after, product_id, shop_id)
		if err2 != nil {

			o.Rollback()
			logs.Error(-4999, "出库单出库失败，", out_order_num, "，更新出库总表数据执行失败。")
			return 0.00, stock_count, false, errors.New("出库失败，更新出库总表数据执行失败。")

		}

		//更新库存明细表

		//3.更新出库商品明细表
		_, err3 := db_out_stock_detail_insert.Exec(shop_id, product_id, stock_count, out_count, stock_count_after, out_type, out_order_num, stock_single_price, stock_total_price, out_stock_amount, stock_total_price_after)
		if err3 != nil {

			o.Rollback()
			logs.Error(-4999, "出库单出库失败，", out_order_num, "，插入出库明细表数据执行失败。")

			return 0.00, stock_count, false, errors.New("出库失败，插入出库明细表数据执行失败。")
		}

	}

	return stock_single_price, stock_count, true, nil

}

func (p *OnlineOrderController) Finish(){
	var hhResult hh.HHResult
	if p.Data["errors"] != nil{
		hhResult.Status = p.Data["Status"].(string)
		hhResult.Code = p.Data["Code"].(int)
		hhResult.Num = p.Data["Num"].(int64)
		hhResult.Message = p.Data["Message"].(string)
	}else{
		hhResult.Status = "success"
		hhResult.Code = p.Data["code"].(int)
		hhResult.Num = p.Data["Num"].(int64)
		//hhResult.Data = p.Data["result"].([]orm.Params)
		hhResult.Message = p.Data["Message"].(string)
	}
	hhResult.Timestamp = time.Now().Unix()
	p.Data["json"] = hhResult
	p.ServeJSON()
}