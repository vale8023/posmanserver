package controllers

import (
	"github.com/astaxie/beego"
	"freshlife.io/PosmanServer/hh"
	"time"

	"github.com/astaxie/beego/orm"
	"fmt"
)

// 自定义的操作接口演示
type DemoController struct {
	beego.Controller
}


// URLMapping ...
func (c *DemoController) URLMapping() {
	c.Mapping("Add", c.Add)
	c.Mapping("Add2", c.Add2)
	c.Mapping("Json", c.JsonDemo1)
	c.Mapping("Json2", c.JsonDemo2)
}


// Add ...
// @Title Post
// @Description 添加一个Demo数据
// @Param	method	query	string	false	"e.g. commin.find"
// @Param	appkey	query	string	false	"e.g. commin.find"
// @Param	timestamp	query	string	false	"e.g. commin.find"
// @Param	sign	query	string	false	"e.g. commin.find"
// @Param	body    body 	string true "e.g. { "param1": "55552333", "param2": "333333" }"
// @Success 201 {int} models.HhAppkey
// @Failure 403 body is empty
// @router /add/ [post]
func (c *DemoController) Add() {
	var hhResut hh.HHResult
	//fmt.Println(c.Ctx.Request.Body)
	//构建返回结果
	hhResut.Status = "success"
	hhResut.Code = 0
	hhResut.Num = 0
	hhResut.Message = "自定义接口演示演示1"
	hhResut.Timestamp = time.Now().Unix()
	c.Data["json"] = hhResut
	c.ServeJSON()

}


// Add2 ...
// @Title Post
// @Description 添加一个Demo数据
// @Param	method	query	string	false	"e.g. commin.find"
// @Param	appkey	query	string	false	"e.g. commin.find"
// @Param	timestamp	query	string	false	"e.g. commin.find"
// @Param	sign	query	string	false	"e.g. commin.find"
// @Param	body    body 	string true "e.g. { "param1": "55552333", "param2": "333333" }"
// @Success 201 {int} models.HhAppkey
// @Failure 403 body is empty
// @router /add2/ [post]
func (c *DemoController) Add2() {
	var hhResut hh.HHResult
	//fmt.Println(c.Ctx.Request.Body)
	//构建返回结果
	hhResut.Status = "success"
	hhResut.Code = 0
	hhResut.Num = 0
	hhResut.Message = "自定义接口演示演示"
	hhResut.Timestamp = time.Now().Unix()
	c.Data["json"] = hhResut
	c.ServeJSON()

}

// JsonDemo1 ...
// @Title Post
// @Description 添加一个Demo数据
// @Param	method	query	string	false	"e.g. commin.find"
// @Param	appkey	query	string	false	"e.g. commin.find"
// @Param	timestamp	query	string	false	"e.g. commin.find"
// @Param	sign	query	string	false	"e.g. commin.find"
// @Param	body    body 	string true "e.g. { "param1": "55552333", "param2": "333333" }"
// @Success 201 {int} models.HhAppkey
// @Failure 403 body is empty
// @router /json1/ [post]
func (c *DemoController) JsonDemo1() {

	var hhResut hh.HHMapResult

	car := make(map[string] interface{})
	car["band"]="BMW"
	car["model"]="X1"

	maps := make(map[string] interface{})
	maps["name"]="myron"
	maps["sex"]="man"
	maps["age"]=22
	maps["age"]="22"
	maps["car"]=car


	hhResut.Status = "fail"
	hhResut.Code = -9999
	hhResut.Num = 0
	hhResut.Message = "数据查询失败，请检查数据格式."
	hhResut.Timestamp = time.Now().Unix()
	c.Data["json"] = maps
	c.ServeJSON()


}


// JsonDemo2 ...
// @Title Post
// @Description 添加一个Demo数据
// @Param	method	query	string	false	"e.g. commin.find"
// @Param	appkey	query	string	false	"e.g. commin.find"
// @Param	timestamp	query	string	false	"e.g. commin.find"
// @Param	sign	query	string	false	"e.g. commin.find"
// @Param	body    body 	string true "e.g. { "param1": "55552333", "param2": "333333" }"
// @Success 201 {int} models.HhAppkey
// @Failure 403 body is empty
// @router /json2/ [post]
func (c *DemoController) JsonDemo2() {


	car := make(map[string] interface{})
	car["band"]="BMW"
	car["model"]="X1"

	maps := make(map[string] interface{})
	maps["name"]="myron"
	maps["sex"]="man"
	maps["age"]=22
	maps["age"]="22"
	maps["car"]=car

	//附加一个SQL查询结果
	//连接数据库准备查询
	o := orm.NewOrm()
	//构建SQL语句
	qb, _ := orm.NewQueryBuilder("mysql")
	qb.Select("*").From("gr_appkey")
	//生成SQL语句
	sql := qb.String()
	//将数据库中查询结果映射到maps中
	var mapssql []orm.Params
	o.Raw(sql).Values(&mapssql)
	maps["appkey"]=mapssql

	//遍历查询出来的数据 ------附加额外的数据
	for _, term := range mapssql {
		fmt.Println(term["id"], ":", term["appkey"], term["securitykey"])
		term["otherdata"]="附加额外的数据"
	}


	var hhResut hh.HHMapResult
	hhResut.Status = "fail"
	hhResut.Code = -9999
	hhResut.Num = 0
	hhResut.Message = "数据查询失败，请检查数据格式."
	hhResut.Timestamp = time.Now().Unix()
	c.Data["json"] = maps
	c.ServeJSON()

}

