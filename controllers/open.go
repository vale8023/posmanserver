package controllers

import (
	"github.com/astaxie/beego"

	//"encoding/json"
	"github.com/astaxie/beego/orm"
	"time"
	"freshlife.io/PosmanServer/hh"
	"encoding/json"
)

type OpenController struct {
	beego.Controller
}

// URLMapping...
func (p *OpenController) URLMapping() {
	var hhResult hh.HHResult
	var f interface{}
	err1 := json.Unmarshal(p.Ctx.Input.RequestBody, &f)
	if err1 != nil {
		hhResult.Status = "fail"
		hhResult.Code = -9999
		hhResult.Message = "body 传入的JSON数据格式不正确！"
		hhResult.Timestamp = time.Now().Unix()
		p.Data["json"] = hhResult
		p.ServeJSON()
		return
	}
	p.Data["code"] = int(0)
	p.Data["Message"] = "Query Success"
	p.Data["params"] = f
}

func (p *OpenController) Prepare() {

}

//门店映射
func (p *OpenController) GetVersion() {
	params := p.Data["params"].(map[string]interface{})
	client_type := params["client_type"]
	//client_type :=  p.Ctx.Request.Form.Get("client_type")
	//连接数据库准备查询
	o := orm.NewOrm()
	//SQL准备
	//0.插入转换记录表准备
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, _ := o.Raw("SELECT * FROM gr_pos_client_version_old a WHERE a.type = ?", client_type).Values(&maps)
	//if err != nil {
	//	p.Data["errors"] = err
	//	p.Data["Status"] = "fail"
	//	p.Data["Code"] = -1
	//	p.Data["Num"] = int64(0)
	//	p.Data["Message"] = "查询失败"
	//	return
	//}
	if num < 1 {
		p.Data["errors"] = "fail"
		p.Data["status"] = "fail"
		p.Data["code"] = -1
		p.Data["message"] = "无此客户端"
		p.Data["num"] = num
		return
	} else {
		p.Data["status"] = "success"
		p.Data["code"] = 0
		p.Data["result"] = maps
		p.Data["num"] = num
		p.Data["message"] = ""
		return
	}

}

func (p *OpenController) Finish() {
	var hhResult hh.HHResult
	if p.Data["errors"] != nil {
		hhResult.Status = "fail"
		hhResult.Code = p.Data["code"].(int)
		hhResult.Num = p.Data["num"].(int64)
		hhResult.Message = p.Data["message"].(string)
	} else {
		hhResult.Status = "success"
		hhResult.Code = p.Data["code"].(int)
		hhResult.Num = p.Data["num"].(int64)
		hhResult.Data = p.Data["result"].([]orm.Params)
		hhResult.Message = p.Data["message"].(string)
	}
	hhResult.Timestamp = time.Now().Unix()
	p.Data["json"] = hhResult
	p.ServeJSON()
}
