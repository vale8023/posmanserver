package controllers

import (
	"github.com/astaxie/beego"
	"encoding/json"
	"time"
	"freshlife.io/PosmanServer/hh"
	"github.com/astaxie/beego/orm"
	"freshlife.io/PosmanServer/ws"
	"freshlife.io/PosmanServer/posman"
)

type CuptipController struct{
	beego.Controller
}

// URLMapping...
func (p *CuptipController) URLMapping(){

}

func (p *CuptipController) Prepare(){
	var hhResult hh.HHResult
	var f interface{}
	err1 := json.Unmarshal(p.Ctx.Input.RequestBody, &f)
	if err1 != nil{
		hhResult.Status = "fail"
		hhResult.Code = -9999
		hhResult.Message = "body 传入的JSON数据格式不正确！"
		hhResult.Timestamp = time.Now().Unix()
		p.Data["json"] = hhResult
		p.ServeJSON()
		return
	}
	p.Data["code"] = int(0)
	p.Data["Message"] = "Query Success"
	p.Data["params"] = f
}

func (p *CuptipController) BroadcastCuptip(){
	params := p.Data["params"].(map[string]interface{})

	cupnum :=params["cupnum"]
	shop_id  :=params["shop_id"]
	cuptip := params["cuptip"]
	order_num := params["order_num"]
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中

	o := orm.NewOrm()
	var maps1 []orm.Params
	//查询在线客户端client_id
	num, err := o.Raw("SELECT shop_id,client_id FROM  `gr_pos_client_online`  WHERE shop_id = ?", shop_id).Values(&maps1)

	if err != nil{
		p.Data["errors"] = err
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "查询失败"
		return
	}

	if num < 1 {
		p.Data["errors"] = "err"
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "客户端不在线"
		return
	}

	client_id, _ := maps1[0]["client_id"].(string)
	_, ok := ws.ClientPool[client_id] //判断客户端是否ws在线
	if ok == false {
		beego.Error(client_id, "掉线无法推送充值订单消息")
		posman.ClientLogout(client_id)

		p.Data["errors"] = "err"
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "客户端不在线"
		return
	}

	map2 := make(map[string]interface{})
	map2["cupnum"] = cupnum
	map2["shop_id"] = shop_id
	map2["cuptip"] = cuptip
	map2["order_num"] = order_num

	ws.SendEvent(client_id, "posman", "cuptip.printResult", map2)
	beego.Error("push cuptip success")
}


func (p *CuptipController) Finish(){

	var hhResult hh.HHResult
	if p.Data["errors"] != nil{
		hhResult.Status = p.Data["Status"].(string)
		hhResult.Code = p.Data["Code"].(int)
		hhResult.Num = p.Data["Num"].(int64)
		hhResult.Message = p.Data["Message"].(string)
	}else{
		hhResult.Status = "success"
		hhResult.Code = p.Data["code"].(int)
		hhResult.Num =int64(0)
		hhResult.Message = "success"
	}
	hhResult.Timestamp = time.Now().Unix()
	p.Data["json"] = hhResult
	p.ServeJSON()
}