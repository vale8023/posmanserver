package controllers

import (
	"github.com/astaxie/beego"
	//"encoding/json"
	//"time"
	//"freshlife.io/PosmanServer/hh"
	//"github.com/astaxie/beego/orm"
	//"github.com/astaxie/beego/orm"
	"github.com/bitly/go-simplejson"
	//"encoding/json"
	"github.com/astaxie/beego/orm"
)

type MeituanController struct {
	beego.Controller
}

// URLMapping...
func (p *MeituanController) URLMapping() {

}

func (p *MeituanController) Prepare() {

}

//门店映射
func (p *MeituanController) StoreMapping () {

	ePoiId :=  p.Ctx.Request.Form.Get("ePoiId")
	appAuthToken := p.Ctx.Request.Form.Get("appAuthToken")
	businessId :=p.Ctx.Request.Form.Get("businessId")
	//连接数据库准备查询
	o := orm.NewOrm()
	//SQL准备
	o.Begin()
	//0.插入转换记录表准备
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, _ := o.Raw("SELECT a.mt_appAuthToken,a.mt_businessId FROM gr_pos_shop a WHERE a.id = ? and a.status = 1",ePoiId).Values(&maps)

	if num < 1{
		o.Rollback()
		p.Data["errors"] = "fail"
		p .Data["status"]="fail"
		p .Data["message"]="无此店铺"+ePoiId
		beego.Error(p .Data["message"])
		return
	}else {
		if maps[0]["mt_businessId"] == businessId{
			businessId = "0"
		}
	}

	var db_pos_shop_update orm.RawPreparer
	var err error
	if appAuthToken != "" {
		db_pos_shop_update, _ = o.Raw("UPDATE `gr_pos_shop` SET mt_appAuthToken = ?,mt_businessId = mt_businessId + ? WHERE id = ?;").Prepare()
		defer db_pos_shop_update.Close()
		_,err = db_pos_shop_update.Exec(appAuthToken,businessId,ePoiId)

	}else {
		db_pos_shop_update, _ = o.Raw("UPDATE `gr_pos_shop` SET mt_businessId = mt_businessId - ? WHERE id = ?;").Prepare()
		defer db_pos_shop_update.Close()
		_,err = db_pos_shop_update.Exec(businessId,ePoiId)

	}


	if err != nil{
		o.Rollback()
		p.Data["errors"] = "fail"
		p .Data["status"]="fail"
		p .Data["message"]="店铺表编辑错误"
		beego.Error(p .Data["message"])
		return
	}else {
		o.Commit()
		p.Data["errors"] = nil
		p.Data["status"] = "success"
		p.Data["message"] = "店铺表编辑成功"
		beego.Error(p.Data["message"])
		return
	}
}

//心跳接口
func (p *MeituanController) Heartbeats () {
	p.Data["errors"]  = nil
	return
}

func (p *MeituanController) Finish() {
	if p.Data["errors"] != nil {
		beego.Error(3)
		cnnn := "{\"data\":\"fail\"}"
		cn_json, _ := simplejson.NewJson([]byte(cnnn))
		p.Data["json"] = cn_json
		p.ServeJSON()
	}else {
		beego.Error(4)
		cnnn := "{\"data\":\"success\"}"
		cn_json, _ := simplejson.NewJson([]byte(cnnn))
		p.Data["json"] = cn_json
		p.ServeJSON()
	}
}
