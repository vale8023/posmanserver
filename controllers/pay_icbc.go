package controllers

import (
	"github.com/astaxie/beego"
	"encoding/json"
	"time"
	"freshlife.io/PosmanServer/hh"
	"github.com/astaxie/beego/orm"
	"net/http"
	"strings"
	"fmt"
	"github.com/bitly/go-simplejson"
	//"net/url"
	"github.com/astaxie/beego/logs"
)

type PayIcbcController struct {
	beego.Controller
}

// URLMapping...
func (p *PayIcbcController) URLMapping() {

}

func (p *PayIcbcController) Prepare() {
	var hhResult hh.HHResult
	var f interface{}
	err1 := json.Unmarshal(p.Ctx.Input.RequestBody, &f)
	if err1 != nil {
		hhResult.Status = "fail"
		hhResult.Code = -9999
		hhResult.Message = "body 传入的JSON数据格式不正确！"
		hhResult.Timestamp = time.Now().Unix()
		p.Data["json"] = hhResult
		p.ServeJSON()
		return
	}
	p.Data["code"] = int(0)
	p.Data["Message"] = "Query Success"
	p.Data["params"] = f
}

func (p *PayIcbcController) PayAction() {
	params := p.Data["params"].(map[string]interface{})
	order_num := params["order_num"]
	order_amt := params["order_amt"]
	qr_code := params["qr_code"]
	shop_id := params["shop_id"]

	if order_num == nil || order_amt == nil || qr_code == nil {
		p.Data["errors"] = "fail"
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "传入参数不能为空"
		return
	}

	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	o := orm.NewOrm()
	var maps1 []orm.Params
	//查询订单结算状态结算
	num, err := o.Raw("SELECT * FROM  `gr_icbc_payment`  WHERE 	pos_order_num = ?", order_num).Values(&maps1)

	if err != nil {
		p.Data["errors"] = err
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "查询失败"
		return
	}

	if num == 0 { //首次支付
		p.Pay(order_num.(string), order_amt.(string), qr_code.(string), shop_id.(string))
	} else {
		if maps1[0]["pay_status"].(string) == "1" { //1已支付
			p.Data["errors"] = nil
			p.Data["Status"] = "success"
			p.Data["Code"] = 1
			p.Data["Num"] = int64(0)
			p.Data["result"] = maps1
			p.Data["Message"] = "支付成功"
			return
		} else { //0支付失败
			p.Data["errors"] = "FAIL"
			p.Data["Status"] = "fail"
			p.Data["Code"] = -1
			p.Data["Num"] = int64(0)
			p.Data["Message"] = maps1[0]["return_msg"]
			return
		}
	}
}

//支付结果查询
func (p *PayIcbcController) GetPayStatus() {
	params := p.Data["params"].(map[string]interface{})
	order_num := params["order_num"]
	paytype := params["paytype"]
	if order_num == nil {
		p.Data["errors"] = "fail"
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "传入参数不能为空"
		return
	}

	//p.GetOfficalPayStatusIcb(order_num.(string))
	p.GetOfficalPayStatus(paytype.(string),order_num.(string))
}

func (p *PayIcbcController) GetOfficalPayStatus (paytype string ,out_trade_no string){
	beego.Error("查询官方接口")
	resp, err := http.Post(hh.Payment_URL+"/index.php?s=/Home/index/orderquery",
		"application/x-www-form-urlencoded;charset=utf-8",
		strings.NewReader("pay_type="+paytype+"&out_trade_no="+out_trade_no))
	defer resp.Body.Close()

	if err != nil {
		fmt.Println(err)
		p.Data["errors"] = err
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "调用支付接口失败"
		return
	}

	body, err := simplejson.NewFromReader(resp.Body)
	beego.Error(body)
	logs.Error(body)

	if err != nil {
		p.Data["errors"] = err
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "读取支付结果失败"
		return
	}

	if paytype == "weixinpay" {
		result, _ := body.Get("result_code").String()

		if result == "SUCCESS" {
			payresult, _ := body.Get("result_code").String()
			trade_state, _ := body.Get("result_data").Get("trade_state").String()
			total_amount := body.Get("total_amount")
			receipt_amount:= body.Get("receipt_amount")
			if trade_state == "SUCCESS" && payresult == "SUCCESS" {
				map2 := make(map[string]interface{})
				map2["payresult"] = payresult
				map2["total_amount"] = total_amount
				map2["receipt_amount"] = receipt_amount

				var maps1 []orm.Params = []orm.Params{map2}
				p.Data["errors"] = nil
				p.Data["Status"] = "success"
				p.Data["Code"] = 1
				p.Data["Num"] = int64(0)
				p.Data["result"] = maps1
				p.Data["Message"] = "官方返回，支付成功"
			} else {
				p.Data["errors"] = "FAIL"
				p.Data["Status"] = "fail"
				p.Data["Code"] = -1
				p.Data["Num"] = int64(0)
				p.Data["Message"] = "官方返回，支付失败"
				return
			}
		} else if result == "FAIL" {
			p.Data["errors"] = "FAIL"
			p.Data["Status"] = "fail"
			p.Data["Code"] = -1
			p.Data["Num"] = int64(0)
			p.Data["Message"] = "官方返回，支付失败"
			return
		}
	}else if paytype == "alipay"{
		result_code, _ :=  body.Get("result_code").String()

		if result_code == "SUCCESS" {
			trade_status, _ :=  body.Get("result_data").Get("trade_status").String()
			total_amount := body.Get("total_amount")
			receipt_amount:= body.Get("receipt_amount")
			if trade_status == "TRADE_SUCCESS"{
				map2 := make(map[string]interface{})
				map2["payresult"] = "success"
				map2["total_amount"] = total_amount
				map2["receipt_amount"] = receipt_amount

				var maps1 []orm.Params = []orm.Params{map2}
				beego.Error(maps1)
				p.Data["errors"] = nil
				p.Data["Status"] = "success"
				p.Data["Code"] = 1
				p.Data["Num"] = int64(0)
				p.Data["result"] = maps1
				p.Data["Message"] = "官方返回，支付成功"
			}
		}else if result_code == "FAILED"{
			p.Data["errors"] = "FAIL"
			p.Data["Status"] = "fail"
			p.Data["Code"] = -1
			p.Data["Num"] = int64(0)
			p.Data["Message"] = "官方返回，支付失败"
			return
		}
	}
}
func (p *PayIcbcController) GetOfficalPayStatusIcb(order_num string) {
	beego.Error("查询官方接口")
	resp, err := http.Post(hh.Payment_URL_ICBC+"/pay/qrcodeQuery",
		"application/x-www-form-urlencoded;charset=utf-8",
		strings.NewReader("order_num="+order_num))
	defer resp.Body.Close()

	if err != nil {
		fmt.Println(err)
		p.Data["errors"] = err
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "调用支付接口失败"
		return
	}

	body, err := simplejson.NewFromReader(resp.Body)
	beego.Error(body)
	logs.Error(body)

	if err != nil {
		p.Data["errors"] = err
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "读取支付结果失败"
		return
	}
	payresult, _ := body.Get("result").Get("payStatus").String()
	if payresult == "1" {
		total_amount := body.Get("result").Get("totalAmt")
		receipt_amount := body.Get("result").Get("totalAmt")

		map2 := make(map[string]interface{})
		map2["payresult"] = "success"
		map2["total_amount"] = total_amount
		map2["receipt_amount"] = receipt_amount

		var maps1 = []orm.Params{map2}
		beego.Error(maps1)
		p.Data["errors"] = nil
		p.Data["Status"] = "success"
		p.Data["Code"] = 1
		p.Data["Num"] = int64(0)
		p.Data["result"] = maps1
		p.Data["Message"] = "官方返回，支付成功"

	} else {
		p.Data["errors"] = "FAIL"
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		a,_:=body.Get("message").String()
		p.Data["Message"] = a
		return
	}
}

//支付请求
func (p *PayIcbcController) Pay(order_num string, order_amt string, qr_code string, shop_id string) {
	resp, err := http.Post(hh.Payment_URL_ICBC+"/pay/qrcodePay",
		"application/x-www-form-urlencoded;charset=utf-8",
		strings.NewReader("order_num="+order_num+"&order_amt="+order_amt+"&qr_code="+qr_code+"&shop_id="+shop_id))
	defer resp.Body.Close()

	if err != nil {
		fmt.Println(err)
		p.Data["errors"] = err
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "调用支付接口失败"
		return
	}

	body, err := simplejson.NewFromReader(resp.Body)

	if err != nil {
		p.Data["errors"] = err
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "读取支付结果失败"
		return
	}

	beego.Error(body)
	payresult, _ := body.Get("result").Get("payStatus").String()
	if payresult == "1" {
		total_amount := body.Get("result").Get("totalAmt")
		receipt_amount := body.Get("result").Get("totalAmt")

		map2 := make(map[string]interface{})
		map2["payresult"] = "success"
		map2["total_amount"] = total_amount
		map2["receipt_amount"] = receipt_amount

		var maps1 = []orm.Params{map2}
		beego.Error(maps1)
		p.Data["errors"] = nil
		p.Data["Status"] = "success"
		p.Data["Code"] = 1
		p.Data["Num"] = int64(0)
		p.Data["result"] = maps1
		p.Data["Message"] = "官方返回，支付成功"

	} else if payresult == "0" {
		p.Data["errors"] = "FAIL"
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "支付超时，如客户已支付，系统将自动退款"

	}else {
		p.Data["errors"] = "FAIL"
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		a,_:=body.Get("message").String()
		p.Data["Message"] = a
		return
	}
}

func (p *PayIcbcController) Finish() {

	var hhResult hh.HHResult
	if p.Data["errors"] != nil {
		hhResult.Status = p.Data["Status"].(string)
		hhResult.Code = p.Data["Code"].(int)
		hhResult.Num = p.Data["Num"].(int64)
		hhResult.Data = nil
		hhResult.Message = p.Data["Message"].(string)
	} else {
		hhResult.Status = "success"
		hhResult.Code = p.Data["code"].(int)
		hhResult.Num = int64(0)
		hhResult.Message = "支付成功"
		hhResult.Data = p.Data["result"].([]orm.Params)
	}
	hhResult.Timestamp = time.Now().Unix()
	p.Data["json"] = hhResult
	p.ServeJSON()
}
