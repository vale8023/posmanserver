package controllers

import (
	"fmt"
	"github.com/astaxie/beego"
	"encoding/json"
	"github.com/bitly/go-simplejson"
	"net/http"
	"strings"
	"time"
	"freshlife.io/PosmanServer/hh"
	"github.com/astaxie/beego/orm"
	"freshlife.io/PosmanServer/ws"
	"freshlife.io/PosmanServer/posman"
)

// 材料库相关接口
type ShopController struct {
	beego.Controller
}

// URLMapping...
func (p *ShopController) URLMapping() {

}

func (p *ShopController) Prepare() {
	var hhResult hh.HHResult
	var f interface{}
	err1 := json.Unmarshal(p.Ctx.Input.RequestBody, &f)
	if err1 != nil {
		hhResult.Status = "fail"
		hhResult.Code = -9999
		hhResult.Message = "body 传入的JSON数据格式不正确！"
		hhResult.Timestamp = time.Now().Unix()
		p.Data["json"] = hhResult
		p.ServeJSON()
		return
	}
	p.Data["code"] = int(0)
	p.Data["Message"] = "Query Success"
	p.Data["params"] = f
}

// CheckManagePassword ...
// @Title Post
// @Description 校验店铺的管理密码是否正确
// @Param	method	query	string	false	"e.g. commin.find"
// @Param	appkey	query	string	false	"e.g. commin.find"
// @Param	timestamp	query	string	false	"e.g. commin.find"
// @Param	sign	query	string	false	"e.g. commin.find"
// @Param	body    body 	string true "e.g. {"shop_id":1,"shop_manage_password":"123456"}"
// @Success 201 {int} models.HhAppkey
// @Failure 403 body is empty
// @router /CheckManagePassword/ [post]
func (p *ShopController) CheckManagePassword() {
	params := p.Data["params"].(map[string]interface{})
	shop_id := params["shop_id"]
	shop_manage_password := params["shop_manage_password"].(string)

	var ucauthkey = beego.AppConfig.String("UC_AUTH_KEY")
	var passwordmd5 = think_ucenter_md5(shop_manage_password, ucauthkey);

	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw("SELECT a.* FROM gr_pos_shop a WHERE a.id = ? and a.manage_password = ? and a.status = 1", shop_id, passwordmd5).Values(&maps)
	if err != nil {
		p.Data["errors"] = err
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "查询失败"
		return
	}

	if num < 1 {
		p.Data["errors"] = "err"
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "管理密码错误！"
		return
	}

	p.Data["num"] = int64(num)
	p.Data["result"] = maps
}

//获取优惠券
func (p *ShopController) GetCoupon() {
	params := p.Data["params"].(map[string]interface{})
	//shop_id  :=params["shop_id"]
	couponid := params["couponid"]
	use_type := params["use_type"]

	n := orm.NewOrm()
	n.Using("ECV3")
	var maps1 []orm.Params
	num1, err1 := n.Raw("SELECT a.id,a.uid,a.used,a.cid,a.status,a.coupon,a.validityStartPeriod,a.validityEndPeriod,b.type,b.multi_discount,"+
		"b.consume,b.reduce,b.status cstatus "+
		"FROM gr_user_coupon a "+
		"left join gr_coupons b on a.cid = b.id "+
		"WHERE a.id = ? and (b.use_type = ? or b.use_type = 0)", couponid, use_type).Values(&maps1)

	if err1 != nil || num1 == 0 {
		p.Data["errors"] = "err"
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "无此优惠券"
		return
	}

	p.Data["num"] = int64(num1)
	p.Data["result"] = maps1
}

//获取当前订单活动
func (p *ShopController) GetPromotion() {
	params := p.Data["params"].(map[string]interface{})
	shop_id := params["shop_id"]

	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps1 []orm.Params
	//num, err := o.Raw("select * from gr_pos_maketing_order " +
	//						  "where shop_id = ? and status = 1 and delflag = 0 " +
	//						  "and begin_time <= ? and end_time >= ? and count > sell_count",shop_id,hh.NowTime(),hh.NowTime()).Values(&maps1)
	num, err := o.Raw("SELECT a.*, b.maketing_name as name,b.begin_time,b.end_time,b.rule "+
		"FROM gr_pos_maketing_order a "+
		"left join gr_pos_maketing_relation b on a.relation_id = b.id and b.maketing_type = 2 "+
		"where a.shop_id = ? and a.status = 1 and a.delflag = 0 and  b.status = 1 and b.delflag = 0 "+
		"and b.begin_time <= ? and b.end_time >= ? and a.count > a.sell_count", shop_id, hh.NowTime(), hh.NowTime()).Values(&maps1)
	if err != nil || num == 0 {
		p.Data["errors"] = "err"
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "无活动"
		return
	}

	p.Data["status"] = "success"
	p.Data["result"] = maps1
	p.Data["num"] = num
	p.Data["message"] = "活动查询成功"
}


func (p *ShopController) PayAction() {
	params := p.Data["params"].(map[string]interface{})
	order_num := params["order_num"]
	paytype := params["paytype"]
	postdata := params["postdata"]

	if order_num== nil || postdata == nil || paytype == nil{
		p.Data["errors"] = "fail"
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "传入参数不能为空"
		return
	}

	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	o := orm.NewOrm()
	var maps1 []orm.Params
	//查询订单结算状态结算
	num, err := o.Raw("SELECT total_amount,receipt_amount,status FROM  `gr_pos_payment_geteway`  WHERE 	out_trade_no = ?", order_num).Values(&maps1)

	if err != nil {
		p.Data["errors"] = err
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "查询失败"
		return
	}

	if num == 0 {//首次支付
		p.Pay(paytype.(string),postdata.(string))
	}else {
		if maps1[0]["status"].(string) == "1"{ //1已支付
			p.Data["errors"] = nil
			p.Data["Status"] = "success"
			p.Data["Code"] = 1
			p.Data["Num"] = int64(0)
			p.Data["result"] =  maps1
			p.Data["Message"] = "支付成功"
			return
		}else if  maps1[0]["status"].(string) == "0" { //0支付失败
			p.Data["errors"] = "FAIL"
			p.Data["Status"] = "fail"
			p.Data["Code"] = -1
			p.Data["Num"] = int64(0)
			p.Data["Message"] = "支付失败"
			return
		}
	}
}

func (p *ShopController) Pay(paytype string,postdata string) {
	resp, err := http.Post(hh.Payment_URL+"/index.php?s=/Home/index/barpay",
		"application/x-www-form-urlencoded;charset=utf-8",
		strings.NewReader("pay_type="+paytype +"&pay_content="+postdata))
	defer resp.Body.Close()

	if err != nil {
		fmt.Println(err)
		p.Data["errors"] = err
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "调用支付接口失败"
		return
	}

	body, err := simplejson.NewFromReader(resp.Body)

	if err != nil {
		p.Data["errors"] = err
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "读取支付结果失败"
		return
	}

	result, _ :=  body.Get("result_code").String()

	if result== "FAIL" {
		p.Data["errors"] = "FAIL"
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "支付失败"
		return
	}else {
		payresult := body.Get("result_code")
		total_amount := body.Get("total_amount")
		receipt_amount := body.Get("receipt_amount")

		map2 := make(map[string]interface{})
		map2["payresult"] = payresult
		map2["total_amount"] = total_amount
		map2["receipt_amount"] = receipt_amount

		var maps1 []orm.Params = []orm.Params{map2}
		p.Data["errors"] = nil
		p.Data["Status"] = "success"
		p.Data["Code"] = 1
		p.Data["Num"] = int64(0)
		p.Data["result"] =maps1
		p.Data["Message"] = "支付成功"
		return
	}
}


//发送消息到收银端
func (p *ShopController) SendMsgToShop() {
	params := p.Data["params"].(map[string]interface{})
	shop_id := params["shop_id"]
	msg := params["msg"]

	o := orm.NewOrm()
	var maps1 []orm.Params
	var num int64
	var err error
	//查询在线客户端client_id
	beego.Error(shop_id)
	if shop_id.(float64) == 0 {
		beego.Error(1)
		num, err = o.Raw("SELECT shop_id,client_id FROM  `gr_pos_client_online`").Values(&maps1)
	} else {
		beego.Error(2)
		num, err = o.Raw("SELECT shop_id,client_id FROM  `gr_pos_client_online`  WHERE shop_id = ?", shop_id).Values(&maps1)
	}

	if err != nil {
		p.Data["errors"] = err
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "查询失败"
		return
	}

	if num < 1 {
		p.Data["errors"] = "err"
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "客户端不在线"
		return
	}

	for _, value := range maps1 {
		client_id := value["client_id"].(string)
		_, ok := ws.ClientPool[client_id] //判断客户端是否ws在线
		if ok == false {
			beego.Error(client_id, "掉线无法推送充值订单消息")
			posman.ClientLogout(client_id)
			continue
		}

		map2 := make(map[string]interface{})
		map2["status"] = "success"
		map2["msg"] = msg
		ws.SendEvent(client_id, "posman", "shop.sendmsgtoshopResult", map2)
	}
	p.Data["errors"] = nil
	p.Data["Status"] = "success"
	p.Data["Code"] = 1
	p.Data["num"] = int64(0)
	p.Data["result"] = make([]orm.Params, 0)
	p.Data["Message"] = "发送消息成功"
	return
}

func (p *ShopController) Finish() {
	var hhResult hh.HHResult
	if p.Data["errors"] != nil {
		hhResult.Status = p.Data["Status"].(string)
		hhResult.Code = p.Data["Code"].(int)
		hhResult.Num = p.Data["Num"].(int64)
		hhResult.Message = p.Data["Message"].(string)
	} else {
		hhResult.Status = "success"
		hhResult.Code = p.Data["code"].(int)
		hhResult.Num = p.Data["num"].(int64)
		hhResult.Data = p.Data["result"].([]orm.Params)
		hhResult.Message = p.Data["Message"].(string)
	}
	hhResult.Timestamp = time.Now().Unix()
	p.Data["json"] = hhResult
	p.ServeJSON()
}
