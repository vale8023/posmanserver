package controllers

import (

	"fmt"
	"strings"
	"time"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	//"github.com/bitly/go-simplejson"
	_ "github.com/go-sql-driver/mysql"

	"encoding/json"

	"strconv"
	"freshlife.io/PosmanServer/hh"
)




// 通用的单表操作接口
type CommonController struct {
	beego.Controller
}

// URLMapping ...
func (c *CommonController) URLMapping() {
	c.Mapping("Post", c.Find)
	c.Mapping("GetById", c.GetById)
	c.Mapping("Create", c.Create)
	c.Mapping("Update", c.Update)
	c.Mapping("Delete",c.Delete)
	c.Mapping("Count",c.Count)
}

// Post ...
// @Title Post
// @Description 查询数据
// @Param	appkey	query	string	false	"e.g. commin.find"
// @Param	timestamp	query	string	false	"e.g. commin.find"
// @Param	sign	query	string	false	"e.g. commin.find"
// @Param	table	query	string	true	"e.g. hh_appkey"
// @Param	condition	query	string	true	"e.g. id>10"
// @Param	limit	query	int	true	"e.g. 100"
// @Param	offset	query	int	true	"e.g. 0"
// @Param	sort	query	string	true	"e.g. id+"
// @Success 201 {int} models.HhAppkey
// @Failure 403 body is empty
// @router /find/ [post]
func (c *CommonController) Find() {

	//获取POST请求过来的param内容
	//param := c.GetString("param")
	table := c.GetString("table")
	condition := c.GetString("condition")
	limit,_ := c.GetInt("limit")
	offset,_ := c.GetInt("offset")
	sort := c.GetString("sort")
	//logs.Debug(param)
	//将获得的param转成JSON对象
	//paramobject, err := simplejson.NewJson([]byte(param))
	//if err == nil {
	//
	//}
	//var table, _ = paramobject.Get("table").String()
	//var condition, _ = paramobject.Get("condition").String()
	//var limit, _ = paramobject.Get("limit").Int()
	//var offset, _ = paramobject.Get("offset").Int()
	//var sort, _ = paramobject.Get("sort").String()

	//连接数据库准备查询
	o := orm.NewOrm()
	//构建SQL语句
	qb, _ := orm.NewQueryBuilder("mysql")
	qb.Select("*").From(table)
	if condition != "" {
		qb.Where(condition)
	}

	//sort + -
	if sort != "" {
		if strings.ContainsRune(sort, rune('+')) {
			sort = strings.Replace(sort, "+", "", -1)
			qb.OrderBy(sort).Asc()
		} else {
			sort = strings.Replace(sort, "-", "", -1)
			qb.OrderBy(sort).Desc()
		}
	}

	if limit != 0 {
		qb.Limit(limit)
	} else {
		qb.Limit(100)
	}

	if offset != 0 {
		qb.Offset(offset)
	} else {
		qb.Offset(0)
	}

	//生成SQL语句
	sql := qb.String()

	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw(sql).Values(&maps)
	var hhResut hh.HHResult
	if err != nil{
		hhResut.Status = "fail"
		hhResut.Code = -9999
		hhResut.Num = 0
		hhResut.Message = "数据查询失败，请检查数据格式."
		hhResut.Timestamp = time.Now().Unix()
		c.Data["json"] = hhResut
		c.ServeJSON()
	}
	//遍历查询出来的数据
	//	for _, term := range maps {
	//		fmt.Println(term["id"], ":", term["appkey"], term["securitykey"])
	//	}

	//将查询后的结果转成json []byte  使用时需要用 string（b） 转成string
	//	b, encodeingerr := json.Marshal(maps)
	//	if encodeingerr == nil {

	//	}

	//构建返回结果


	hhResut.Status = "success"
	hhResut.Code = 0
	hhResut.Num = num
	hhResut.Data = maps //string(b)
	hhResut.Message = "Qurey Success"
	hhResut.Timestamp = time.Now().Unix()
	c.Data["json"] = hhResut
	c.ServeJSON()

	//fmt.Println(string(b))
	//打印总长度
	//fmt.Println("mysql row affected nums: ", num)

	//logs.Debug(table + condition + strconv.Itoa(limit) + strconv.Itoa(skip) + sort)

	//fmt.Println(js.Get("skip"))

	//	err := json.Unmarshal([]byte(findParam), &findParam)

	//c.Ctx.WriteString(string(b))
	//c.Ctx.Output.Body([]byte("hello world 222"))

}

// GetById ...
// @Title Post
// @Description 通过id查询一条数据
// @Param	appkey	query	string	false	"e.g. commin.find"
// @Param	timestamp	query	string	false	"e.g. commin.find"
// @Param	sign	query	string	false	"e.g. commin.find"
// @Param	table	query	string	true	"e.g. hh_appkey"
// @Param	id	query	string	true	"e.g. 10"
// @Success 201 {int} models.HhAppkey
// @Failure 403 body is empty
// @router /get/ [post]
func (c *CommonController) GetById() {

	table := c.GetString("table")
	id := c.GetString("id")
	//将获得的param转成JSON对象
	//paramobject, err := simplejson.NewJson([]byte(param))
	//if err == nil {
	//
	//}
	//var table, _ = paramobject.Get("table").String()
	//var id, _ = paramobject.Get("id").Int()
	//连接数据库准备查询
	o := orm.NewOrm()
	//构建SQL语句
	qb, _ := orm.NewQueryBuilder("mysql")
	qb.Select("*").From(table).Where("id = ?")
	//生成SQL语句
	sql := qb.String()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw(sql, id).Values(&maps)
	//构建返回结果
	var hhResut hh.HHResult
	if err != nil{
		hhResut.Status = "fail"
		hhResut.Code = -9999
		hhResut.Message = "数据查询失败,请检查数据格式！"
		hhResut.Timestamp = time.Now().Unix()
		c.Data["json"] = hhResut
		c.ServeJSON()
		return
	}

	hhResut.Status = "success"
	hhResut.Code = 0
	hhResut.Num = num
	hhResut.Data = maps
	hhResut.Message = "Qurey Success"
	hhResut.Timestamp = time.Now().Unix()
	c.Data["json"] = hhResut
	c.ServeJSON()
}

// Create ...
// @Title Post
// @Description 创建一条数据
// @Param	appkey	query	string	false	"e.g. commin.find"
// @Param	timestamp	query	string	false	"e.g. commin.find"
// @Param	sign	query	string	false	"e.g. commin.find"
// @Param	table	query	string	true	"e.g. hh_appkey"
// @Param	body    body 	string true "e.g. { "appkey": "55552333", "securitykey": "333333" }"
// @Success 201 {int} models.HhAppkey
// @Failure 403 body is empty
// @router /create/ [post]
func (c *CommonController) Create() {
	var hhResut hh.HHResult
	table := c.GetString("table")
	//获得Body中的数据 []byte
	requestBody := c.Ctx.Input.RequestBody

	//转化成MAP类型
	var f interface{}
	err1 := json.Unmarshal(requestBody, &f)
	if err1 != nil{
		hhResut.Status = "fail"
		hhResut.Code = -9999
		hhResut.Message = "body 传入的JSON数据格式不正确！"
		hhResut.Timestamp = time.Now().Unix()
		c.Data["json"] = hhResut
		c.ServeJSON()
		return
	}
	m := f.(map[string]interface{})
	var param1=""
	var param2=""
	for k, v := range m {
		param1=param1 + "," + k
		switch vv := v.(type) {
		case string:
			param2=param2 + ",'" + v.(string)+"'"
		case float64:
			param2=param2 + "," + strconv.FormatFloat(vv,'f',0,64)
		default:
			hhResut.Status = "fail"
			hhResut.Code = -9999
			hhResut.Message = "数据格式不正确"
			hhResut.Timestamp = time.Now().Unix()
			c.Data["json"] = hhResut
			c.ServeJSON()
			return
		}
	}
	param1=strings.TrimLeft(param1, ",")
	param1=param1+","+hh.DBCreateAt
	param2=strings.TrimLeft(param2, ",")
 	param2=param2+ "," + hh.NowTime()

	//连接数据库准备查询
	o := orm.NewOrm()
	var sql =fmt.Sprintf("INSERT INTO %s (%s) VALUES (%s)",table,param1,param2)
	res, err := o.Raw(sql).Exec()
	if err != nil {
		hhResut.Status = "fail"
		hhResut.Code = -9999
		hhResut.Message = "数据库插入失败,请检查数据格式！"
		hhResut.Timestamp = time.Now().Unix()
		c.Data["json"] = hhResut
		c.ServeJSON()
		return
	}

	//查询出来
	lastInsertId ,_ :=res.LastInsertId()
	//构建SQL语句
	qb, _ := orm.NewQueryBuilder("mysql")
	qb.Select("*").From(table).Where("id = ?")
	//生成SQL语句
	sql2 := qb.String()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err2 := o.Raw(sql2, lastInsertId).Values(&maps)
	if err2 == nil{
	}
	//构建返回结果
	hhResut.Status = "success"
	hhResut.Code = 0
	hhResut.Num = num
	hhResut.Data =maps
	hhResut.Message = "Insert Success"
	hhResut.Timestamp = time.Now().Unix()
	c.Data["json"] = hhResut
	c.ServeJSON()

}


// Update ...
// @Title Post
// @Description 通过条件修改一条数据
// @Param	appkey	query	string	false	"e.g. commin.find"
// @Param	timestamp	query	string	false	"e.g. commin.find"
// @Param	sign	query	string	false	"e.g. commin.find"
// @Param	table	query	string	true	"e.g. hh_appkey"
// @Param	condition	query	string	true	"id=10"
// @Param	body    body 	string true "e.g. { "appkey": "55552333", "securitykey": "333333" }"
// @Success 201 {int} models.HhAppkey
// @Failure 403 body is empty
// @router /update/ [post]
func (c *CommonController) Update() {

	var hhResut hh.HHResult
	table := c.GetString("table")
	condition := c.GetString("condition")
	//获得Body中的数据 []byte
	requestBody := c.Ctx.Input.RequestBody
	//转化成MAP类型
	var f interface{}
	err1 := json.Unmarshal(requestBody, &f)
	if err1 != nil{
		hhResut.Status = "fail"
		hhResut.Code = -9999
		hhResut.Message = "body 传入的JSON数据格式不正确！"
		hhResut.Timestamp = time.Now().Unix()
		c.Data["json"] = hhResut
		c.ServeJSON()
		return
	}
	m := f.(map[string]interface{})
	var param1=""
	//var param2=""
	for k, v := range m {

		switch vv := v.(type) {
		case string:
			//Address = 'Zhongshan 23',
			param1 =param1 + "," + k + "= '" +v.(string)+"'"
			//param2=param2 + ",'" + v.(string)+"'"
		case float64:
			param1 =param1 + "," + k + "= "+ strconv.FormatFloat(vv,'f',0,64)
			//param2=param2 + "," + strconv.FormatFloat(vv,'f',0,64)
		default:
			hhResut.Status = "fail"
			hhResut.Code = -9999
			hhResut.Message = "数据格式不正确"
			hhResut.Timestamp = time.Now().Unix()
			c.Data["json"] = hhResut
			c.ServeJSON()
			return
		}
	}
	param1=strings.TrimLeft(param1, ",")
	param1=param1+", "+ hh.DBUpdateAt +"="+hh.NowTime()
	//param2=strings.TrimLeft(param2, ",")

	//连接数据库准备查询
	o := orm.NewOrm()
 	var sql =fmt.Sprintf("UPDATE %s SET %s where %s",table,param1,condition)
	res, err := o.Raw(sql).Exec()
	if err != nil {
		hhResut.Status = "fail"
		hhResut.Code = -9999
		hhResut.Message = "数据库修改失败,请检查数据格式！"
		hhResut.Timestamp = time.Now().Unix()
		c.Data["json"] = hhResut
		c.ServeJSON()
		return
	}
	//查询出来
	lastInsertId ,_ :=res.LastInsertId()
	if lastInsertId>0{}
	//fmt.Println(lastInsertId)
	//构建SQL语句
	qb, _ := orm.NewQueryBuilder("mysql")
	qb.Select("*").From(table).Where(condition)
	//生成SQL语句
	sql2 := qb.String()

	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err2 := o.Raw(sql2).Values(&maps)
	if err2 == nil{
	}
	//构建返回结果
	hhResut.Status = "success"
	hhResut.Code = 0
	hhResut.Num = num
	hhResut.Data =maps
	hhResut.Message = "Update Success"
	hhResut.Timestamp = time.Now().Unix()
	c.Data["json"] = hhResut
	c.ServeJSON()

}


// Delete ...
// @Title Post
// @Description 通过条件删除一些数据
// @Param	appkey	query	string	false	"e.g. commin.find"
// @Param	timestamp	query	string	false	"e.g. commin.find"
// @Param	sign	query	string	false	"e.g. commin.find"
// @Param	table	query	string	true	"e.g. hh_appkey"
// @Param	condition	query	string	true	"id=10"
// @Success 201 {int} models.HhAppkey
// @Failure 403 body is empty
// @router /delete/ [post]
func (c *CommonController) Delete() {

	var hhResut hh.HHResult
	table := c.GetString("table")
	condition := c.GetString("condition")

	o := orm.NewOrm()


	//真删除
	//var sql =fmt.Sprintf("DELETE FROM %s WHERE %s",table,condition)
	//假删除
	var sql =fmt.Sprintf("UPDATE %s SET %s=1 WHERE %s",table,hh.DBDelfalg,condition)
	res, err := o.Raw(sql).Exec()
	if err != nil {
		hhResut.Status = "fail"
		hhResut.Code = -9999
		hhResut.Message = "数据库删除失败,请检查数据格式！"
		hhResut.Timestamp = time.Now().Unix()
		c.Data["json"] = hhResut
		c.ServeJSON()
		return
	}

	rownum,_ := res.RowsAffected()

	//构建返回结果
	hhResut.Status = "success"
	hhResut.Code = 0
	hhResut.Num = rownum
	hhResut.Message = "Delete Success"
	hhResut.Timestamp = time.Now().Unix()
	c.Data["json"] = hhResut
	c.ServeJSON()
}


// Count ...
// @Title Post
// @Description 统计符合条件的数量
// @Param	appkey	query	string	false	"e.g. commin.find"
// @Param	timestamp	query	string	false	"e.g. commin.find"
// @Param	sign	query	string	false	"e.g. commin.find"
// @Param	table	query	string	true	"e.g. hh_appkey"
// @Param	condition	query	string	true	"e.g. id>1"
// @Success 201 {int} models.HhAppkey
// @Failure 403 body is empty
// @router /count/ [post]
func (c *CommonController) Count() {

	table := c.GetString("table")
	condition := c.GetString("condition")
	//将获得的param转成JSON对象
	//paramobject, err := simplejson.NewJson([]byte(param))
	//if err == nil {
	//
	//}
	//var table, _ = paramobject.Get("table").String()
	//var id, _ = paramobject.Get("id").Int()
	//连接数据库准备查询
	o := orm.NewOrm()
	//构建SQL语句
	qb, _ := orm.NewQueryBuilder("mysql")
	qb.Select("COUNT(id) as counts").From(table).Where(condition)
	//生成SQL语句
	sql := qb.String()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw(sql).Values(&maps)
	//构建返回结果
	var hhResut hh.HHResult
	if err != nil{
		hhResut.Status = "fail"
		hhResut.Code = -9999
		hhResut.Message = "数据查询失败,请检查数据格式！"
		hhResut.Timestamp = time.Now().Unix()
		c.Data["json"] = hhResut
		c.ServeJSON()
		return
	}

	hhResut.Status = "success"
	hhResut.Code = 0
	hhResut.Num = num
	hhResut.Data = maps
	hhResut.Message = "Count Success"
	hhResut.Timestamp = time.Now().Unix()
	c.Data["json"] = hhResut
	c.ServeJSON()
}


