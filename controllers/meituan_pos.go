package controllers

import (
	"github.com/astaxie/beego"
	"encoding/json"
	"time"
	"freshlife.io/PosmanServer/hh"
	"github.com/astaxie/beego/orm"
	//"net/http"
	//"net/url"
	"strings"
	"net/http"
	"github.com/bitly/go-simplejson"
	"errors"
)
const (
	SignKey = "si9ysg0ly70tg6rn"
	DeveloperId = "101555"
)

type MeituanPosController struct {
	beego.Controller
}

func (p *MeituanPosController) Prepare() {
	var hhResult hh.HHResult
	var f interface{}
	err1 := json.Unmarshal(p.Ctx.Input.RequestBody, &f)
	if err1 != nil {
		hhResult.Status = "fail"
		hhResult.Code = -9999
		hhResult.Message = "body 传入的JSON数据格式不正确！"
		hhResult.Timestamp = time.Now().Unix()
		p.Data["json"] = hhResult
		p.ServeJSON()
		return
	}
	p.Data["code"] = int(0)
	p.Data["Message"] = "Query Success"
	p.Data["params"] = f
}

func (p *MeituanPosController) QueryById() {
	params := p.Data["params"].(map[string]interface{})
	shop_id := params["shop_id"]
	couponCode := params["couponCode"]// 券码
	timestampnow := hh.NowTime()
	appAuthToken,err :=p.GetappAuthToken(shop_id.(string),1)//店铺授权码

	if err != nil{
		return
	}

	signstr := SignKey+"appAuthToken"+appAuthToken+"charsetUTF-8couponCode"+couponCode.(string)+"timestamp"+timestampnow+"version1"

	sign := hh.Sha1(signstr)

	url := "http://api.open.cater.meituan.com/tuangou/coupon/queryById?appAuthToken="+appAuthToken+"&charset=UTF-8&timestamp="+timestampnow+"&version=1&sign="+ strings.ToLower(sign)+"&couponCode="+couponCode.(string)
	resp, err := http.Get(url)
	defer resp.Body.Close()

	if err != nil {
		p.Data["errors"] = "err"
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "从美团获取信息失败"
		return
	}

	body, err := simplejson.NewFromReader(resp.Body)
	if err != nil {
		p.Data["errors"] = err
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "读取返回结果失败"
		return
	}

	beego.Error(body)

	if body.Get("error") != nil{
		p.Data["errors"] = "err"
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)

		cnnn := body.Get("error")
		p.Data["Message"],_ = cnnn.Get("message").String()
		return
	}

	p.Data["errors"] = nil
	p.Data["Status"] = "fail"
	p.Data["Code"] = 1
	p.Data["Num"] = int64(0)
	p.Data["Message"] = "查询成功"
	p.Data["result"] = body
	beego.Error(p.Data["json"])
}

func (p *MeituanPosController) CheckPrepare() {
	params := p.Data["params"].(map[string]interface{})
	shop_id := params["shop_id"]
	couponCode := params["couponCode"]//券码
	timestampnow := hh.NowTime()
	appAuthToken,err :=p.GetappAuthToken(shop_id.(string),1)//店铺授权码

	if err != nil{
		return
	}

	signstr := SignKey+"appAuthToken"+appAuthToken+"charsetUTF-8couponCode"+couponCode.(string)+"timestamp"+timestampnow+"version1"

	sign := hh.Sha1(signstr)
	url := "http://api.open.cater.meituan.com/tuangou/coupon/prepare?appAuthToken="+appAuthToken+"&charset=UTF-8&timestamp="+timestampnow+"&version=1&sign="+ strings.ToLower(sign)+"&couponCode="+couponCode.(string)
	beego.Error(url)
	resp, err := http.Post (url,"application/x-www-form-urlencoded;charset=utf-8",
		strings.NewReader(""))
	defer resp.Body.Close()

	if err != nil {
		p.Data["errors"] = "err"
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "从美团获取信息失败"
		return
	}

	body, err := simplejson.NewFromReader(resp.Body)
	if err != nil {
		p.Data["errors"] = err
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "读取返回结果失败"
		return
	}

	result,_ :=body.Get("error").String()

	if result != ""{
		p.Data["errors"] = "err"
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)

		cnnn := body.Get("error")
		p.Data["Message"],_ = cnnn.Get("message").String()
		return
	}

	f,_:= body.Map()
	beego.Error(f)
	var maps1 []orm.Params = []orm.Params{f}

	p.Data["errors"] = nil
	p.Data["Status"] = "fail"
	p.Data["Code"] = 1
	p.Data["Num"] = int64(0)
	p.Data["Message"] = "查询成功"
	p.Data["result"] =maps1
}

func (p *MeituanPosController)Consume()  {//验券
	params := p.Data["params"].(map[string]interface{})
	eId := params["shop_id"]
	couponCode := params["couponCode"]//券码
	count := params["count"]//验券数量
	eName :=  params["eName"]//店铺名称.
	eOrderId := params["eOrderId"]//系统订单号
	timestampnow := hh.NowTime()
	appAuthToken,err :=p.GetappAuthToken(eId.(string),1)//店铺授权码
	if err != nil{
		return
	}

	signstr := SignKey+"appAuthToken"+appAuthToken+"charsetUTF-8count"+count.(string)+"couponCode"+couponCode.(string)+"eId"+eId.(string)+"eName"+eName.(string)+"eOrderId"+eOrderId.(string)+"timestamp"+timestampnow+"version1"

	beego.Error(signstr)
	sign := hh.Sha1(signstr)

	url := "http://api.open.cater.meituan.com/tuangou/coupon/consume?appAuthToken="+appAuthToken+"&charset=UTF-8&timestamp="+timestampnow+"&version=1&sign="+
			strings.ToLower(sign)+"&couponCode="+couponCode.(string)+"&count="+count.(string)+"&eId="+eId.(string)+"&eName="+eName.(string)+"&eOrderId="+eOrderId.(string)
	beego.Error(url)
	resp, err := http.Post(url,"application/x-www-form-urlencoded;charset=utf-8",
		strings.NewReader(""))
	defer resp.Body.Close()

	if err != nil {
		p.Data["errors"] = "err"
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "从美团获取信息失败"
		return
	}

	body, err := simplejson.NewFromReader(resp.Body)
	if err != nil {
		p.Data["errors"] = err
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)

		p.Data["Message"] = "读取返回结果失败"
		return
	}

	f,_:= body.Map()
	beego.Error(f)
	var maps1 []orm.Params = []orm.Params{f}

	_,ok:=f["orderId"]
	if ok {

		p.Data["errors"] = nil
		p.Data["Status"] = "success"
		p.Data["Code"] = 1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "查询成功"
		p.Data["result"] = maps1
	}else {
		p.Data["errors"] = "err"
		p.Data["Status"] = "fail"
		p.Data["Code"] = 1
		p.Data["Num"] = int64(0)
		cnnn := body.Get("error")
		p.Data["Message"] ,_= cnnn.Get("message").String()
		p.Data["result"] = maps1
	}
}

//心跳
func (p *MeituanPosController) Heartbeat() {
	params := p.Data["params"].(map[string]interface{})
	ePoiId := params["ePoiId"]
	posId := params["posId"]//终端唯一标识

	map2 := make(map[string]interface{})
	map2["ePoiId"] = ePoiId
	map2["developerId"] = DeveloperId
	map2["time"] = hh.NowTime()
	map2["posId"] =posId
	beego.Error(map2)

	b,_:= json.Marshal(map2)

	beego.Error(string(b))
	datavalue :=string(b)
	signstr := SignKey+"data"+datavalue
	beego.Error(signstr)

	sign := hh.Sha1(signstr)

	url := "http://heartbeat.meituan.com/pos/heartbeat?data="+datavalue+"&sign="+sign
	beego.Error(url)
	resp, _ := http.Post(url,"application/x-www-form-urlencoded;charset=utf-8",
		strings.NewReader(""))
	defer resp.Body.Close()


	body, _ := simplejson.NewFromReader(resp.Body)

	beego.Error(body)
	f,_:= body.Map()
	beego.Error(f)
	var maps1 []orm.Params = []orm.Params{f}
	p.Data["errors"] = nil
	p.Data["Status"] = "success"
	p.Data["Code"] = 1
	p.Data["Num"] = int64(0)
	p.Data["Message"] = "成功"
	p.Data["result"] = maps1
}


func (p *MeituanPosController) Finish() {

	var hhResult hh.HHResult
	if p.Data["errors"] != nil {
		beego.Error(p.Data["errors"])
		hhResult.Status = p.Data["Status"].(string)
		hhResult.Code = p.Data["Code"].(int)
		hhResult.Num = p.Data["Num"].(int64)
		hhResult.Data = nil
		hhResult.Message = p.Data["Message"].(string)
		beego.Error(hhResult)
	} else {
		hhResult.Status = "success"
		hhResult.Code = p.Data["code"].(int)
		hhResult.Num = int64(0)
		hhResult.Message =  p.Data["Message"].(string)
		hhResult.Data = p.Data["result"].([]orm.Params)
	}
	hhResult.Timestamp = time.Now().Unix()
	p.Data["json"] = hhResult

	beego.Error(p.Data["json"])
	p.ServeJSON()
}

func (p *MeituanPosController)GetappAuthToken(shop_id string,shoptype int)  (string,error){
	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw("SELECT a.mt_appAuthToken,a.mt_businessId FROM gr_pos_shop a WHERE a.id = ? and a.status = 1 and a.mt_businessId in (?,3) ",shop_id,shoptype).Values(&maps)

	if err != nil {
		p.Data["errors"] = "err"
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "查询失败"
		return "",err
	}

	if num < 1 {
		p.Data["errors"] = "err"
		p.Data["Status"] = "fail"
		p.Data["Code"] = -1
		p.Data["Num"] = int64(0)
		p.Data["Message"] = "该店已禁用或未开通美团功能"
		error := errors.New( "该店已禁用或未开通美团功能")
		return "", error
	}
	return  maps[0]["mt_appAuthToken"].(string) ,nil
}

