FROM daocloud.io/library/golang:1.7
MAINTAINER Cao MengLong<138888611@qq.com>
COPY PosmanServer $GOPATH/bin/
RUN chmod 777 $GOPATH/bin/PosmanServer
COPY conf $GOPATH/bin/conf/
WORKDIR $GOPATH/bin/
EXPOSE 8080
ENTRYPOINT  $GOPATH/bin/PosmanServer