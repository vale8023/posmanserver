package hh

import (
	"github.com/daviddengcn/go-colortext"
	"fmt"
	"github.com/astaxie/beego"
	"time"
	"strconv"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"crypto/sha1"
	"runtime"
	"encoding/hex"
)

//程序初始化时调用
func Run() {
	welcome()
}

//欢迎画面
func  welcome(){
	ct.Foreground(ct.Yellow, false)
	fmt.Println("##############################################################")
	fmt.Println("# ___  ___  ___  ___                                          ")
	fmt.Println("# | |__| |  | |__| | 扬州鲜生活电子商务有限公司                  ")
	fmt.Println("# | ____ |  | ____ | XSH.PosMan 服务端程序 v1.0.1              ")
	fmt.Println("# | |__| |  | |__| | https://github.com/CaoMengLong/HHServer  ")
	fmt.Println("# | ____ |  | ____ |                                          ")
	fmt.Println("# |_|  |_|  |_|  |_| 基于开源软件HHServer开发                   ")
	fmt.Println("##############################################################")

	fmt.Println("CPU 内核数量：\t",runtime.NumCPU())
	fmt.Println("当前运行模式：\t",beego.BConfig.RunMode);
	fmt.Println("是否开启了安全认证：\t",EnableSgin);
	fmt.Println("是否显示安全认证过程：\t",ShowTrace);

	ct.ResetColor()
}


//获取当前的系统时间戳 文本类型
func NowTime() string{
	return strconv.FormatInt(time.Now().Unix(),10)
}


//hmacsha256算法加密 文本类型
func ComputeHmac256(message string, secret string) string {
	key := []byte(secret)
	h := hmac.New(sha256.New, key)
	h.Write([]byte(message))
	return base64.StdEncoding.EncodeToString(h.Sum(nil))
}

//SHA1加密
func Sha1(data string) string {
	sha1 := sha1.New()
	sha1.Write([]byte(data))
	return hex.EncodeToString(sha1.Sum([]byte("")))
}