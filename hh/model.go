package hh


import (
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego"
	"os"
	"strings"

	"github.com/bitly/go-simplejson"
)


var DBDelfalg string
var DBCreateAt string
var DBUpdateAt string
var DBAppKeyTable string
var ShowTrace bool
var EnableSgin bool
var DBConfig string
var DBConfig_ECV3 string
var Payment_URL string
var Payment_URL_ICBC string

func init(){


	//读取app.conf中的参数
	DBDelfalg = beego.AppConfig.String("DBDelfalg")
	DBCreateAt = beego.AppConfig.String("DBCreateAt")
	DBUpdateAt = beego.AppConfig.String("DBUpdateAt")
	DBAppKeyTable = beego.AppConfig.String("DBAppKeyTable")




	//读取环境变量中的参数
	runmode := os.Getenv("GRSERVER_RUNMODE")
	beego.BConfig.RunMode = runmode

	DBConfig =os.Getenv("GRSERVER_DBCONFIG")

	DBConfig_ECV3 =os.Getenv("GRSERVER_DBCONFIG_ECV3")
	beego.Error(DBConfig_ECV3)

	Payment_URL =os.Getenv("GRPAYMENT_URL")
	beego.Error(Payment_URL)

	Payment_URL_ICBC =os.Getenv("GRPAYMENT_URL_ICBC")
	beego.Error(Payment_URL_ICBC)

	ShowTraceStr := os.Getenv("GRSERVER_SHOWTRACE")

	if strings.EqualFold(ShowTraceStr,"true"){		ShowTrace=true
	}else{
		ShowTrace=false
	}

	EnableSginStr := os.Getenv("GRSERVER_ENABLESGIN")
	if strings.EqualFold(EnableSginStr,"true"){
		EnableSgin=true
	}else{
		EnableSgin=false
	}


}

type HHResult struct {
	Status    string
	Code      int
	Num       int64
	Timestamp int64
	Message   string
	Data      []orm.Params
}


type HHMapResult struct {
	Status    string
	Code      int
	Num       int64
	Timestamp int64
	Message   string
	Data      []map[string] interface{}
}


//WS用
type EventType int

const (
	EVENT_JOIN = iota
	EVENT_LEAVE
	EVENT_MESSAGE
)


type EventContent struct {
	Player string
	Model string
	Method string
	Data  *simplejson.Json
}

type Event struct {
	Type      EventType
	ClientID     string
	Timestamp int
	Content   EventContent
}