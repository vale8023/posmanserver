package hh

import (
	"net/http"
	"strings"
	"fmt"
	"io/ioutil"
	"time"

)

var DingTalk DingTalkClass

type DingTalkClass struct {
	lastRebotSendTime int64
}


func (c *DingTalkClass) RebotSendMsgToIT(context string){

	if time.Now().Unix()-c.lastRebotSendTime <1000 {
		return
	}

	//
	resp, err := http.Post("https://oapi.dingtalk.com/robot/send?access_token=02a42ace1a7145b21db112ec4dc419bacbda8aee370a5e15bd1152c71139b7b3",
		"application/json; charset=utf-8",
		strings.NewReader("{'msgtype': 'text', 'text': {'content': '"+context+"'}}"))
	defer resp.Body.Close()


	if err != nil {
		fmt.Println(err)
	}

	c.lastRebotSendTime = time.Now().Unix()


	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {

	}
	fmt.Println(string(body))

}

//重要消息
func (c *DingTalkClass) RebotSendImportantMsgToIT(context string){

	resp, err := http.Post("https://oapi.dingtalk.com/robot/send?access_token=02a42ace1a7145b21db112ec4dc419bacbda8aee370a5e15bd1152c71139b7b3",
		"application/json; charset=utf-8",
		strings.NewReader("{'msgtype': 'text', 'text': {'content': '"+context+"'}}"))
	defer resp.Body.Close()


	if err != nil {
		fmt.Println(err)
	}

	c.lastRebotSendTime = time.Now().Unix()


	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {

	}
	fmt.Println(string(body))

}
