package main

import (
	_ "freshlife.io/PosmanServer/routers"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
	"freshlife.io/PosmanServer/hh"

	"github.com/astaxie/beego/logs"

	"runtime"
)

func init() {
	orm.RegisterDataBase("default", "mysql", hh.DBConfig)
	beego.Error("default",hh.DBConfig)
	beego.Error("ECV3",hh.DBConfig_ECV3)
	orm.RegisterDataBase("ECV3", "mysql", hh.DBConfig_ECV3)

	orm.Debug = true //是否开启SQL 输出
	logs.SetLogger(logs.AdapterFile,`{"filename":"project.log","level":7,"maxlines":0,"maxsize":0,"daily":true,"maxdays":10}`)
}

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	if beego.BConfig.RunMode == "dev" {
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"
	}

	hh.Run()
	beego.Run()
}