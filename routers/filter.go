package routers

import (
	"github.com/astaxie/beego/context"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "net/url"
	"time"
	"strings"
	"strconv"
	"github.com/daviddengcn/go-colortext"
	"encoding/json"
	"freshlife.io/PosmanServer/hh"
	"net/url"
	"fmt"
)

func init()  {
	//启用授权认证过滤器

	if hh.ShowTrace{
		beego.InsertFilter("/*",beego.BeforeRouter,FilterDebugSginTrace)
	}

	if hh.EnableSgin {
		beego.InsertFilter("/v1/*",beego.BeforeRouter,FilterAuth)//只验证v1接口
	}

}




//API KEY 方式的安全认证方法
var FilterAuth = func(ctx *context.Context) {

	var hhResult hh.HHResult
	appkey :=ctx.Input.Query("appkey")
	sign := ctx.Input.Query("sign")
	timestamp := ctx.Input.Query("timestamp")

	url :=ctx.Input.URL()

	servertimestamp := time.Now().Unix()
	clienttimestamp ,_  := strconv.ParseInt(timestamp,10,64)
	timediff :=servertimestamp-clienttimestamp //请求相差多少秒

	//请求间隔60秒以上
	if(timediff>60000){
		hhResult.Status = "fail"
		hhResult.Code = -99999999
		hhResult.Message = "您的请求已经过期。"
		hhResult.Timestamp = time.Now().Unix()
		ctx.Output.JSON(hhResult,true,true)

		beego.Error("您的请求已经过期。")

		return
	}



	//判断是否必填了 4个验证参数
	//通过appkey查询出securitykey 进行校验
	o := orm.NewOrm()
	//构建SQL语句
	qb, _ := orm.NewQueryBuilder("mysql")
	qb.Select("securitykey").From(hh.DBAppKeyTable).Where("appkey = ? AND status = 1")
	//生成SQL语句
	sql := qb.String()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw(sql, appkey).Values(&maps)

	if err!=nil {
		//数据库查询错误报错
		hhResult.Status = "fail"
		hhResult.Code = -99999999
		hhResult.Message = "密钥查询失败。"
		hhResult.Timestamp = time.Now().Unix()
		ctx.Output.JSON(hhResult,true,true)
		return
	}
	if num==0{
		//没有查询到Appkey
		hhResult.Status = "fail"
		hhResult.Code = -99999999
		hhResult.Message = "AppKey不存在，请不要请求。"
		hhResult.Timestamp = time.Now().Unix()
		ctx.Output.JSON(hhResult,true,true)
		beego.Error("AppKey不存在，请不要请求。")
		return
	}

	//得到加密密钥
	securitykey := maps[0]["securitykey"].(string)
	//拼接待加密的字符串
	waitesecuritystr:=fmt.Sprintf("%s?appkey=%s&timestamp=%s",url,appkey,timestamp)
	serversgin := hh.ComputeHmac256(waitesecuritystr,securitykey)





	if hh.ShowTrace {
		ct.Foreground(ct.Yellow, false)
		fmt.Println("--------------------   加密跟踪  --------------------")
		//fmt.Println("请求方式:\t",ctx.Request.Method)
		//fmt.Println("请求URL:\t",ctx.Request.URL)
		//fmt.Println("FormData数据:\t",ctx.Request.Form)
		//fmt.Println("Body数据:\t",ctx.Request.Body)
		fmt.Println("待加密的数据:\t",waitesecuritystr)
		fmt.Println("加密密钥:\t",securitykey)
		fmt.Println("客户端的sgin:\t",sign)
		fmt.Println("服务端的sgin:\t",serversgin)
		ct.ResetColor()
	}

	v:=strings.EqualFold(sign,serversgin)
	if !v {
		hhResult.Status = "fail"
		hhResult.Code = -99999999
		hhResult.Message = "签名校验失败，传入的sign值不正确。"
		hhResult.Timestamp = time.Now().Unix()
		beego.Error("签名校验失败，传入的sign值不正确。")
		ctx.Output.JSON(hhResult,true,true)

		return
	}

	//转化成MAP类型



	if len(ctx.Input.RequestBody) >0{
		var f interface{}
		err1 := json.Unmarshal(ctx.Input.RequestBody, &f)

		if err1 != nil && url !="/ws/erp" {
			hhResult.Status = "fail"
			hhResult.Code = -9999
			hhResult.Message = "body 传入的JSON数据格式不正确！"
			hhResult.Timestamp = time.Now().Unix()
			beego.Error("body 传入的JSON数据格式不正确！")
			ctx.Output.JSON(hhResult,true,true)
			return
		}
	}


}


//显示页面调试信息
var FilterDebugSginTrace = func(ctx *context.Context) {

	ct.Foreground(ct.Yellow, false)
	fmt.Println("--------------------   请求跟踪  --------------------")
	fmt.Println("请求方式:\t",ctx.Request.Method)
	decodeurl ,_ := url.QueryUnescape(ctx.Request.URL.String()) //url deco
	fmt.Println("请求URL:\t",decodeurl)
	fmt.Println("FormData数据:\t",ctx.Request.Form)
	fmt.Println("Body数据:\t",ctx.Request.Body)
	ct.ResetColor()
}