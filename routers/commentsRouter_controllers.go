package routers

import (
	"github.com/astaxie/beego"
)

func init() {

	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:CommonController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:CommonController"],
		beego.ControllerComments{
			Method: "Find",
			Router: `/find/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:CommonController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:CommonController"],
		beego.ControllerComments{
			Method: "GetById",
			Router: `/get/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:CommonController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:CommonController"],
		beego.ControllerComments{
			Method: "Create",
			Router: `/create/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:CommonController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:CommonController"],
		beego.ControllerComments{
			Method: "Update",
			Router: `/update/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:CommonController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:CommonController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/delete/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:CommonController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:CommonController"],
		beego.ControllerComments{
			Method: "Count",
			Router: `/count/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:DemoController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:DemoController"],
		beego.ControllerComments{
			Method: "Add",
			Router: `/add/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:DemoController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:DemoController"],
		beego.ControllerComments{
			Method: "Add2",
			Router: `/add2/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:DemoController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:DemoController"],
		beego.ControllerComments{
			Method: "JsonDemo1",
			Router: `/json1/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:DemoController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:DemoController"],
		beego.ControllerComments{
			Method: "JsonDemo2",
			Router: `/json2/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:ShopController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:ShopController"],
		beego.ControllerComments{
			Method: "CheckManagePassword",
			Router: `/CheckManagePassword/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:ShopController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:ShopController"],
		beego.ControllerComments{
			Method: "SendMsgToShop",
			Router: `/SendMsgToShop/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:CuptipController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:CuptipController"],
		beego.ControllerComments{
			Method: "BroadcastCuptip",
			Router: `/BroadcastCuptip/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})
	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:PayController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:PayController"],
		beego.ControllerComments{
			Method: "PayAction",
			Router: `/PayAction/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})
	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:MeituanController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:MeituanController"],
		beego.ControllerComments{
			Method: "StoreMapping",
			Router: `/StoreMapping/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})
	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:MeituanController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:MeituanController"],
		beego.ControllerComments{
			Method: "Heartbeats",
			Router: `/Heartbeats/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})
	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:MeituanPosController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:MeituanPosController"],
		beego.ControllerComments{
			Method: "QueryById",
			Router: `/QueryById/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})
	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:MeituanPosController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:MeituanPosController"],
		beego.ControllerComments{
			Method: "CheckPrepare",
			Router: `/CheckPrepare/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})
	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:MeituanPosController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:MeituanPosController"],
		beego.ControllerComments{
			Method: "Consume",
			Router: `/Consume/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})
	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:MeituanPosController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:MeituanPosController"],
		beego.ControllerComments{
			Method: "Heartbeat",
			Router: `/Heartbeat/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})
	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:ShopController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:ShopController"],
		beego.ControllerComments{
			Method: "GetCoupon",
			Router: `/GetCoupon/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})
	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:ShopController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:ShopController"],
		beego.ControllerComments{
			Method: "GetPromotion",
			Router: `/GetPromotion/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})
	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:OnlineOrderController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:OnlineOrderController"],
		beego.ControllerComments{
			Method: "OnlineOrderHandling",
			Router: `/OnlineOrderHandling/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})
	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:PayController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:PayController"],
		beego.ControllerComments{
			Method: "GetPayStatus",
			Router: `/GetPayStatus/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})
	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:PayIcbcController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:PayIcbcController"],
		beego.ControllerComments{
			Method: "PayAction",
			Router: `/PayAction/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:PayIcbcController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:PayIcbcController"],
		beego.ControllerComments{
			Method: "GetPayStatus",
			Router: `/GetPayStatus/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:OpenController"] = append(beego.GlobalControllerRouter["freshlife.io/PosmanServer/controllers:OpenController"],
		beego.ControllerComments{
			Method: "GetVersion",
			Router: `/GetVersion/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})
}
