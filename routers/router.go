// @APIVersion 1.0.0
// @Title HHServer API在线测试工具
// @Description 这个工具可以帮助你快速调试HHServer API，同时也是一个文档中心。
// @Contact caomenglong788@gmail.com
// @TermsOfServiceUrl https://github.com/CaoMengLong/HHServer
// @License Apache 2.0
// @LicenseUrl http://www.apache.org/licenses/LICENSE-2.0.html
package routers

import (
	"freshlife.io/PosmanServer/controllers"
	"github.com/astaxie/beego"
	"freshlife.io/PosmanServer/ws"
)

func init() {
	ns := beego.NewNamespace("/v1",
		beego.NSNamespace("/common",
			beego.NSInclude(
				&controllers.CommonController{},
			),
		),
		beego.NSNamespace("/shop",
			beego.NSInclude(
				&controllers.ShopController{},
			),
		),
		beego.NSNamespace("/demo",
			beego.NSInclude(
				&controllers.DemoController{},
			),
		),
		beego.NSNamespace("/cuptip",
			beego.NSInclude(
				&controllers.CuptipController{},
			),
		),
		beego.NSNamespace("/pay",
			beego.NSInclude(
				&controllers.PayController{},
			),
		),
		beego.NSNamespace("/meituan_pos",
			beego.NSInclude(
				&controllers.MeituanPosController{},
			),
		),
		beego.NSNamespace("/onlineorder",
			beego.NSInclude(
				&controllers.OnlineOrderController{},
			),
		),
		beego.NSNamespace("/payicbc",
			beego.NSInclude(
				&controllers.PayIcbcController{},
			),
		),
	)
	mt := beego.NewNamespace("/mt",
		beego.NSNamespace("/meituan",
			beego.NSInclude(
				&controllers.MeituanController{},
			),
		), )
	open := beego.NewNamespace("/open",
		beego.NSInclude(
			&controllers.OpenController{},
		), )
	beego.AddNamespace(ns)
	beego.AddNamespace(mt)
	beego.AddNamespace(open)

	//WebScoket服务入口
	beego.Router("/ws/erp", &ws.WebSocketController{}, "get:Upgrader")
}
