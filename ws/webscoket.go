package ws

import (
	"github.com/astaxie/beego"
	"github.com/gorilla/websocket"
	"net/http"

	"fmt"

	"time"
)

type WebSocketController struct {
	beego.Controller
}



func (this *WebSocketController) Upgrader() {

	ClientID :=  this.GetString("ClientID")

	fmt.Println(ClientID)
	beego.Error(ClientID)

	//将http请求 升级为 websocket 协议
	ws, err := websocket.Upgrade(this.Ctx.ResponseWriter, this.Ctx.Request, nil, 1024, 1024)
	if _, ok := err.(websocket.HandshakeError); ok {
		http.Error(this.Ctx.ResponseWriter, "Not a websocket handshake", 400)
		return
	} else if err != nil {
		beego.Error("Cannot setup WebSocket connection:", err)
		return
	}

	defer ws.Close()


	//连接成功后
	var CurrentClient Client
	CurrentClient.ClientID = ClientID
	CurrentClient.Conn = ws

	//ws正常断开时执行
	ws.SetCloseHandler(func(int,string) error{
		LeaveClient(CurrentClient)
		return nil
	})


	ws.SetReadDeadline(time.Now().Add(10 * time.Second))
	ws.SetPongHandler(func(string) error {
		ws.SetReadDeadline(time.Now().Add(10 * time.Second))
		//beego.Error("执行了 SetPongHandler 事件")
		if err := ws.WriteControl(websocket.PongMessage, []byte{}, time.Now().Add(3 * time.Second)); err != nil {
			beego.Error("pong:", err)
		}
		return nil
	})
	ws.SetPingHandler(func(string) error {
		//beego.Error("执行了 SetPingHandler 事件")
		if err := ws.WriteControl(websocket.PingMessage, []byte{}, time.Now().Add(10 * time.Second)); err != nil {
			beego.Error("ping:", err)
		}
		return nil
	})


	JoinClient(CurrentClient)

	for {
		mtype, message, err := ws.ReadMessage()

		if err != nil {
			beego.Error(mtype,err.Error())
			if(mtype ==-1){
				LeaveClient(CurrentClient)
			}
			return
		}

		var m Message
		m.ClientID =ClientID
		m.Message =message
		ReceiveMessagechan <- m

	}



}
