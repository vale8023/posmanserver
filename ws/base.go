package ws

import (
	"github.com/astaxie/beego"
	"freshlife.io/PosmanServer/hh"
)

type BaseService struct {
	ModelName string
}


func (this *BaseService)modelNotFount(event hh.Event)  {
	data := make(map[string] interface{})
	beego.Error("Player:",event.Content.Player," Model:",event.Content.Model," Method:",event.Content.Method,"模型Model不存在")
	data["message"]=event.Content.Model + ",模型Model不存在！"
	SendEvent(event.ClientID,this.ModelName,event.Content.Method,data)
}


func (this *BaseService)methodNotFount(event hh.Event)  {
	data := make(map[string] interface{})
	beego.Error("Player:",event.Content.Player," Model:",event.Content.Model," Method:",event.Content.Method,"方法Method不存在")
	data["message"]=event.Content.Method + "方法Method不存在！"
	SendEvent(event.ClientID,this.ModelName,event.Content.Method,data)
}


func (this *BaseService)SendResult(event hh.Event,data map[string] interface{})  {
	SendEvent(event.ClientID,this.ModelName,event.Content.Method+"Result",data)
}

