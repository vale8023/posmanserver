package ws

import (
	"github.com/gorilla/websocket"
	"github.com/astaxie/beego"

	"freshlife.io/PosmanServer/posman"
)


//定义客户端结构
type Client struct {
	ClientID string
	Conn *websocket.Conn
}

//存放客户端的Map数组
var ClientPool map[string] Client = make(map[string] Client)

func JoinClient(client Client){
	joinchan <- client
}

func LeaveClient(client Client){
	posman.ClientLogout(client.ClientID)  //连接断开时 posman 进行相关处理
	leavechan <- client
}


func isClientExist(c Client) bool {
	_ ,ok := ClientPool[c.ClientID]
	if ok != false{
		return true
	}else{
		return false
	}
}


var (
	joinchan = make(chan Client, 20)
	leavechan = make(chan Client, 20)
)



func clientGoroutine() {
	for {
		select {
		case client := <-joinchan:
			if isClientExist(client) == false {
				ClientPool[client.ClientID]= client
				beego.Info("新客户端",client.ClientID,"登陆成功,当前在线数量：",len(ClientPool))
			}else{
				ClientPool[client.ClientID].Conn.Close()
				delete(ClientPool,client.ClientID)
				ClientPool[client.ClientID]= client
				beego.Info("老客户端",client.ClientID,"登陆成功,当前在线数量：",len(ClientPool))
			}

		case client := <-leavechan:
			//用户存在池中 离开后需要移除 并断开连接
			if isClientExist(client) == true {
				client.Conn.Close() //断开连接
				delete(ClientPool,client.ClientID)
				beego.Info("客户端",client.ClientID,"断开连接成功,当前在线数量：",len(ClientPool))
			}
		}
	}
}

func init()  {
	go clientGoroutine()
}