package ws

import (
	"github.com/bitly/go-simplejson"
	"time"
	"encoding/json"
	"github.com/astaxie/beego"
	"github.com/gorilla/websocket"
	"freshlife.io/PosmanServer/hh"

)



type Message struct {
	ClientID string
	Message []byte
}

var (
	ReceiveMessagechan = make(chan Message, 10)
	SendMessagechan = make(chan Message, 10)
	ServicePool = make(map[string] chan hh.Event)
)




func checkReceiveJsonFormat(msgJson *simplejson.Json) bool{
	_ , isExitPlayer := msgJson.CheckGet("Player")
	_ , isExitModel := msgJson.CheckGet("Model")
	_ , isExitMethod := msgJson.CheckGet("Method")
	_ , isExitData := msgJson.CheckGet("Data")
	if isExitPlayer &&  isExitModel && isExitMethod && isExitData {
		return  true
	}else {
		return false
	}
}

func ReceiveEvent(ep hh.EventType, clientID string, msgJson *simplejson.Json) hh.Event {
	var eventcontent hh.EventContent
	eventcontent.Player,_ = msgJson.Get("Player").String()
	eventcontent.Model,_ = msgJson.Get("Model").String()
	eventcontent.Method,_ = msgJson.Get("Method").String()
	eventcontent.Data = msgJson.Get("Data")
	return hh.Event{ep,clientID,int(time.Now().Unix()),eventcontent}
}


func SendEvent(clientID string,model string,method string,data map[string]interface{}){
	f := make(map[string] interface{})
	f["Type"]=hh.EVENT_MESSAGE
	f["ClientID"]=clientID
	f["Timestamp"]=int(time.Now().Unix())
	z := make(map[string] interface{})
	z["Player"]="Client"
	z["Model"]=model
	z["Method"]=method
	z["Data"]=data
	f["Content"]=z
	r,_:= json.Marshal(f)
	var m Message
	m.ClientID =clientID
	m.Message =r

	_ ,ok1 := ClientPool[clientID] //判断客户端是否ws在线
	if ok1 != false{
		SendMessagechan <- m
	}

}


func modelNotFount(event hh.Event)  {
	data := make(map[string] interface{})
	beego.Error("Player:",event.Content.Player," Model:",event.Content.Model," Method:",event.Content.Method,"模型Model不存在")
	data["message"]=event.Content.Model + ",模型Model不存在！"
	SendEvent(event.ClientID,event.Content.Model,event.Content.Method,data)
}


func eventhubGoroutine() {
	for {
		select {
		case message := <-ReceiveMessagechan:
			messagejson, err := simplejson.NewJson(message.Message)

			if err != nil || checkReceiveJsonFormat(messagejson) != true {
				beego.Error(string(message.Message))
				beego.Error("客户端发送来的数据格式，未遵循约定的结构。")
				LeaveClient(ClientPool[message.ClientID])
				break
			}else{
				beego.Info(string(message.Message))
			}

			event := ReceiveEvent(hh.EVENT_MESSAGE,message.ClientID,messagejson)

			//判断Model是否存在ServicePool中
			if _, ok := ServicePool[event.Content.Model]; ok {
				//存在
				ServicePool[event.Content.Model] <- event
			}else {
				//不存在
				modelNotFount(event)

			}

		case message := <-SendMessagechan:

			//defer func() {
			//	if v := recover(); v != nil {
			//		beego.Error(v)
			//	}
			//}()

			ws := ClientPool[message.ClientID].Conn
			err :=ws.WriteMessage(websocket.TextMessage,message.Message)
			if err != nil {
				return
			}

		}
	}
}

func init()  {

	go eventhubGoroutine()
}