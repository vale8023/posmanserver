package ws

import (
	"freshlife.io/PosmanServer/posman"
	"freshlife.io/PosmanServer/hh"
)

/*
收银系统v2 服务端业务scoket接口
 */

type PosmanService struct {
	BaseService
}

func init() {
	model := new(PosmanService)
	model.ModelName = "posman"
	model.Run()
}

func (this *PosmanService) Run() {
	ServicePool[this.ModelName] = make(chan hh.Event)

	for i := 0; i < 10000; i++ {
		go this.Goroutine()
	}

}

func (this *PosmanService) Goroutine() {

	////屏蔽报错
	//defer func() {
	//	if v := recover(); v != nil {
	//		beego.Error(v)
	//	}
	//}()

	for {
		select {
		case event := <-ServicePool[this.ModelName]:
			switch event.Content.Method {

			case "synch.down.test":
				//下行单向同步数据测试接口
				Synch := new(posman.SynchController)
				this.SendResult(event, Synch.DownDataTest(event))
				break
			case "synch.up.test":
				//下行单向同步数据测试接口
				Synch := new(posman.SynchController)
				this.SendResult(event, Synch.UpDataTest(event))
				break
			case "client.login":
				//客户端软件连接登录
				User := new(posman.UserController)
				this.SendResult(event, User.ClientLogin(event))
				break
			case "user.shop.login":
				//店铺账号登录
				User := new(posman.UserController)
				this.SendResult(event, User.ShopLogin(event))
				break
			case "user.cashier.login":
				//收银员登录
				User := new(posman.UserController)
				this.SendResult(event, User.CashierLogin(event))
				break
			case "synch.down.productcategor":
				//下载单向同步商品分类数据接口
				Synch := new(posman.SynchController)
				this.SendResult(event, Synch.DownProductCategor(event))
				break
			case "synch.down.product":
				//下载单向同步商品分类数据接口
				//posman.Pospal.PospalCardSwith("55A61321")
				Synch := new(posman.SynchController)
				this.SendResult(event, Synch.DownProduct(event))
				break
			case "synch.down.shop.stock":
				//下载单向同步店铺商品库存数据接口
				Synch := new(posman.SynchController)
				this.SendResult(event, Synch.DownShopStock(event))
				break
			case "synch.down.payway":
				//下载单向同步店铺商品库存数据接口
				Synch := new(posman.SynchController)
				this.SendResult(event, Synch.DownPayWay(event))
				break
			case "synch.up.orderpackage":
				//上传订单包
				Synch := new(posman.SynchController)
				this.SendResult(event, Synch.UpOrderPackage(event))
				break
			case "synch.down.activity.limitrule":
				//下载限量促销活动规则表
				Synch := new(posman.SynchController)
				this.SendResult(event, Synch.DownActivityLimitRule(event))
				break

			case "product.search":
				//获取某门店库存信息数据
				Product := new(posman.ProductController)
				this.SendResult(event, Product.Search(event))
				break
			case "product.stock.search":
				//获取某门店库存信息数据
				Product := new(posman.ProductController)
				this.SendResult(event, Product.StockSearch(event))
				break
			case "product.stock.andproductsearch":
				//获取某门店库存信息数据
				Product := new(posman.ProductController)
				this.SendResult(event, Product.StockAndProductSearch(event))
				break

			case "product.spec.changerule":
				//获取商品规格转换规则
				Product := new(posman.ProductController)
				this.SendResult(event, Product.GetSpecChangeRule(event))
				break
			case "product.spec.changeaction":
				//执行商品规格转换
				Product := new(posman.ProductController)
				this.SendResult(event, Product.SpecChangeAction(event))
				break
			case "product.freshcutfruits.makeaction":
				//执行鲜果切制作
				Product := new(posman.ProductController)
				this.SendResult(event, Product.FreshcutfruitsMakeAction(event))
				break
			case "product.stock.outaction":
				//执行报损，榨汁出库
				Product := new(posman.ProductController)
				this.SendResult(event, Product.StockOutAction(event))
				break
			case "order.search":
				//订单查询
				Order := new(posman.OrderController)
				this.SendResult(event, Order.Search(event))
				break
			case "order.goods.search":
				//订单查询
				Order := new(posman.OrderController)
				this.SendResult(event, Order.OrderGoodsSearch(event))
				break
			case "order.payway":
				//获取支付方式
				Order := new(posman.OrderController)
				this.SendResult(event, Order.GetPayWay(event))
				break
			case "card.search":
				//会员卡查询
				Card := new(posman.CardController)
				this.SendResult(event, Card.Search(event))
				break
			case "card.searchbyhardwarenum":
				//会员卡查询通过硬件编码
				Card := new(posman.CardController)
				this.SendResult(event, Card.SearchByHardWareNum(event))
				break
			case "card.add":
				//会员卡添加接口
				Card := new(posman.CardController)
				this.SendResult(event, Card.Add(event))
				break
			case "card.edit":
				//会员卡编辑接口
				Card := new(posman.CardController)
				this.SendResult(event, Card.Edit(event))
				break
			case "card.rechargerule":
				//会员卡充值方案活动接口
				Card := new(posman.CardController)
				this.SendResult(event, Card.GetRechargeRule(event))
				break
			case "card.rechargeaction":
				//会员卡充值接口
				Card := new(posman.CardController)
				this.SendResult(event, Card.RechargeAction(event))
				break
			case "card.rechargenotify":
				//会员卡充值订单通知回调接口
				Card := new(posman.CardController)
				this.SendResult(event, Card.RechargeNotify(event))
				break
			case "complete.changeshiftaction":
				//交接班操作接口
				Complete := new(posman.CompleteController)
				this.SendResult(event, Complete.ChangeShiftAction(event))
				break
			case "complete.overalldaliyaction":
				//日结操作接口
				Complete := new(posman.CompleteController)
				this.SendResult(event, Complete.OverAllDaliyAction(event))
				break
			case "stock.whetherstockcheck":
				//日结范围内是否已经盘点过
				Stock := new(posman.StockController)
				this.SendResult(event, Stock.WhetherStockCheck(event))
				break
			case "stock.getstockcheckdatabyid":
				//日结范围内是否已经盘点过
				Stock := new(posman.StockController)
				this.SendResult(event, Stock.GetStockCheckDataByID(event))
				break
			case "shop.searchorder":
				Shop := new(posman.ShopController)
				this.SendResult(event, Shop.SearchOrder(event))
				break
			case "pospal.getpospalcardinfo":
				Pospal := new(posman.PospalController)
				this.SendResult(event, Pospal.GetPosPalCardInfo(event))
				break
			case "pospal.pospalswitchtoxsh":
				Pospal := new(posman.PospalController)
				this.SendResult(event, Pospal.PospalSwitchtoXSH(event))
				break
			case "shop.pricechanged":
				Shop := new(posman.ShopController)
				this.SendResult(event, Shop.PriceChanged(event))
				break
			case "user.offlinelogin":
				User := new(posman.UserController)
				this.SendResult(event, User.OfflineLogin(event))
				break
			case "product.productPriceTag":
				Product := new(posman.ProductController)
				this.SendResult(event, Product.GetProductPriceTag(event))
				break
			case "synch.down.maketinggoods":
				Synch := new(posman.SynchController)
				this.SendResult(event, Synch.DownMaketingGoods(event))
				break
			case "synch.down.maketinggoodsdetail":
				Synch := new(posman.SynchController)
				this.SendResult(event, Synch.DownMaketingGoodsDetail(event))
				break
			case "synch.down.selfnutritioninfo":
				Synch := new(posman.SynchController)
				this.SendResult(event, Synch.DownProductNutrition(event))
				break
			case "synch.down.selfservicegroup":
				Synch := new(posman.SynchController)
				this.SendResult(event, Synch.DownSelfServiceGroup(event))
				break
			case "synch.down.selfservicegroupproduct":
				Synch := new(posman.SynchController)
				this.SendResult(event, Synch.DownSelfServiceGroupProduct(event))
				break
			case "synch.down.selfserviceproduct":
				Synch := new(posman.SynchController)
				this.SendResult(event, Synch.DownSelfServiceProduct(event))
				break
			case "synch.down.selfserviceproductnutrition":
				Synch := new(posman.SynchController)
				this.SendResult(event, Synch.DownSelfServiceProductNutrition(event))
				break
			case "stock.getstockcheckdata":
				//日结范围内是否已经盘点过
				Stock := new(posman.StockController)
				this.SendResult(event, Stock.GetStockCheckData(event))
				break
			default:
				//this.methodNotFount(event)
				break
			}
		}
	}
}
