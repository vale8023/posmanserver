package ws

import (
	"github.com/astaxie/beego/orm"

	"freshlife.io/PosmanServer/hh"
)

type Chat struct {
	BaseService
	OnLineUserClientPool map[string] interface{}
}



func init()  {
	chat := new(Chat)
	chat.ModelName="chat"
	chat.Run()

}

func (this *Chat) Run(){
	ServicePool[this.ModelName] = make(chan hh.Event)
	this.OnLineUserClientPool = make(map[string] interface{})
	go this.Goroutine()
}

func (this *Chat)Goroutine(){
	for {
		select {
		case event := <-ServicePool[this.ModelName]:

			switch event.Content.Method  {
			case "Login":
				this.Login(event)
				break
			case "Logout":
				this.Logout(event)
				break
			case "Reg":
				this.Reg(event)
				break
			case "Send":
				this.Send(event)
				break
			case "ShowOnLineUser":
				this.ShowOnLineUser()
				break
			default:
				this.methodNotFount(event)
				break
			}

		}
	}
}


func (this *Chat)Login(event hh.Event)  {

	name , _:= event.Content.Data.Get("name").String()
	pwd , _:= event.Content.Data.Get("pwd").String()


	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM ws_qq  WHERE name = ? and pwd = ?",name,pwd).Values(&maps)
	if err != nil{
		return
	}

	data := make(map[string] interface{})
	if num > 0 {
		this.OnLineUserClientPool[event.ClientID]=name
		data["ok"]=1
	}else {
		data["ok"]=0
	}



	SendEvent(event.ClientID,this.ModelName,event.Content.Method+"Result",data)
}


func (this *Chat)Logout(event hh.Event)  {
	delete(this.OnLineUserClientPool,event.ClientID)
	this.ShowOnLineUser()
}

func (this *Chat)Reg(event hh.Event)  {

	name , _:= event.Content.Data.Get("name").String()
	pwd , _:= event.Content.Data.Get("pwd").String()


	//连接数据库准备查询
	o := orm.NewOrm()
	//将数据库中查询结果映射到maps中，并返回查询的数量至num中
	var maps []orm.Params
	num, err := o.Raw("SELECT * FROM ws_qq  WHERE name = ? ",name).Values(&maps)
	if err != nil{
		return
	}

	data := make(map[string] interface{})
	if num > 0 {
		data["ok"]=-1
		SendEvent(event.ClientID,this.ModelName,event.Content.Method+"Result",data)
		return
	}

	_,insertErr := o.Raw("insert into ws_qq (name,pwd) values (?,?) ",name,pwd).Exec();
	if insertErr != nil{
		data["ok"]=0
	}else{
		data["ok"]=1
	}
	SendEvent(event.ClientID,this.ModelName,event.Content.Method+"Result",data)

}

func (this *Chat)Send(event hh.Event)  {
	name , _:= event.Content.Data.Get("name").String()
	msg , _:= event.Content.Data.Get("msg").String()
	data := make(map[string] interface{})
	data["ok"]=1
	data["name"]=name
	data["msg"]=msg

	SendEvent(event.ClientID,this.ModelName,event.Content.Method+"Result",data)

	this.SendNewMsgToAllUser(name,msg,event.ClientID)

}

func (this *Chat)SendNewMsgToAllUser(fromuser string,msg string,clientId string)  {
	data := make(map[string] interface{})
	data["name"]=fromuser
	data["msg"]=msg
	data["clientId"]=clientId
	for _, k := range ClientPool {
		 SendEvent(k.ClientID,this.ModelName,"SendNewMsgToAllUserServer",data)
	}
}




func (this *Chat)ShowOnLineUser()  {

	data := make(map[string] interface{})

	userlist := make([] map[string] interface{},len(this.OnLineUserClientPool))

	i := 0
	for key := range this.OnLineUserClientPool {

		d := make(map[string] interface{})
		d["ClientID"]=key
		d["UserName"]=this.OnLineUserClientPool[key]
		userlist[i]=d
		i++
	}

	data["UserList"]=userlist

	for _, k := range ClientPool {
		SendEvent(k.ClientID,this.ModelName,"ShowOnLineUserServer",data)
	}
}