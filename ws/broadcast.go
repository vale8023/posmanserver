package ws

//广播服务
import (
	"time"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego"
	"strconv"
	"freshlife.io/PosmanServer/posman"
	"freshlife.io/PosmanServer/hh"
	"github.com/astaxie/beego/logs"
	"errors"
)

type BroadcastService struct {
	BaseService
}

func init() {
	Service := new(BroadcastService)
	Service.Run()
}

func (this *BroadcastService) Run() {
	this.RechargeOrderBroadcast()
	this.OnLineInfo()
	this.OnlineOrderBroadcast()
}

//充值订单完成通知广播
func (c *BroadcastService) OnLineInfo() {
	ticker := time.NewTicker(time.Second * 30)

	go func() {
		for _ = range ticker.C {
			beego.Info("当前客户端在线数量:", len(ClientPool))

		}
	}()
}

//充值订单完成通知广播
func (c *BroadcastService) RechargeOrderBroadcast() {
	ticker := time.NewTicker(time.Second * 10)

	go func() {

		//屏蔽报错
		defer func() {
			if v := recover(); v != nil {
				beego.Error(v)
			}
		}()

		for _ = range ticker.C {

			data := make(map[string]interface{})

			o := orm.NewOrm()

			//查询出那些店铺的充值订单是需要推送的
			var maps []orm.Params
			num, err := o.Raw("SELECT a.shop_id,b.client_id,count(1) cnt FROM `gr_pos_member_charge_order` a INNER JOIN `gr_pos_client_online` b ON a.shop_id = b.shop_id WHERE a.notify_flag=0 and b.server_id = ? group by a.shop_id,b.client_id", posman.ServerID).Values(&maps)
			if err != nil {
				continue
			}
			if num < 1 {
				beego.Info("尚无充值订单需要广播推送.1")
				continue
			}

			//开始对这些店发送消息
			for k := 0; k < len(maps); k++ {

				shop_id, _ := strconv.ParseFloat(maps[k]["shop_id"].(string), 64)
				client_id, _ := maps[k]["client_id"].(string)

				_, ok := ClientPool[client_id] //判断客户端是否ws在线
				if ok == false {
					beego.Error(client_id, "掉线无法推送充值订单消息")
					posman.ClientLogout(client_id)
					continue
				}

				var maps2 []orm.Params
				num, err := o.Raw("SELECT a.createAt,d.card_num,d.name as card_name,d.phone as card_phone,d.amount as card_amount, a.member_id,a.order_num,a.type,a.shop_id,a.client_id,a.notify_flag,a.create_by,c.nickname as create_by_name,a.charge_amount,a.give_amount,a.total_amount,a.payway_id,b.name as payway_name,a.final_amount from (select * FROM `gr_pos_member_charge_order` WHERE shop_id=? AND notify_flag=0) a LEFT JOIN `gr_pos_payway` b ON a.payway_id = b.id LEFT JOIN `gr_member` c ON a.create_by = c.uid LEFT JOIN `gr_pos_member_card` d ON a.member_id = d.id", shop_id).Values(&maps2)
				if err != nil {
					continue
				}
				if num < 1 {
					beego.Info("尚无充值订单需要广播推送.2")
					continue
				}

				data["status"] = "success"
				data["num"] = num
				data["data"] = maps2
				data["message"] = "将充值成功的订单推送给客户端！"

				_, ok1 := ClientPool[client_id] //判断客户端是否ws在线
				if ok1 == false {
					beego.Error(client_id, "掉线无法推送充值订单消息")
					posman.ClientLogout(client_id)
					continue
				} else {
					//发送通知给收银机客户端软件
					createAt, _ := strconv.ParseInt(maps2[k]["createAt"].(string), 10, 64)
					nowTime, _ := strconv.ParseInt(hh.NowTime(), 10, 64)

					if nowTime-createAt > 20 { //创建大于10秒的订单才能发送通知
						SendEvent(client_id, "posman", "card.rechargeactionResult", data)
					}

				}

			}

		}
	}()
}

//在线订单定时处理任务
func (c *BroadcastService) OnlineOrderBroadcast() {
	ticker := time.NewTicker(time.Second * 4)

	isRuning := false

	go func() {
		defer func() {
			if v := recover(); v != nil {
				beego.Error(v)
			}
		}()

		for _ = range ticker.C {

			if isRuning == true {
				continue //之前还没有运行成功
			}

			isRuning = true
			t1 := time.Now() // get current time

			o := orm.NewOrm()

			//查询出需要待出库的线上订单
			var maps []orm.Params
			num, err := o.Raw("SELECT * FROM `gr_pos_online_order` WHERE synch=0 AND type=1 LIMIT 5").Values(&maps)
			if err != nil {
				isRuning = false
				continue
			}
			if num < 1 {
				//beego.Info("尚无线上销售订单处理")
				isRuning = false
				continue
			}

			////1.更新线上订单总表
			db_online_order_update, _ := o.Raw("UPDATE `gr_pos_online_order` SET `synch` = '9', `updateAt` = unix_timestamp() WHERE `id` = ? AND updateAt=?").Prepare()
			defer db_online_order_update.Close()

			////2.更新线上订单明细表
			db_online_order_goods_update, _ := o.Raw("UPDATE `gr_pos_online_order_goods` SET `final_single_cost` = ?,`stock_count` = ?, `profit` = ?, `updateAt` = unix_timestamp() WHERE `id` = ?").Prepare()
			defer db_online_order_goods_update.Close()

			//遍历这些店铺

			for k := 0; k < len(maps); k++ {
				thisOrderCanResume := true
				order_id, _ := strconv.ParseInt(maps[k]["id"].(string), 10, 0)
				shop_id, _ := strconv.ParseInt(maps[k]["shop_id"].(string), 10, 0)
				order_num, _ := maps[k]["order_num"].(string)
				order_updateAt, _ := strconv.ParseInt(maps[k]["updateAt"].(string), 10, 0)

				o.Begin()

				//查询线上订单的详情
				var maps2 []orm.Params
				num2, err2 := o.Raw("SELECT ifnull(b.id,0) as pos_product_id ,e.statistic_classify,b.name as pos_product_name,b.spec_name as pos_product_spec_name,a.* "+
					"FROM gr_pos_online_order c "+
					"left join `gr_pos_online_order_goods` a on c.order_num = a.order_num "+
					"LEFT JOIN `gr_pos_product` b ON a.product_code = b.code "+
					"LEFT JOIN gr_pos_product_category e on b.category_id = e.id "+
					"WHERE a.order_num = ? AND c.type = 1", order_num).Values(&maps2)
				if err2 != nil {

					continue
				}
				if num2 < 1 {
					beego.Info("线上订单居然没有详情信息")
					_, err00 := db_online_order_update.Exec(order_id, order_updateAt)
					if err00 != nil {

						o.Rollback()
						continue
					}

					o.Commit()
					continue
				}

				//遍历订单商品的详情
				for i := 0; i < len(maps2); i++ {

					goods_id, _ := strconv.ParseInt(maps2[i]["id"].(string), 10, 0)

					pos_product_id, _ := strconv.ParseInt(maps2[i]["pos_product_id"].(string), 10, 0)
					statistic_classify, _ := strconv.ParseInt(maps2[i]["statistic_classify"].(string), 10, 0)

					//无库存商品不判断
					if statistic_classify == 1 {
						continue;
					}

					//判断线上商品与收银系统中的商品是否匹配

					if (pos_product_id == 0) {
						beego.Error("大哥，线上订单同步失败啦，线上订单商品与收银系统中的商品编码不匹配啊！,订单号：", order_num)
						hh.DingTalk.RebotSendMsgToIT("大哥，线上订单同步失败啦，线上订单商品与收银系统中的商品编码不匹配啊！,订单号：" + order_num)
					}

					out_count, _ := strconv.ParseFloat(maps2[i]["count"].(string), 64)
					finnal_total_price, _ := strconv.ParseFloat(maps2[i]["finnal_total_price"].(string), 64)

					if pos_product_id < 1 {
						thisOrderCanResume = false
						break
					}

					var shop_stock_count float64;
					stock_single_price, shop_stock_count, resultbool, err3 := c.productStcokOutActionReturnStockSinglePrice(o, shop_id, pos_product_id, out_count, 8000, order_num)

					if err3 != nil || resultbool == false {
						if err3.Error() == "出库失败，出库商品库存量不购减。" ||err3.Error() == "查询商品明细表失败。" {
							ofail := orm.NewOrm()
							_, errfail1 := ofail.Raw("UPDATE `gr_pos_online_order_goods` SET `stock_count` = ?, `updateAt` = unix_timestamp() WHERE `id` = ?", shop_stock_count, goods_id).Exec()
							_, errfail2 := ofail.Raw("UPDATE `gr_pos_online_order` SET `synch` = '-999', `updateAt` = unix_timestamp() WHERE `id` = ?", order_id).Exec()

							if errfail1 != nil || errfail2 != nil {
								beego.Error("fail")
							}

						}

						o.Rollback()

						thisOrderCanResume = false
						break
					}

					//反更新线上订单明细
					_, err4 := db_online_order_goods_update.Exec(stock_single_price, shop_stock_count, finnal_total_price-out_count*stock_single_price, goods_id)
					if err4 != nil {
						o.Rollback()
						thisOrderCanResume = false
						break
					}
				}

				if thisOrderCanResume == false {

					o.Rollback()
					continue
				}

				//反更新线上订单完成同步

				_, err5 := db_online_order_update.Exec(order_id, order_updateAt)

				if err5 != nil {
					o.Rollback()
					continue
				}

				beego.Error("success")
				o.Commit()
			}
			isRuning = false

			elapsed := time.Since(t1)

			beego.Info("线上订单同步执行成功，共", len(maps), "条订单数据， 耗时：", elapsed)

		}
	}()
}

//商品出库操作带事务的
func (c *BroadcastService) productStcokOutActionReturnStockSinglePrice(o orm.Ormer, shop_id int64, product_id int64, out_count float64, out_type int, out_order_num string) (float64, float64, bool, error) {

	////2.更新出库商品的总表
	db_out_stock_update, _ := o.Raw("UPDATE `gr_pos_shop_stock` SET `count` = ?,  `stock_total_price` = ?, `row_version` = unix_timestamp() WHERE `gr_pos_shop_stock`.`product_id` = ? and `gr_pos_shop_stock`.`shop_id` = ?").Prepare()
	defer db_out_stock_update.Close()
	//3.更新出库商品明细表
	db_out_stock_detail_insert, _ := o.Raw("INSERT INTO `gr_pos_shop_stock_detail` (`shop_id`, `product_id`, `origin_stock`, `change_count`, `current_count`, `type`, `order_num`, `stock_single_price`, `origin_stock_total_price`, `change_stock_total_price`, `current_stock_total_price`, `status`, `createAt`, `updateAt`, `delflag`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1, unix_timestamp() ,unix_timestamp(), '0')").Prepare()
	defer db_out_stock_detail_insert.Close()

	//开始出库操作
	var maps []orm.Params

	var stock_single_price float64
	var stock_count float64

	num, err := o.Raw("SELECT a.*,b.name as product_name  FROM `gr_pos_shop_stock` a INNER JOIN gr_pos_product b WHERE a.`shop_id` = ? AND a.`product_id` = ? and a.`product_id`=b.id", shop_id, product_id).Values(&maps)
	if err == nil && num > 0 {

		stock_count, _ = strconv.ParseFloat(maps[0]["count"].(string), 64)

		if maps[0]["stock_single_price"] == nil {
			if maps[0]["stock_total_price"] == nil {
				o.Rollback()
				logs.Error(-4999, "出库单出库失败，", out_order_num, "，库存金额，和库存总价为空。")
				hh.DingTalk.RebotSendMsgToIT("大哥，线上订单同步失败啦，商品ID：" + strconv.FormatInt(product_id, 10) + " ，的居然库存金额，和库存总价为空。订单号：" + out_order_num)
				return 0.00, stock_count, false, errors.New("出库失败，出库商品库存金额，和库存总价为空。")
			}
		}

		stock_single_price, _ = strconv.ParseFloat(maps[0]["stock_single_price"].(string), 64)
		stock_total_price, _ := strconv.ParseFloat(maps[0]["stock_total_price"].(string), 64)

		//当前库存数量小于要出库的数量 得报错
		if stock_count < float64(out_count) {
			o.Rollback()
			logs.Error(-4999, "出库单出库失败，", out_order_num, "，出库商品库存量不购减。")
			hh.DingTalk.RebotSendMsgToIT("大哥，线上订单同步失败啦，商品ID：" + strconv.FormatInt(product_id, 10) + " ，的库存数量不购减。订单号：" + out_order_num)
			return 0.00, stock_count, false, errors.New("出库失败，出库商品库存量不购减。")
		}

		//本商品的出库金额

		out_stock_amount := stock_single_price * float64(out_count)

		//出库后的剩余金额

		stock_total_price_after := stock_total_price - out_stock_amount

		//出库后的剩余数量
		stock_count_after := stock_count - float64(out_count)

		//更新库存表

		////2.更新出库商品的总表
		_, err2 := db_out_stock_update.Exec(stock_count_after, stock_total_price_after, product_id, shop_id)
		if err2 != nil {

			o.Rollback()
			logs.Error(-4999, "出库单出库失败，", out_order_num, "，更新出库总表数据执行失败。")
			return 0.00, stock_count, false, errors.New("出库失败，更新出库总表数据执行失败。")

		}

		//更新库存明细表

		//3.更新出库商品明细表
		_, err3 := db_out_stock_detail_insert.Exec(shop_id, product_id, stock_count, out_count, stock_count_after, out_type, out_order_num, stock_single_price, stock_total_price, out_stock_amount, stock_total_price_after)
		if err3 != nil {

			o.Rollback()
			logs.Error(-4999, "出库单出库失败，", out_order_num, "，插入出库明细表数据执行失败。")

			return 0.00, stock_count, false, errors.New("出库失败，插入出库明细表数据执行失败。")
		}

	} else {
		return 0.00, stock_count, false, errors.New("查询商品明细表失败。")
	}

	return stock_single_price, stock_count, true, nil

}
