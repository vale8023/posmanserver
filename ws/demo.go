package ws

import (
	"time"
	"freshlife.io/PosmanServer/hh"
)

type Demo struct {
	BaseService
}

func init()  {
	model := new(Demo)
	model.ModelName="demo"
	model.Run()
}

func (this *Demo) Run(){
	ServicePool[this.ModelName] = make(chan hh.Event)
	go this.Goroutine()
}

func (this *Demo)Goroutine(){
	for {
		select {
		case event := <-ServicePool[this.ModelName]:
			switch event.Content.Method  {
			case "GetCountClient":
				this.getCountClient(event)
				break
			case "GetServerTime":
				this.getServerTime(event)
				break
			default:
				this.methodNotFount(event)
				break
			}

		}
	}
}


func (this *Demo)getServerTime(event hh.Event)  {
	data := make(map[string] interface{})
	data["ServerTime"]=time.Now().Unix()
	SendEvent(event.ClientID,this.ModelName,event.Content.Method+"Result",data)
}

func (this *Demo)getCountClient(event hh.Event)  {
	data := make(map[string] interface{})
	data["number"]=len(ClientPool)
	SendEvent(event.ClientID,this.ModelName,event.Content.Method+"Result",data)
}
